# Matchex

Matchex is a command line tool based on STYX as a library.
It can be used to get the list of all the references to a given symbol in a code base.

## Build

    $ make

## Usage

    $ matchex --help
