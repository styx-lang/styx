---
header-includes:
title: Pre Expressions
---

## Grammar

```{=html}
<code class=ebnf>
PreExpression  ::= [AtExpression]
                 | [DerefExpression]
                 | [NegExpression]
                 | [NotExpression]
                 | [OneCompExpression]
                 | [PreDecExpression]
                 | [PreIncExpression]
                 | [PostExpression]
                 ;
</code>
```

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## AtExpression

### Grammar

```{=html}
<code class=ebnf>
AtExpression ::= "&" [PreExpression]
</code>
```

### Semantics

The unary operand must represent either a Lvalue or a function.

The expression takes the address of the unary operand.

The expression type is the [TypePointer] for the unary operand type.

```{.styx .numberLines}
var auto a = 0;  // u64
var auto b = &a; // u64*
var auto c = &b; // u64**
var auto d = &0; // error, cannot take address of rvalue expression `0`
```

It is illegal to take the address of an `@LLVM` function.

Pointers to virtual functions remain virtual unless `@virtual` is cast away using a pointer to a [TypeFunction]

```{.styx .numberLines}
class A
{
    @virtual function f() { }
}

class B : A
{
    @override function f()
    {
        alias Fun  = function (A* this);
        alias PFun = Fun*;

        // takes the static address but it's still virtual
        var auto p0 = &super.f;
        // would result in recursive `B.f()` calls
        version none do p0();

        // devirtualized, dont use the vtable, just call `A.f()`
        var auto p1 = p0:PFun;
        p1();
    }
}
```

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## DerefExpression

### Grammar

```{=html}
<code class=ebnf>
DerefExpression ::= "*" [PreExpression]
</code>
```

### Semantics

The type of the unary operand must be a pointer.

The expression is a Lvalue unless the sub-expression is for a [TypePointer] to a [TypeBool].

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## NegExpression

### Grammar

```{=html}
<code class=ebnf>
NegExpression ::= "-" [PreExpression]
</code>
```

### Semantics

The expression negate its unary.

The type of the unary operand must be numeric.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## NotExpression

### Grammar

```{=html}
<code class=ebnf>
NotExpression ::= "!" [PreExpression]
</code>
```

### Semantics

The expression performs logical negation of its unary.

The type of the unary operand must be `bool`{.styx} or interpretable as a condition.

The expression has the type `bool`{.styx}.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## OneCompExpression

### Grammar

```{=html}
<code class=ebnf>
OneCompExpression ::= "~" [PreExpression]
</code>
```

### Semantics

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## PreDecExpression

### Grammar

```{=html}
<code class=ebnf>
PreDecExpression ::= "--" [PreExpression]
</code>
```

### Semantics

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## PreIncExpression

### Grammar

```{=html}
<code class=ebnf>
PreIncExpression ::= "++" [PreExpression]
</code>
```

### Semantics
