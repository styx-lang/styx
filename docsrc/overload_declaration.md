---
header-includes:
title: Overload declaration
---

## Grammar

```{=html}
<code class=ebnf>
OverloadDeclaration ::= [Attribute]* "overload" [Identifier] (":" [Type])?  "{" [OverloadMember] ("," [Expression])* ","? "}"
</code>
```

## Semantics

An overload declaration allows to create a set of functions that are callable using the same identifier.
Overload declarations can contain free functions but also member functions if declared in the same parent as the members.
Each function must have an unique type. That type is determined ignoring the [TypeReturn],
meaning that the member uniqueness relies exclusively on the parameters type.

```{.styx .numberLines}
function createFromBytes(char[] values) {return;}
function createFromFile(char* name)     {return;}

overload create
{
    createFromBytes,
    createFromFile,
}

function main(): s32
{
    const char* name = "test.txt";
    create(name); // calls createFromFile
    return 0;
}
```

The members are tried following the lexicographical order, in two passes.

During the first pass a member is selected if all the arguments match exactly, then the second pass
if aborted.

During the second pass the first member that matches, including using implicit convertions, is selected.

```{.styx .numberLines}
function testF32(f32 value){}
function testF64(f64 value){}

overload test
{
    testF64,
    testF32
}

function main(): s32
{
    const f32 v = 0.0:f32;
    test(v); // calls testF32
    return 0;
}
```

It is illegal to define a unapplied generic function as member.

```{.styx .numberLines}
function f1[T](){}

function f2[T](){}

overload f
{
    f1, // error
    f2  // error
}
```

An overload declaration can extend an existing overload. This is useful for example to extend overloads defined in libraries.

```{.styx .numberLines}
import rtl.convert;

overload toString : rtl.convert.toString
{
    convertItemtoString
}

struct Item {}

convertItemtoString(var Item item): char*
{
    return "representation...";
}
```

Only a single base is allowed as otherwise unrelated sets could be merged.

If the overload is an extension then its protection must be higher or equal to the base declaration.

If the overload is `@public` or `@protected` then all its members must have the same protection.

If the overload is `@final` then it cannot be extended.

If the overload is `@final` then members cannot be added using the `@overload` attribute.
