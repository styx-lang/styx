/// The Styx token type, as produced by the lexer, and few utilities
unit styx.token;

@private import
    system,
    styx.position,
    styx.allocator,
    ;

/// Enumerates the different token.
enum TokenType : u8
{
    invalid,
    eof,
    id,
    //
    intLiteral,
    floatLiteral,
    hexLiteral,
    binLiteral,
    stringLiteral,
    rawStringLiteral,
    charLiteral,
    //
    lineComment,
    starComment,
    // Keywords
    $alias,
    $apply,
    $assert,
    $asm,
    $auto,
    $break,
    $class,
    $const,
    $continue,
    $defer,
    $delete,
    $do,
    $echo,
    $else,
    $enum,
    $false,
    $foreach,
    $from,
    $function,
    $goto,
    $if,
    $import,
    $in,
    $label,
    $new,
    $null,
    $on,
    $overload,
    $protection,
    $return,
    $static,
    $struct,
    $super,
    $switch,
    $this,
    $true,
    $union,
    $unit,
    $var,
    $version,
    $while,
    // basic types
    $bool,
    $f32,
    $f64,
    $s16,
    $s32,
    $s64,
    $s8,
    $ssize,
    $u16,
    $u32,
    $u64,
    $u8,
    $char,
    $usize,
    // symbols
    not,
    qmark,
    colon,
    comma,
    dot,
    semiColon,
    leftCurly,
    leftParen,
    leftSquare,
    rightCurly,
    rightParen,
    rightSquare,
    at,
    dollar,
    dotDot,
    ellipsis,
    optAccess,
    // assign
    ass,
    mulAss,
    divAss,
    modAss,
    plusAss,
    minusAss,
    ampAss,
    pipeAss,
    xorAss,
    lshiftAss,
    rshiftAss,
    tidAss,
    dotAss,
    optAss,
    // arithmetic, bitwise operators
    mul,
    div,
    mod,
    plus,
    minus,
    amp,
    pipe,
    tidle,
    lShift,
    rShift,
    xor,
    // relational operators
    equal,
    notEqual,
    greater,
    greaterEqual,
    lesser,
    lesserEqual,
    // logical operators
    andAnd,
    orOr,
    // postfixes
    minMin,
    plusPlus,
    // additional ExpressionAstNode.operator
    eopArray    = 119,
    eopIndex,
    eopSlice,
    eopPreTidle,
    eopNeg,
    eopDeref,
    eopAt,
    eopPreInc,
    eopPreDec,
    eopType,
    eopLength,
    eopPtr,
    eopSetLength,
    eopOpOvMacro,
    eopDecl,
    eopLambda,
    eopRefCount,
    eopRcaAssign,
    // additional StatementAstNode.kind
    skContinueOn = 137,
    skBreakOn,
    // additional DeclarationAstNode.kind
    dkEnumMembr = 139,
    generic,
    // additional TypeAstNode.kind
    tkSlice     = 141,
    tkSta,
    tkUnamb,
    tkVoid,
    tkEnumSet,
    tkRca,
}

/// Returns: the string representation of a `TokenType`
const char[][TokenType.max + 1] tokenString =
[
    "(invalid)",
    "(eof)",
    "(id)",
    //
    "(intLiteral)",
    "(floatLiteral)",
    "(hexLiteral)",
    "(binLiteral)",
    "(stringLiteral)",
    "(rawStringLiteral)",
    "(charLiteral)",
    //
    "(//)",
    "(/* */)",
    // Keywords
    "alias",
    "apply",
    "assert",
    "asm",
    "auto",
    "break",
    "class",
    "const",
    "continue",
    "defer",
    "delete",
    "do",
    "echo",
    "else",
    "enum",
    "false",
    "foreach",
    "from",
    "function",
    "goto",
    "if",
    "import",
    "in",
    "label",
    "new",
    "null",
    "on",
    "overload",
    "protection",
    "return",
    "static",
    "struct",
    "super",
    "switch",
    "this",
    "true",
    "union",
    "unit",
    "var",
    "version",
    "while",
    // basic types
    "bool",
    "f32",
    "f64",
    "s16",
    "s32",
    "s64",
    "s8",
    "ssize",
    "u16",
    "u32",
    "u64",
    "u8",
    "char",
    "usize",
    // symbols
    "!",
    "?",
    ":",
    ",",
    ".",
    ";",
    "{",
    "(",
    "[",
    "}",
    ")",
    "]",
    "@",
    "$",
    "..",
    "...",
    "?.",
    // assign
    "=",
    "*=",
    "/=",
    "%=",
    "+=",
    "-=",
    "&=",
    "|=",
    "^=",
    "<<=",
    ">>=",
    "~=",
    ".=",
    "?=",
    // arithmetic, bitwise operators
    "*",
    "/",
    "%",
    "+",
    "-",
    "&",
    "|",
    "~",
    "<<",
    ">>",
    "^",
    // relational operators
    "==",
    "!=",
    ">",
    ">=",
    "<",
    "<=",
    // logical operators
    "&&",
    "||",
    // postfixes
    "--",
    "++",
    // additional ExpressionAstNode.operator
    "(eopArray)",
    "(eopIndex)",
    "(eopSlice)",
    "~",
    "-",
    "*",
    "&",
    "++",
    "--",
    "(eopType)",
    "(.length)",
    "(.ptr)",
    "(.length = )",
    "(eopOpOvMacro)",
    "(eopDecl)",
    "=>",
    "(eopRefCount)",
    "(eopRcaAssign)",
    // additional StatementAstNode.kind
    "(continue on)",
    "(break on)",
    // additional DeclarationAstNode.kind
    "(dkEnumMember)",
    "(generic)",
    // additional TypeAstNode.kind
    "(tkSlice)",
    "(tkSta)",
    "(tkUnamb)",
    "void",
    "(tkEnumSet)",
    "(tkRca)"
];

/// Returns: if the token is for a relational operator
function isRelational(TokenType tt): bool
{
    return  TokenType.equal <= tt && tt <= TokenType.lesserEqual;
}

/// Internable string type
@private struct String
{
    ///
    var char[+] text;
    var String* next;
    var usize hash;
    ///
    @constructor function create(char[] text; const String* next; usize hash)
    {
        this.text = text;
        this.next = next;
        this.hash = hash;
    }
    ///
    @destructor function destroy()
    {
        if const n = next do
            delete n;
        text.decref;
    }
    ///
    function array(): char[+]
    {
        return text;
    }
}

/** Storage for the unique address of interned String.

 The goal  here is not to save memory but uniquely to reduce the time complexity
 of the Symbol.searchXXXX functions  */
@private struct StringAddrPool
{
protection (private)

    var static String*[16384] entries;

    static function hash(const char* data; const usize length): usize
    {
        version X86_64 do
        {
            var usize h = 0xCBF29CE484222325;
            foreach const i in 0 .. length do
            {
                h ^= *(data+i);
                h *= 0x100000001B3;
            }
        }
        else version X86 do
        {
            var usize h = 0x811C9DC5;
            foreach const i in 0 .. length do
            {
                h ^= *(data+i);
                h *= 0x1000193;
            }
        }
        return h;
    }

protection (public)

    @destructor static function destroy()
    {
        foreach const i in 0 .. entries.length do
            if const s = entries.ptr[i] do
                delete s;
    }

    /// Returns: a pointer to a string, interned
    static function intern(char[] text): String*
    {
        // elem hash
        const h1 = hash(text.ptr, text.length);
        // bucket hash
        const h0 = h1 % 16384;
        var s1 = var s = entries.ptr[h0];
        while s do
        {
            if s.hash == h1 && s.text[] == text do
                return s;
            s .= next;
        }
        return entries.ptr[h0] = (new String).create(text, s1, h1);
    }

    /// Returns: a pointer to a string, not interned
    static function nonintern(char[] text): String*
    {
        return (new String).create(text, null, 0);
    }
}

@unittest function test2()
{
    const String* s1 = StringAddrPool.intern("hello");
    const String* s2 = StringAddrPool.intern("hello");

    assert(s1.array() == s2.array());
    assert(s1 == s2);
}

@unittest function testBucketLeak()
{
    // with FNV, both idents have the same hash, which reveals a case of #289
    const String* s1 = StringAddrPool.intern("styx");
    const String* s2 = StringAddrPool.intern("parseReturnStatement");
    const String* s3 = StringAddrPool.intern("parseReturnStatement");
}

@private function getIdentifierKind(const char[] str): TokenType
{
    return switch str do
    {
    "alias"       => $alias,
    "apply"       => $apply,
    "asm"         => $asm,
    "assert"      => $assert,
    "auto"        => $auto,
    "bool"        => $bool,
    "break"       => $break,
    "char"        => $char,
    "class"       => $class,
    "const"       => $const,
    "continue"    => $continue,
    "defer"       => $defer,
    "delete"      => $delete,
    "do"          => $do,
    "echo"        => $echo,
    "else"        => $else,
    "enum"        => $enum,
    "f32"         => $f32,
    "f64"         => $f64,
    "false"       => $false,
    "foreach"     => $foreach,
    "from"        => $from,
    "function"    => $function,
    "goto"        => $goto,
    "if"          => $if,
    "import"      => $import,
    "in"          => $in,
    "label"       => $label,
    "new"         => $new,
    "null"        => $null,
    "on"          => $on,
    "overload"    => $overload,
    "protection"  => $protection,
    "return"      => $return,
    "s16"         => $s16,
    "s32"         => $s32,
    "s64"         => $s64,
    "s8"          => $s8,
    "ssize"       => $ssize,
    "static"      => $static,
    "struct"      => $struct,
    "super"       => $super,
    "switch"      => $switch,
    "this"        => $this,
    "true"        => $true,
    "u16"         => $u16,
    "u32"         => $u32,
    "u64"         => $u64,
    "u8"          => $u8,
    "union"       => $union,
    "unit"        => $unit,
    "usize"       => $usize,
    "var"         => $var,
    "version"     => $version,
    "while"       => $while,
    else          => id
    };
}

struct Token
{
    @private var String* ttext;
    var Token* prev;
    var Position position;
    var TokenType type;
    @private var bool isDollarKw;

    /* The source is maintained as a storage for the token text so escapes
    in string literals need to be processed from the constant storage*/
    function postProcessStringLiteral(const char[] rawText): char[+]
    {
        var result  = (new char[+])(rawText.length);
        var rptr    = result.ptr;
        var usize i;
        var usize j;

        while i < rawText.length do
        {
            if rawText.ptr[i] == '\\' do
            {
                switch rawText.ptr[++i] do
                {
                on '0' do
                {
                    rptr[j] = '\0';
                    ++i; ++j;
                    continue;
                }
                on 'a' do
                {
                    rptr[j] = '\a';
                    ++i; ++j;
                    continue;
                }
                on 'b' do
                {
                    rptr[j] = '\b';
                    ++i; ++j;
                    continue;
                }
                on 'f' do
                {
                    rptr[j] = '\f';
                    ++i; ++j;
                    continue;
                }
                on 'n' do
                {
                    rptr[j] = '\n';
                    ++i; ++j;
                    continue;
                }
                on 'r' do
                {
                    rptr[j] = '\r';
                    ++i; ++j;
                    continue;
                }
                on 't' do
                {
                    rptr[j] = '\t';
                    ++i; ++j;
                    continue;
                }
                on 'v' do
                {
                    rptr[j] = '\v';
                    ++i; ++j;
                    continue;
                }
                on '"' do
                {
                    rptr[j] = '"';
                    ++i; ++j;
                    continue;
                }
                on '\'' do
                {
                    rptr[j] = '\'';
                    ++i; ++j;
                    continue;
                }
                on '\\' do
                {
                    rptr[j] = '\\';
                    ++i; ++j;
                    continue;
                }
                on 'x' do
                {
                    var u8 hi = rawText.ptr[i+1];
                    var u8 lo = rawText.ptr[i+2];
                    hi = hi <= '9' ? hi - '0' else hi <= 'F' ? 10:u8 + hi - 'A' else 10:u8 + hi - 'a';
                    lo = lo <= '9' ? lo - '0' else lo <= 'F' ? 10:u8 + lo - 'A' else 10:u8 + lo - 'a';
                    rptr[j] = (hi << 4) + lo;
                    i += 3; ++j;
                    continue;
                }
                // invalid escape already rejected by the lexer
                else { assert(0); }
                }
            }
            rptr[j] = rawText.ptr[i];
            ++i; ++j;
            continue;
        }
        return rptr[0..j];
    }

    @constructor function create(char[] text; u32 line; u32 column; TokenType type)
    {
        this.position   = (line, column);
        this.type       = type == TokenType.id ? getIdentifierKind(text) else type;

        if type == TokenType.id && (const tlen = text.length) > 1 && text.ptr[0] == '$' do
        {
            text = text.ptr[1 .. tlen];
            --column;
            isDollarKw = true;
        }

        // `postProcessStringLiteral()` result lifetime LT `text`'s one
        var char[+] ppstring;
        if this.type == TokenType.stringLiteral || this.type == TokenType.charLiteral do
        {
            ppstring = postProcessStringLiteral(text);
            text = ppstring;
        }

        iptr = (ttext = switch this.type do
        {
            id, TokenType.$this, stringLiteral, rawStringLiteral, charLiteral => StringAddrPool.intern(text),
            invalid, intLiteral .. binLiteral => StringAddrPool.nonintern(text),
            else => null
        }):u8*;
    }

    @destructor function destroy()
    {
        switch type do
        {
            on id, TokenType.$this, stringLiteral, rawStringLiteral, charLiteral do {}
            else if ttext do delete ttext;
        }
    }

    static function generateIdentifier(): Token*
    {
        return identifier("__tmp" ~ (static var u32 cnt)++.toString());
    }

    static function generateIdentifierFromPos(const var Position p; char[] prefix): Token*
    {
        return identifier(prefix ~ 'L' ~ p.line.toString() ~ 'C' ~  p.column.toString());
    }

    /// Returns: if the identifier is a keyword prefixed with dollar.
    function keywordAsIdentifier(): bool {return isDollarKw;}

    /// Returns: The line, 1 based, where the token starts.
    function line(): u32 {return position.line;}

    /// Returns: The column, 1 based, where the token starts.
    function column(): u32 {return position.column;}

    /// Returns: The pointer used for fast comparisons of identifiers
    const u8* iptr = null;

    /// Returns: The token text.
    function text(): char[+]
    {
        return ttext ? ttext.text else tokenString[type];
    }

    function isTokBasicType(): bool
    {
        return TokenType.$bool <= type && type <= TokenType.$usize;
    }

    function isTokKeyword(): bool {return TokenType.$alias <= type && type <= TokenType.$while;}

    function isTokStorageClass(): bool {return type == $var || type == $const || type == $static;}

    function isTok[TokenType ett](): bool { return type == ett; }
}

/// Returns: `t` string representation.
function stringRepresentation(const Token* t): char[+]
{
    if t.type != TokenType.stringLiteral && t.type != TokenType.charLiteral do
        return t.ttext.array();

    // *4 : in case all chars must be output using hex escapes
    const ttlen = t.ttext.text.length;
    var char[+] result = (new char[+])(ttlen * 4 + 1);
    var usize i;
    var usize j;
    while i != ttlen do
    {
        const c = t.ttext.text.ptr[i];
        switch c do
        {
        on '\\' do
        {
            result.ptr[j .. j + 2] = `\\`;
            j += 2;
        }
        on '"' do
        {
            result.ptr[j .. j + 2] = `\"`;
            j += 2;
        }
        on  0 .. 31 do
        {
            result.ptr[j++] = '\\';
            result.ptr[j++] = 'x';
            var u8 hi   = (c >> 4) & 0xF;
            result.ptr[j++] = hi < 10 ? hi + '0' else (hi - 10) + 'A';
            var u8 lo   = (c     ) & 0xF;
            result.ptr[j++] = lo < 10 ? lo + '0' else (lo - 10) + 'A';
        }
        else
        {
            result.ptr[j] = c;
            ++j;
        }
        }
        ++i;
    }
    result[j++] = 0;
    return result[0 .. j-1];
}

/// Returns: a Token of type identifier for the input string.
@allocator(tokAllocator.allocate)
function identifier(char[] text): Token*
{
    return (new Token).create(text, 0, 0, TokenType.id);
}

/// Returns: a pointer to the static Token*, lazily initialized, for the string `S`.
function StaticTok[S](): Token*
{
    static var Token* tok;

    @destructor static function destroy()
    {
        if tok do delete tok;
    }

    return tok ?= (new Token).create(S, 0, 0, TokenType.id);
}

/// Returns: The Token used to identify `align`.
alias alignToken = StaticTok!["align"];

/// Returns: The Token used to identify `intel`.
alias asmFlavIntelToken = StaticTok!["intel"];

/// Returns: The Token used to identify `att`.
alias asmFlavAttToken = StaticTok!["att"];

/// Returns: The Token used to identify `deprecated`.
alias deprecatedToken = StaticTok!["deprecated"];

/// Returns: The Token used to identify `func_t`.
alias echoFunctToken = StaticTok!["func_t"];

/// Returns: The Token used to identify `type`.
alias echoTypeToken = StaticTok!["type"];

/// Returns: The Token used to identify `foreign`.
alias foreignToken = StaticTok!["foreign"];

/// Returns: The Token used to identify `getPointedType`.
alias echoGetPointedTypeToken = StaticTok!["getPointedType"];

/// Returns: The Token used to identify `inline`.
alias inlineToken = StaticTok!["inline"];

/// Returns: The Token used to identify `fflush`.
alias libcFFlushToken = StaticTok!["fflush"];

/// Returns: The Token used to identify `printf`.
alias libcPrintfToken = StaticTok!["printf"];

/// Returns: The Token used to identify `realloc`.
alias libcReallocToken = StaticTok!["realloc"];

/// Returns: The Token used to identify `memcmp`.
alias libcMemcmpToken = StaticTok!["memcmp"];

/// Returns: The Token used to identify `LLVM`.
alias llvmToken = StaticTok!["LLVM"];

/// Returns: The Token used to identify `main`.
alias mainToken = StaticTok!["main"];

/// Returns: The Token used to identify `operator`.
alias operatorToken = StaticTok!["operator"];

/// Returns: The Token used to identify `private`.
alias privateToken = StaticTok!["private"];

/// Returns: The Token used to identify `length`.
alias propLengthToken = StaticTok!["length"];

/// Returns: The Token used to identify `expand`.
alias propExpandToken = StaticTok!["expand"];

/// Returns: The Token used to identify `protected`.
alias protectedToken = StaticTok!["protected"];

/// Returns: The Token used to identify `public`.
alias publicToken = StaticTok!["public"];

/// Returns: The Token used to identify `reverse`.
alias reverseToken = StaticTok!["reverse"];

/// Returns: The Token used to identify `rtl`.
alias rtlToken = StaticTok!["rtl"];

/// Returns: The Token used to identify `strict`.
alias strictToken = StaticTok!["strict"];

/// Returns: The Token used to identify `this`.
alias thisToken = StaticTok!["this"];

/// Returns: The Token used to identify `unittest`.
alias unittestToken = StaticTok!["unittest"];

/// Returns: The Token that gives the semver.
alias versionToken = StaticTok!["0.24.1"];

/// Returns: The Token used to name the virtual table.
alias vtableToken = StaticTok!["#vtable"];

/// Returns: The Token used to identify `__sx_obj_equal`.
alias objEqualToken = StaticTok!["__sx_obj_equal"];

/// Returns: The Token used to identify `Object`.
alias objectToken = StaticTok!["Object"];

/// Returns: The Token used to identify `RcArray`.
alias rcArrayToken = StaticTok!["RcArray"];

/// Returns: The Token used to identify `allocator`.
alias allocatorToken = StaticTok!["allocator"];

/// Returns: The Token used to identify `deallocator`.
alias deallocatorToken = StaticTok!["deallocator"];

/// Returns: The Token used to identify `__sx_ss_lut`
alias sxssLutToken = StaticTok!["__sx_ss_lut"];

/// Returns: `toks` string representing, separated with `.`
function tokenChainText(const Token*[] toks): char[+]
{
    var Token* t0 = toks[0];
    if toks.length == 1 do
        return t0.text();

    var Appender![char,128] app;

    @return var char[+] result;
    const lastIndex = toks.length - 1;
    foreach (const i; const t) in toks do
    {
        app.appendN(t.text());
        if i != lastIndex do
            app.append('.');
    }
    app.fill(result);
    return result;
}

@unittest function test4()
{
    var Token* t0 = (new Token).create("one", 0, 0, TokenType.id);
    var Token* t1 = (new Token).create("two", 0, 0, TokenType.id);
    var Token*[+] toks = [t0, t1];

    assert(toks.tokenChainText() == "one.two");

    assert([t0].tokenChainText() == "one");
    delete t0;
    delete t1;
}

@unittest function test5()
{
    var Token* t0 = (new Token).create("unit", 0, 0, TokenType.id);
    assert(t0.isTok![$unit]());
    delete t0;
}

@unittest function test6()
{
    var Token* t0 = (new Token).create("$function", 0, 0, TokenType.id);
    assert(t0.isTok![id]());
    assert(t0.stringRepresentation() == "function");
    delete t0;
}
