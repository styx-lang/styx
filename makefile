FRONT_SRC = src/system.sx\
            src/styx/allocator.sx\
            src/styx/position.sx\
            src/styx/session.sx\
            src/styx/token.sx\
            src/styx/symbol.sx\
            src/styx/sar.sx\
            src/styx/string_switch.sx\
            src/styx/lexer.sx\
            src/styx/ast/base.sx\
            src/styx/ast/visitor.sx\
            src/styx/ast/expressions.sx\
            src/styx/ast/formatter.sx\
            src/styx/ast/declarations.sx\
            src/styx/ast/statements.sx\
            src/styx/ast/types.sx\
            src/styx/scope.sx\
            src/styx/parser.sx\
            src/styx/semantic/symbolize.sx\
            src/styx/semantic/protection.sx\
            src/styx/semantic.sx\
            src/styx/semantic/imports.sx\
            src/styx/semantic/declarations.sx\
            src/styx/semantic/expressions.sx\
            src/styx/semantic/convert.sx\
            src/styx/semantic/deprecations.sx\
            src/styx/semantic/evaluate.sx\
            src/styx/semantic/switches.sx\
            src/styx/semantic/types.sx\
            src/styx/semantic/memulator.sx\
            src/styx/semantic/statements.sx\
            src/styx/semantic/bodies.sx\
            src/styx/semantic/generics.sx\
            src/styx/lint.sx\
            src/styx/suggest.sx\
            src/algorithms.sx

BACK_SRC =  src/styx/backend/link.sx\
            src/styx/backend/llvm.sx\
            src/styx/backend/irgen.sx\
            src/styx/backend.sx

BACK_LNK = -L-L/usr/lib64 -L-lLLVM-15

RTL ?= library/rtl.sx
STYXC ?= bin/styx
RTLPREV =  previous/rtl.sx
STYXCPREV = previous/styx

COMPILER_COMMON = src/driver.sx $(FRONT_SRC) $(BACK_SRC) $(BACK_LNK) --of=bin/styx --check=all --check=-new --vid=llvm --fno-tls-escapes -g --fassertz-loc $(extra)

prepare-folder:
	if [ ! -d bin ]; then mkdir bin; fi

# The compiler, without backend
compiler-front: prepare-folder
	$(STYXC) --rtl=$(RTL) src/driver.sx $(FRONT_SRC) --of=bin/styx --check=all -g $(extra)

# The full compiler, build using the previous release
compiler-previous: prepare-folder
	$(STYXCPREV) --rtl=$(RTLPREV) $(COMPILER_COMMON)

# The full compiler, build using the development version
compiler: prepare-folder
	$(STYXC) --rtl=$(RTL) $(COMPILER_COMMON)

# The full compiler, build using the development version and with code to self-cover
compiler-cov: prepare-folder
	$(STYXC) --rtl=$(RTL) $(COMPILER_COMMON) --cov

# The front end, including linting
compiler-sema:
	$(STYXC) --rtl=$(RTL) src/driver.sx $(FRONT_SRC) $(BACK_SRC) --vid=llvm --lint=all --fforeach-bce --vforeach-bce --until=semantic $(extra)

# Run the test suite
test: compiler-previous
	sh test.sh

clean:
	rm -rf *.o
