---
header-includes:
title: Unit Declaration
---

## Grammar

```{=html}
<code class=ebnf>
UnitDeclaration ::= "unit" [Identifier] (.[Identifier])* ";" [Declaration]*
</code>
```

## Semantics

A compilation unit always starts with a chain allowing to identify the unit.

It is then followed by an arbitrary amount of [Declaration].
