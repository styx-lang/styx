unit styx.semantic.bodies;

@private import
    styx.ast.types,
    styx.ast.declarations,
    styx.ast.base,
    styx.ast.statements,
    styx.session,
    styx.ast.formatter,
    styx.ast.expressions,
    styx.semantic.memulator,
    styx.symbol,
    styx.semantic.statements,
    styx.semantic.convert,
    ;

@private alias fv = from(styx.ast.visitor);

/// Perform semantic checks for all function bodies in the given declaration
function bodySemantic(Declaration* node)
{
    @final class BodyVisitor : fv.AstVisitor
    {
        var FunctionDeclaration* curr;

        // When a generic is encountered, not all instances exist because
        // they are often created  during body sema of other functions.
        var Declaration*[+] generics;
        function processGenerics()
        {
            var usize i = 0;
            while true do
            {
                if i == generics.length do
                    break;
                var g = generics.ptr[i++];
                while g do
                {
                    if g.genericParameters.applied() do
                        visitDeclaration(g);
                    g .= nextInstance;
                }
            }
        }

        @override function visitAliasDeclaration(@plain AliasDeclaration* node)
        {
            // Applied generics have an AST which is not inserted in the
            // AST produced by parsing the source code.
            //
            // This allows to visit member functions bodies located in those
            // "dangling" trees.
            if node.sourceSymbol && ((node.exp && node.exp.operator != $from) ||
                // TypeAppliedGeneric is substitued during typesma
                // so it never appears as alias RHS.
                node.wasTypeApplied)
            do
            {
                var d = node.sourceSymbol.astNode.asDeclaration();
                visitConcreteDeclaration(d);
            }
        }

        @override function visitApplyExpression(ApplyExpression* node)
        {
            assert(0, "bodies of instances should be handled in `processGenerics()`");
        }

        @override function visitClassDeclaration(@plain ClassDeclaration* node)
        {
            memulate(node, node.scope);
            if !node.isGeneric() do
                node.accept(this);
        }

        @override function visitDeclaration(@plain Declaration* node)
        {
            if !node.isGeneric() do
            {
                const sym    = node.symbol;
                const hasSym = !!sym;
                if !hasSym || !sym.recurseGuard do
                {
                    if hasSym do ++sym.recurseGuard;
                    visitConcreteDeclaration(node);
                    if hasSym do --sym.recurseGuard;
                }
            }
            else generics ~= node;
        }

        @override function visitExpression(const Expression* node)
        {
        }

        @override function visitFunctionDeclaration(@plain FunctionDeclaration* node)
        {
            //printf("%s\n", format(node/*, testMode*/).ptr);

            if !node.body || node.isGeneric() || isInGeneric in node.scope.flags || node.bodyDone do
                return;

            assert(node.progress == SemanticProgress.done);
            node.bodyDone = true;

            var bs = asBlockStatement(node.body);
            var og = session.pauseGagging();
            const oe = session.errorsCount();

            var Statement*[+] ma;
            foreach const p in node.parameters do
                if isThisDotId in p.flags do
            {
                var   bp = bs.startPos;
                const id = (new IdentExpression).create(bp, p.name);
                const de = (new DotExpression).create(bp, (new ThisExpression).create(bp), id);
                const ae = (new AssignExpression).create(bp, de, id.copy());
                ma      ~= (new ExpressionStatement).create(bp, ae);
            }
            if ma.length do
                bs.statements = ma ~ bs.statements;
            statementSemantic(bs, bs.scope);

            var t = node.returnType.resolved();
            if isTypeAuto(t) do
                (var AutoReturnVisitor arv).visitFunctionDeclaration(node);

            if !bs.flow.isReturn() && !bs.flow.isContinue() && !bs.flow.isNoReturn() do
            {
                if isTypeVoid(node.returnType.resolved()) do
                {
                    var rs         = (new ReturnStatement).create(bs.stopPos);
                    rs.stopPos     = bs.stopPos;
                    bs.statements ~= rs;
                    statementSemantic(rs, bs.scope);
                    bs.flow.mixLinear(rs.flow);
                    rs.defers = bs.defers;
                    bs.defers.decref;
                }
                else if oe == session.errorsCount() do
                {
                    session.error(node.scope.filename, bs.stopPos, "expected return statement");
                }
            }

            if node.escapes && (session.optLevel || session.escapeUsed) && isNested !in node.flags do
                checkEscapeUsed(node);
            session.resumeGagging(og);
            node.body.accept(this);
        }

        @override function visitStructDeclaration(StructDeclaration* node)
        {
            memulate(node, node.scope);
            if !node.isGeneric() do
                node.accept(this);
        }

        @override function visitTypeClass(const TypeClass* node)
        {
            visitTypeDeclared(node);
        }

        function visitTypeDeclared(TypeDeclared* node)
        {
            const d = node.declaration;
            const s = d.symbol;
            if s.recurseGuard do
                return;
            ++s.recurseGuard;
            visitConcreteDeclaration(d);
            --s.recurseGuard;
        }

        @override function visitTypeFunction(const TypeFunction* node)
        {
            visitTypeDeclared(node);
        }

        @override function visitTypeStruct(const TypeStruct* node)
        {
            visitTypeDeclared(node);
        }

        @override function visitTypeUnion(const TypeUnion* node)
        {
            visitTypeDeclared(node);
        }

        @override function visitUnionDeclaration(UnionDeclaration* node)
        {
            memulate(node, node.scope);
            if !node.isGeneric() do
                node.accept(this);
        }
    }
    var BodyVisitor bv;
    bv.visitDeclaration(node);
    bv.processGenerics();
}

/// Partially prevent escapes that are used to access static members to create slower code
function checkEscapeUsed(@plain FunctionDeclaration* node; const bool isLambda = false)
{
    @final class EscapeUsedVisitor : fv.AstVisitor
    {
        static var FunctionDeclaration* that;
        static var bool inNested;

        @override function visitExpression(Expression* node)
        {
            if inNested do visitConcreteExpression(node);
        }

        @override function visitDotExpression(DotExpression* node)
        {
            node.accept(this);
            var ed = symToVarDecl(node.expression.symbol);
            var vd = symToVarDecl(node.dotted.symbol);
            var fd = symToFuncDecl(node.dotted.symbol);
            if ed && ed.escapeUsed && (var d = vd ? fd) do
            {
                // dont use the type of the dot LHS:
                // 1. because the dot LHS can be a pointer to an aggr instance
                // 2. that allows to skip global static functions
                var t = d.symbol.parent.asType();

                if t && d.isStatic() do
                {
                    const o = node.expression;
                    // `escapeVar.staticMember` -> `AggregateType.staticMember`
                    node.expression = (new TypeExpression).create(node.expression.startPos, t, true);
                    --ed.escapeUsed;
                    if session.vescapeUsed do
                        printf("%s:%d:%d: rewritten `%s.%s` to `%s`\n", that.scope.filename.ptr, node.startPos.line, node.startPos.column,
                            format(o).ptr, format(node.dotted).ptr, format(node).ptr);
                }
            }
        }

        @override function visitFunctionDeclaration(FunctionDeclaration* node)
        {
            const o  = inNested;
            inNested = isNested in node.flags;
            visitConcreteStatement(node.body);
            inNested = o;
        }
    }

    static var EscapeUsedVisitor euv;
    // lambda bodies are checked before the parent function,
    if (euv.that = isLambda ? node.scope.pop().func else node) do
        euv.visitConcreteDeclaration(node);

    const old = node.escapes.dup;
    var u32 j;
    foreach const vd in old do
    {
        if vd.escapeUsed do
        {
            vd.escapeIndex = j;
            node.escapes[j++] = vd;
        }
        else if session.vescapeUsed do
            printf("%s:%d:%d: unflaged `%s` for escaping\n", node.scope.filename.ptr, vd.startPos.line, vd.startPos.column, format(vd).ptr);
    }
    node.escapes.length = j;
}

@private @final class AutoReturnVisitor : fv.AstVisitor
{
    var ReturnStatement*[+] returns;
    var bool nested;

    @destructor function destroy()
    {
        returns.decref;
    }

    @override function visitReturnStatement(const ReturnStatement* node)
    {
        returns ~= node;
    }

    @override function visitExpressionStatement(const ExpressionStatement* node){}
    @override function visitAssertStatement(const AssertStatement* node){}
    @override function visitType(const Type* node){}

    @override function visitFunctionDeclaration(@plain FunctionDeclaration* node)
    {
        if nested do
            return;
        nested = true;

        visitConcreteStatement(node.body);

        var Type* commonType;
        foreach (const i; const r) in returns do
        {
            var e = r.expression;
            if i == 0 do
                continue commonType = e ? e.type.resolved() else TypeVoid.instance();
            if e do
                e .= implicitConvTo(commonType, node.scope);
            if !e do
                return error(node.scope.filename, returns.ptr[i].startPos, "return expression expected to be of type `%s`", format(commonType).ptr);

            r.expression = e;
        }
        if !commonType || isTypeAuto(commonType) do
        {
            error(node.scope.filename, node.startPos, "cannot determine the return type of `%s`", node.name.text().ptr);
            commonType = TypeError.instance();
        }
        node.returnType = commonType;
        if commonType.kind == tkSlice do
            error(node.scope.filename, node.startPos, "function can only return ref-counted and static arrays, not `%s` which is an array slice", format(commonType).ptr);
    }
}
