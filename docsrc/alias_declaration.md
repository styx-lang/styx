---
header-includes:
title: Alias declaration
---

## Grammar

```{=html}
<code class=ebnf>
Aliasee ::= [ApplyExpression]
          | [LambdaExpression]
          | [FromExpression]
          | "this" [PostExpression]*
          | [Type]
          ;
AliasDeclaration ::= [Attribute]* "alias" [Identifier] "=" Aliasee ";"
</code>
```

## Semantics

An alias can be declared for [Type]s and [Declaration]s.

```{.styx .numberLines}
alias Fun = function(s32 a);
alias FunPtr = Fun*;

struct S { struct R {} }
alias SR = S.R;
```

[Expression]s are allowed when they yield a declaration, for example generic applications

```{.styx .numberLines}
struct S[T] {}
alias S1 = apply(S, 1);
```

lambdas

```{.styx .numberLines}
alias fun = function() => 0;
```

and [FromExpression]s

```{.styx .numberLines}
alias ad = from(awesome.driver);
```

Using an alias is like using the aliasee excepted that a deprecation check is performed
each time that the `alias`{.styx} identifier is used.

```{.styx .numberLines}
alias Int = s32;
var Int i;

function f(s32 p){}

f(i); // `i` type matches exactly
```

If the aliasee is not a generic function application then the only [Attribute]s allowed
are `@deprecated`{.styx} and `@final`{.styx}.

In the opposite case the generic function application takes the alias name and attributes.

Only parameter-less attributes are supported, meaning that for example `@overload` is not transferred.

The transfert notably allows `@override` functions to share an implementation

```{.styx .numberLines}
class Base
{
    @abtract function f1(f64 p);
    @abtract function f2(u64 p);
}

class Derived : Base
{
    function impl[T](T t)
    {
        if echo(is, T, f64) do
        {
        }
        else do if echo(is, T, u64) do
        {
        }
    }

    @override alias f1 = impl![f64];
    @final @override alias f2 = impl![u64];
}
```

It is illegal to transfert the attributes on an application that already exists
but non-transferring aliases are still allowed.

```{.styx .numberLines}
class Base
{
    @abtract function f1(f64 p);
}

class Derived : Base
{
    function impl[T](T t)
    {
    }

    @override alias f1 = impl![f64];
    @override @final alias f2 = impl![f64]; // error
    alias NonTransferring = impl![f64];     // OK
}
```
