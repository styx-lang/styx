---
header-includes:
title: Lexical aspects
---

## Encoding

The code is made of ASCII characters only.
Multi Bytes Characters (only UTF8-encoded) are allowed within string literals and comments.
This allows to lex styx code without decoding.

## SheBang line

A Styx source file may start with a unix-style script line

```{.styx .numberLines}
#! /usr/bin/styx
unit u;
```

## Keywords

The following identifiers are reserved and cannot be directly used as identifier:

### Keywords used for declarations or statements

- alias
- apply
- asm
- assert
- auto
- break
- class
- const
- continue
- defer
- delete
- do
- echo
- else
- enum
- finally
- foreach
- from
- function
- goto
- if
- import
- in
- label
- new
- on
- overload
- protection
- return
- static
- struct
- switch
- template
- throw
- try
- union
- unit
- var
- version
- while

### Keywords used for the built-in types

- bool
- char
- f32
- f64
- s8
- s16
- s32
- s64
- ssize
- u8
- u16
- u32
- u64
- usize

### Keywords used for special values

- false
- null
- super
- this
- true

## Identifier

```{=html}
<code class=ebnf>
Identifier ::= [a-zA-z_][\w]*
             | '$'[\w]+
             ;
</code>
```

Identifiers start with an alpha character or an underscore and is followed by any alphanum or underscore.

The `$` prefix allows to

- use keywords as identifier
- use number literals as identifier
- start an identifier with numeric characters


```{.styx .numberLines}
_identifier // Identifier
identifier  // Identifier
false       // Keyword
$false      // Identifier
5th_wheel   // error
$5th_wheel  // Identifier
1           // IntegerLiteral
$1          // Identifier
```

## IntegerLiteral

### Grammar

Note that the RHS is a regex.

```{=html}
<code class=ebnf>
IntegerLiteral ::= [1-9][0-9_]*
</code>
```

### Description

Integer litereals are made of decimal digits, optionally separated by an underscore passed the the first decimal digit.

```{.styx .numberLines}
1000000     // ok
1_000_000   // ok
_1_000_000  // error
```

## HexLiteral

### Grammar

Note that the RHS is a regex.

```{=html}
<code class=ebnf>
HexLiteral ::= 0[xX][0-9A-Fa-f][0-9A-Fa-f_]*
</code>
```

### Description

Hex literals are made of a prefix, either `0X` or `0x`, followed by hex digits,
optionally separated by an underscore passed the first hex digit.

```{.styx .numberLines}
0xDEADBEEF      // ok
0X1111_F0F0     // ok
0x_B            // error
```

## BinLiteral

### Grammar

Note that the RHS is a regex.

```{=html}
<code class=ebnf>
BinLiteral ::= 0[bB][01][01_]*
</code>
```

### Description

Bin literals are made of a prefix, either `0B` or `0b`, followed by bin digits,
optionally separated by an underscore passed the first bin digit.

```{.styx .numberLines}
0b1001001   // ok
0B10_10_10  // ok
0b_10       // error
```

## FloatLiteral

### Grammar

Note that the RHS is a regex.

```{=html}
<code class=ebnf>
FloatLiteral ::= [0-9][0-9_]*.[0-9][0-9_]*+((E|e)(\+|-)[0-9_]+)?
</code>
```

### Description

Float literals begins with an optional integral part, followed by a dot and then the fractional part.
Each part can be separated by an underscore passed to first digit of the part. If the inegral part is present
then the fractional part becomes optional. The fractional part my ends by an exponent, that is `e` or `E`,
an optional sign and a an inegral number.

```{.styx .numberLines}
0.0     // ok
.0      // ok
0       // ok
_0.0    // error
._0     // error
0.0E+1  // ok
```

## Sign prefixes

There is no sign prefixes, however the [NegExpression] can be used to negate literals,
which get evaluated during compilation.

## Suffixes

There is no suffixes to denote the type.
This is done using a [CastExpression],
which is always interpreted at compile-time when following a literal and if the cast is for a basic type.

## StringLiteral

### Grammar

Note that the RHS is a regex.

```{=html}
<code class=ebnf>
StringLiteral ::= "([\w\s](\\")*)*"
</code>
```
### Description

String literals

- are always null terminated
- are delimited with double quotes
- can stand on multiple lines
- may contain escape sequences
- can contain UTF8-encoded multi byte characters, excepted [directional overrides](https://www.unicode.org/reports/tr9/tr9-42.html#Explicit_Directional_Overrides).
- have the `char*`{.styx} [Type]

```{.styx .numberLines}
"foo bar"           // foo bar
"foo\tbar"          // foo    bar
"\"foo\" \"bar\""   // "foo" "bar"
```

Recognized escape sequences are

- `\0` to insert a null character
- `\r` to insert a carriage return
- `\n` to insert a line feed
- `\t` to insert a tabulation
- `\v` to insert a vertical tabulation
- `\f` to insert a formfeed page break
- `\\` to insert a backslash
- `\"` to insert a double quotation mark
- `\x[0-9a-fA-F]{2}` to insert an arbitrar byte

## RawStringLiteral

### Grammar

Note that the RHS is a regex.

```{=html}
<code class=ebnf>
RawStringLiteral ::= `\w*`
</code>
```

### Description

Raw string literals

- are always null terminated
- are delimited with back ticks
- can stand on multiple lines
- dont contain escape sequences
- can contain UTF8-encoded multi byte characters, excepted [directional overrides](https://www.unicode.org/reports/tr9/tr9-42.html#Explicit_Directional_Overrides).
- have the `char*`{.styx} [Type]

```{.styx .numberLines}
`raw "string"\0 literal`  // raw "string"\0 literal
```

## CharLiteral

### Grammar

Note that the RHS is a regex.

```{=html}
<code class=ebnf>
StringLiteral ::= '[\w\s]'
</code>
```

### Description

Char literals

- are delimited with single quotes
- contain a single character or an escape sequences (see [StringLiteral])
- have the `char`{.styx} [Type]

## Comments

### Line comments

Line comments start with double slashes and ends at the end of the line.

```{.styx .numberLines}
no_comented1 // commented
no_comented2
```

### Multi line comments

Multi line comments start with a slash followed by an asterisk and ends with an asterisk followed by a slash.
They may spread on several lines.

They can contain UTF8-encoded multi byte characters, excepted [directional overrides](https://www.unicode.org/reports/tr9/tr9-42.html#Explicit_Directional_Overrides).

```{.styx .numberLines}
not_commented1 /* commented
commented
*/ not_commented2
```
