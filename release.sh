set -e
rm -rf release

# prepare assets
make compiler
mkdir release
mkdir release/input
mkdir release/output
cp bin/styx release/input/styx
find library -name *.sx -exec cp '{}' release/input ';'

# archive the assets
SEMVER=$(cat version.txt)
VER=${SEMVER:1:100}
cd release/input
TAR_NAME=styx-$VER-0-linux-x86_64.tar
tar -cf "../output/$TAR_NAME"  $(find . -type f)

# on tag pushes publish a release.
if [ -z "$CI_MERGE_REQUEST_ID" ]; then

    # take the commit body as changelog
    CHANGELOG=$(git show -s --format=%b)
    # convert into a valid JSON string
    CHANGELOG=$(echo "$CHANGELOG" | sed -z 's/\n/\\n/g' | sed -z 's/\"/\\"/g')

    LNK_BASE="https://gitlab.com/styx-lang/styx/-/jobs/$CI_JOB_ID/artifacts/raw/release/output/"
    ASSET_TAR='{ "name" : "'$TAR_NAME'" , "url" : "'$LNK_BASE$TAR_NAME'" , "direct_asset_path" : "/binaries/'$TAR_NAME'" }'
    REQ_DATA='{ "name" : "'$SEMVER'", "tag_name": "'$SEMVER'", "description": "'$CHANGELOG'", "assets": { "links": [ '$ASSET_TAR'] } }'

    curl -g --header 'Content-Type: application/json' \
            --header "PRIVATE-TOKEN: $PUB_STYX_RLZ" \
            --data-raw "$REQ_DATA" \
            --request POST https://gitlab.com/api/v4/projects/15908328/releases
fi
