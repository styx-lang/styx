unit bad_tuples;

// recursive - mutually dependent
alias R1 = (s32, R2);
alias R2 = (s32, R1);
alias R3 = (s32, R1.expand);
alias R4 = (R4, R4);

// bad indexes and ranges
alias I0 = (s32, s32);
alias I1 = I0[0, 0.123];
alias I2 = I0[0, INVALID];
alias I3 = I0[INVALID..INVALID];
alias I4 = I0[0, 10];
alias I5 = I0[0, 10 .. 12];
alias I6 = I0[0 .. 12];

// bad content
alias C0 = (s32, INVALID);

// bad variables type
alias V0 = I0[0..0];
var V0 v0;
alias V1 = (function (), function ());
var V1[1..2] v2;
var V1 v3;

function badExpressions()
{
    (0,0)[0] = 1;
    var auto t0 = (0,0);
    var s32 a;
    t0[a] = 1;
    if (0,1) == (2,3) do {}
    //(0:(),0:());
    (0,0) = (0,0);
    (0,0)[1,2];
    (0,0)[8];
    var auto i = (0, "ab"[] + 0.1);
    a.expand;
}

alias BadTelem = BadSrc[0,1, 1 .. 2];

function badConvFromTuple1()
{
    struct S
    {
        const u32 a = 1;
        var   u32 b;
    }

    var S s = (2,2);
}

function fixMemulateTypeNull()
{
    class A{}
    alias T = (A*, A*);
    var T[] ts;
    ts[0] = (null, null);
}

function badConvFromTuple2()
{
    struct S
    {
        var u32 a;
        static u32 b;
        var u32 c;
        var u32 d;
    }

    var S s = (2,0);
}

function badTupleSlices()
{
    var auto a = (1,2,3,4);
    var auto b = a[0.1 .. 0.8];
    var auto c = a[8 .. 4 + 10];
    var auto d = a[3 .. 1 + 1];
}

function badTupleElementsCast()
{
    // bad length
    (var u8 a1, var u8 b1) = (0,0,0);
    // bad elem
    (var u8 a2, var u8 b2) = (2, 1024);

    var (s32,s32,s32) badLength;
    (var u8 a3, var u8 b3) = badLength;
    alias badElem = badLength;
    (var u32 a4, var u32 b4, var u8* c4) = badElem;

    (var auto a5, var auto b5) = (0,0,0);
}

function badStructCast()
{
    struct S1 {var f64 b;}
    var e1 = (8,8):S1;
    var (f64,f64) e2 = (null, null):(f64,f64);
}

function badAutoElem()
{
    alias TT = (auto,u32);
}

function testDoublePropAssign1()
{
    struct S {var s32 a; var s32 b;}
    var S s;

    s.tupleof.tupleof = (1,2);

    assert(s.a==1);
    assert(s.b==2);
}

function issue703()
{
    (var c0, var c1);
    (var c2, const c3);
    (var c4, const c5) = (0).tupleof;
}

function issue728()
{

    struct C
    {
        const s32 a = 6;
        const s32 b = 1;
    }
    var C c;
    (c.a, c.b) = (8,6);
    c.(a,b) = (8,6);
}

function dotTuple()
{
    struct S {}
    var S s;
    s.(a,0);
}

function issue732()
{
    struct S
    {
        var s32 a;
        var s32 b;
    }

    var S s;

    (s.b,s.a) = (6,1);
    assert(s.a == 1);   // OK

    (s.b,s.a)[1] = 8;
    assert(s.a == 8);  // NG

    (s.b,s.a)[0..2];
}

function issue767()
{
    var u32[+] a;
    a.(length, ptr);
}
