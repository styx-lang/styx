---
header-includes:
title: Generics
---

## Introduction

Generics declarations are partial declarations.

The styx jargon for transforming generics into full declarations is the _application_.

This is what is more commonly called [_monomorphization_](https://en.wikipedia.org/wiki/Monomorphization),
_instantiation_, or _specialization_.

Generic declarations are not usable and are never semantically checked until fully _applied_.

## GenericParameters

### Grammar

```{=html}
<code class=ebnf>
GenericParameters ::= "[" [GenericParameter] ("," [GenericParameter])* "]"
GenericParameter  ::= [Type]? [Identifier]
</code>
```

### Semantics

Add identifiers in the scope of a declaration.

These identifiers can be reused in the declaration headers, in the nested declarations,
types, and expressions.

```{.styx .numberLines}
// define a generic class that derives from the generic argument of `T`
// and contains a `T*` parent variable.
class C[T] : T
{
    var T* parent;
}

// define a generic function that returns the type given by the
// generic argument of `T` and returns the value given by the generic argument of `V`
function f[T,V](): T
{
    var T result = V;
    return result;
}
```

Generic parameters are regular symbols that can conflict with the nested declarations

```{.styx .numberLines}
struct S[t]
{
    var s32 t; // error symbol named `t` already exists in this scope
}
```

Generic parameters cannot be specified for a `@virtual`{.styx} class member function.
This limitation is to keep the length of the virtual table easily predictable.

Generic parameters cannot be specified for a `@foreign`{.styx} declarations.

If the type of a generic parameter is specified then the matching argument can only be a value of that type.

## Generic application

There are 3 ways of applying the generic arguments

### Grammar

```{=html}
<code class=ebnf>
ApplyExpression ::= "apply" "(" [Expression] "," [Expression] ("," [Expression])* ")"

PostApplyExpression ::= [PostExpression] "!" "[" [Expression] ("," [Expression])* "]"

TypeAppliedGeneric ::= [Type] "!" "[" [Expression] ("," [Expression])* "]"
</code>
```

### Semantics

The [ApplyExpression], used as [AliasDeclaration] RHS, is the declarative way to get a generic application.

The two other forms are lowered to [ApplyExpression]s but are more adapted to be used directly in [Expression]s.

The first `apply` argument must resolve to a generic declaration.

The next arguments are applied to its [GenericParameters].

Each must represents either

- an [Expression] that is statically evaluable, e.g `1 + 1`
- a [PrimaryExpression] e.g `true`
- a [Type], e.g `s32`
- a [Declaration], such as [VariableDeclaration], [FunctionDeclaration], [UnitDeclaration], etc.

If an argument matches to a [TypeDeclared](type.html#typedeclared) then the matching declaration
must be fully analyzed.

If a generic function is nested and if it reads variables declared in its parent then
a generic argument cannot resolve to a function that itself reads escapes.

Generic aggregates do not allow arguments resolving to functions that read escapes.

In a copy of the generic declaration, the arguments are associated to the parameters then
the semantics of the declaration are checked.

Application only affects the semantics of [IdentExpression]s and [TypeIdentifier]s,
when one resolves to a generic parameter it is then substitued with the matching argument.

```{.styx .numberLines}
function f[T,V](): T
{
    var T result = V;
    return result;
}

alias f1 = apply(f, u64, 1);
f1();
f![u64, 1](); // alternatively
```

other example, using [TypeAppliedGeneric]:

```{.styx .numberLines}
class C[T] : T
{
    var T* parent;
}

var C![Object] co;
```

For a same set of arguments, an application is always unique

```{.styx .numberLines}
struct S[T]
{
    static var T cnt;
}

var S![s32] s1;
var S![s32] s2;
alias S1 = apply(S, s32);
var S1 s3;

s1.cnt++;
s2.cnt++;
// all have the same type, hence operate on the same static `cnt`
assert(s3.cnt == 2);
```

#### Partial application

Partial application happens when the count of arguments is less than the count of parameters

```{.styx .numberLines}
struct S[T,U,V] { }
alias S1 = apply(S, s32, 1);    // apply s32 to T, 1 to U, nothing to V
var S1 s1;                      // error, generic used as a type
alias S2 = apply(S1, u64);      // T and U are already applied, apply u64 to V
var S2 s2;                      // ok, fully applied
```

Partial application is possible on any argument, not only the last.

The [DollarExpression] is used for this purpose

```{.styx .numberLines}
struct S[T,U,V] { }
alias S1 = apply($, $, 1);          // leave T and U unapplied, apply 1 to V
var S1 s1;                          // error, generic used as a type
alias S2 = apply(S1, u64, u32);     // apply u64 to T and u32 to U
var S2 s2;                          // ok, fully applied
```

#### Implicit Generic Application

Implicit generic application is a transformation that happens during the semantic
analysis of [CallExpression]s.

Since generic functions are not callable without being fully applied then the
compiler tries to apply the supplied arguments types.

```{.styx .numberLines}
function minOf[T,U](T t; U u): T
{
    return t < u ? t else u;
}

function maxOf[T,U](T t; U u): T
{
    return t > u ? t else u;
}

function testIGA()
{
    // same as the more verbose `maxOf![u64,u64](minOf![u64,u64](2, 5), 0)`
    assert(maxOf(minOf(2, 5), 0) == 2);
    // UFC style is also handled
    assert(2.minOf(5).maxOf(1) == 2);
}
```

If the callee looks like a property of a type then the matching type is tried
to apply the generic and the call arguments are ignored

```{.styx .numberLines}
function staticTypeProperty[T](): var auto
{
    static var T value;
    return value;
}

function test()
{
    f64.staticTypeProperty() = 1.0;  // apply(f64, staticTypeProperty)()
    assert(f64.staticTypeProperty() == 1.0);
}
```

If the callee is an aggregate constructor then the arguments are tried
to apply the aggregate.

The arguments types must lexically match to the aggregate generic arguments.

```{.styx .numberLines}
struct S[T, U]
{
    @constructor function create(T t; U u){}
}

function test()
{
    var s1 = S.create(0.1, 2); // -> var s1 = S![f64,u32].create(0.1, 2);
}
```

All the generic arguments must be at least used once and their order does not matter.

```{.styx .numberLines}
struct S[T, U]
{
    @constructor function createGood1(U u; T t){}
    @constructor function createGood2(U u1; U u2; T t){}
    @constructor function createBad(T t){}
}

function test()
{
    var s1 = S.createGood1(1, 0.1); // -> var s1 = S![u32,f64].create(1, 0.1);
    var s2 = S.createGood1(1, 2, 0.1); // -> var s1 = S![u32,f64].create(1, 2, 0.1);
    var s3 = S.createBad(0); // error S![u32] is not fully applied
}
```
