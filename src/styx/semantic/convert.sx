/// Sub-routines used during implicit convertions and casts
unit styx.semantic.convert;

@private import
    styx.ast.expressions,
    styx.ast.types,
    styx.ast.base,
    styx.semantic.expressions,
    styx.session,
    styx.ast.formatter,
    styx.scope,
    styx.ast.declarations,
    styx.symbol,
    styx.token,
    ;

/** Determine the kind of cast when trying to convert an expression to another type.
 * This function is common to `CastExpression` semantics and `implicitConvTo()`.
 *
 * Params:
 *      e       = the expression to convert
 *      toType  = the new type
 */
function getCastKind(@plain Expression* e; @plain Type* toType; bool isExplicit): CastKind
{
    var t1 = e.type.resolved();
    var t2 = toType.resolved();
    if t1:u8* == t2:u8* || t1 == t2 do
        return CastKind.same;

    const isExt        = t1.size < t2.size;
    const isTrunc      = t1.size > t2.size;
    const isSameSize   = t1.size == t2.size;

    var ae = asArrayExp(e);
    var de = asDeclExp(e);
    if !ae && de do
    {
        // An ArrayExp can be hidden by a temporary created to turn it as an lvalue.
        if (var da = de.expression) && (var as = asAssignExp(da)) do
            ae = asArrayExp(as.rhs);
    }

    const isFromInt = t1.isIntegral() || isTypeBool(t1);
    const isFromFp  = t1.isFloating();

    const isToInt   = t2.isIntegral() || isTypeBool(t2);
    const isToFp    = t2.isFloating();

    var tsl1 = asTypeSlice(t1);
    var tsl2 = asTypeSlice(t2);

    var tra1 = asTypeRcArray(t1);
    var tra2 = asTypeRcArray(t2);

    var tsa1 = asTypeStaticArray(t1);
    var tsa2 = asTypeStaticArray(t2);

    var tp1  = asTypePointer(t1);
    var tp2  = asTypePointer(t2);

    var tcp1 = tp1 ? asTypeClass(tp1.modified) else null;
    var tcp2 = tp2 ? asTypeClass(tp2.modified) else null;

    var tcs1 = asTypeClass(t1);
    var tcs2 = asTypeClass(t2);

    var tes1 = asTypeEnumSet(t1);
    var tes2 = asTypeEnumSet(t2);
    var te2  = tes2 ? asTypeEnum(tes2.declaration.asTypeDeclared) else asTypeEnum(t2);

    var tt1 = asTypeTuple(t1);
    var tt2 = asTypeTuple(t2);

    const isToBool = isTypeBool(t2);

    if isTypeVoid(toType) do
        return CastKind.voidCast;

    // class casts, static is always allowed, dynamic must be explicit
    if tcs1 && tcs2 && e.isLvalue() do
        return inheritsOf(tcs1, tcs2) ? CastKind.staticCastV else CastKind.dynCastV;
    if tcp1 && tcp2 do
    {
        // re-use of parent ctor
        if (var ce = asCallExp(e)) && (var fd = getFuncDeclForCall(ce))
        && (FunctionFlag.isCtor in fd.flags) && inheritsOf(tcp2, tcp1) do
            return CastKind.staticCastP;
        return inheritsOf(tcp1, tcp2) ? CastKind.staticCastP else CastKind.dynCastP;
    }

    // always allowed, i.e implicitly
    if isFromInt do
    {
        if isToInt do
        {
            if te2 && !isExplicit do
            {
                var te1 = asTypeEnum(t1);
                if !te1 || te2.declaration.base != te1 do
                    return CastKind.invalid;
            }
            if isExt do
                return CastKind.intExt;
            if isSameSize do
                return CastKind.same;
        }
        if isToFp do
            return CastKind.intToFp;
        if tes2 do
            return CastKind.intToSet;
    }
    if isToBool && (isFromInt || isFromFp || tsl1 || tra1 || tsa1 || tp1 || tes1) do
        return CastKind.toCond;
    if isExt && isFromFp && isToFp do
        return CastKind.fpExt;
    if isTypeNull(t1) && tp2 do
        return CastKind.nullCast;
    if tp1 && tp2 do
    {
        var t = asTypeStaticArray(tp1.modified);
        while t do
        {
            if t.modified.resolved() == tp2.modified.resolved() do
                return CastKind.staToPtr;
            t = asTypeStaticArray(t.modified);
        }
    }
    if tsa1 do
    {
        if ae do
        {
            if tsa2 && tsa1.length() == tsa2.length() do
            {
                const elemCk = getCastKind(ae.items.ptr[0], tsa2.modified, isExplicit);
                return elemCk != CastKind.invalid ? CastKind.staElems else elemCk;
            }
            if tsa1.length() && (var tm2 = tsl2 ? tra2) do
            {
                const elemCk = getCastKind(ae.items.ptr[0], tm2.modified, isExplicit);
                return elemCk != CastKind.invalid ? CastKind.staElems else elemCk;
            }
        }
        if tsl2 && tsa1.modified.resolved() == tsl2.modified.resolved() do
            return CastKind.staToDyna;
        if tes2 && tsa1.modified.resolved() == tes2.declaration.asTypeDeclared do
            return CastKind.staToESet;
        if tra2 && tsa1.modified.resolved() == tra2.modified.resolved() do
            return CastKind.staToRca;
    }
    if var se = asStringExp(e) do
    {
        if tsl2 && isTypeChar(tsl2.modified.resolved()) do
            return CastKind.strToDyna;
        if tra2 && isTypeChar(tra2.modified.resolved()) do
            return CastKind.strToRca;
        if tsa2 && isTypeChar(tsa2.modified) && se.value.text().length == tsa2.length() do
            return CastKind.strToSta;
    }

    if tra1 && tsl2 && tra1.modified.resolved() == tsl2.modified.resolved() do
        return CastKind.rcaToDyna;
    if tsl1 && tra2 && tsl1.modified.resolved() == tra2.modified.resolved() do
        return CastKind.dynaToRca;

    if isToBool && asTypeAggregate(t1) do
        return CastKind.opTrue;

    if ae && !ae.items.length  do
    {
        if tra2 do
            return CastKind.staToRca;
        if tsl2 do
            return CastKind.staToDyna;
        if tes2 do
            return CastKind.staToESet;
    }

    if tp1 && tp2 do
    {
        // allow lambda passed as parameter to return derived class ptr
        var tf1 = asTypeFunction(tp1.modified);
        var tf2 = asTypeFunction(tp2.modified);
        if tf1 && tf2 && tf1.isCovariantWith(tf2) do
            return CastKind.funRet;
    }

    if tt2 && tt2.isSingleElement() == t1 do
        return CastKind.elemToTup;
    if tt1 && asTypeStruct(t2) do
        return CastKind.tupToStruct;
    if tt1 && tt2 && tt1.types.length == tt2.types.length do
        return CastKind.tupElems;

    if tp2 && !tp1 && (var tf = asTypeFunction(tp2.modified)) && asLambdaExp(e)
    && (var tf1 = asTypeFunction(t1)) && tf1.isCovariantWith(tf) do
        return CastKind.lambdaPtr;

    // FIXME: should be only explicit but impConv handles this case too
    // by checking the numeric range
    if isTrunc && isFromInt && isToInt do
        return CastKind.intTrunc;

    // only allowed explicitly
    if isExplicit do
    {
        if isTrunc && isFromFp && isToFp do
            return CastKind.fpTrunc;
        if isFromFp && isToInt do
            return CastKind.fpToInt;
        if tp1 && tp2 do
            return CastKind.bitCast;
        if isFromInt && t1.size == session.ptr_size && tp2 do
            return CastKind.intToPtr;
        if tp1 && t2.size == tp1.size do
            return CastKind.ptrCast;
    }

    return CastKind.invalid;
}

/**
 * Try to implicitly convert an expression to the type of another.
 *
 * Params:
 *      sourceExpr  = the source to convert
 *      targetType  = the target the type
 *      scope       = the scope to analyze newly created expressions
 *
 * Returns: Either a new or a modified expression.
 */
function implicitConvTo(@plain Expression* sourceExpr; @plain Type* targetType; @plain Scope* scope): Expression*
{
    var t1 = sourceExpr.type.resolved();
    var t2 = targetType.resolved();

    function impConvError(): auto
    {
        session.error(scope.filename, sourceExpr.startPos,
            "cannot implicitly convert `%s` of type `%s` to type `%s`",
            format(sourceExpr).ptr, format(t1).ptr, format(t2).ptr);
        suggestParensForCall(sourceExpr, targetType);
        return null;
    }

    if sourceExpr.operator == eopType || ((var te = asTupleExp(sourceExpr)) && te.hasTypes) do
    {
        session.error(scope.filename, sourceExpr.startPos,
            "cannot convert `%s` because it represents a type and has no value", format(sourceExpr).ptr);
        return null;
    }

    switch const ck = getCastKind(sourceExpr, targetType, false) do
    {
    on CastKind.same do
    {
        var tp1 = asTypePointer(t1);
        var tp2 = asTypePointer(t2);
        var tf1 = tp1 ? asTypeFunction(tp1.modified) else null;
        var tf2 = tp2 ? asTypeFunction(tp2.modified) else null;
        /* better diagnostic for this case:
         ```
         alias FunPtr   = (function (A* this))*;
         var FunPtr fp  = &agg.somethingVirtual; // fp(thisParam); would not be a virtual call
         var FunPtr fp2 = (&agg.somethingVirtual):FunPtr; // allow this however
         ```
         */
        if tf1 && tf2 && tf1.declaration.virtualIndex != -1 && tf2.declaration.virtualIndex == -1 do
        {
            session.error(scope.filename, sourceExpr.startPos, "conversion to non-virtual from an `@virtual` function");
            return null;
        }
        return sourceExpr;
    }
    // implicit OK
    on CastKind.staticCastP, CastKind.funRet do
        return (new CastExpression).create(sourceExpr.startPos, sourceExpr, t2, CastKind.bitCast, true);
    on CastKind.intExt, CastKind.fpExt, CastKind.intToFp, CastKind.nullCast, CastKind.staToPtr, CastKind.strToSta do
        return (new CastExpression).create(sourceExpr.startPos, sourceExpr, t2, ck, true);
    on CastKind.intToSet do
    {
        var te = asTypeEnumSet(t2).declaration.asTypeDeclared.asTypeEnum();
        return implicitConvTo(sourceExpr, te.declaration.type, scope);
    }
    on CastKind.staElems do
        return castArrayLiteralElements(sourceExpr, targetType, scope);
    on CastKind.staToESet do
        return castStaticArrayToEnumSet(sourceExpr, asTypeEnumSet(t2), scope);
    on CastKind.strToDyna, CastKind.staToDyna, CastKind.rcaToDyna do
    {
        if var e = castEmptyArrayToSlice(sourceExpr, t2) do
            return e;
        var sl = (new SliceExpression).create(sourceExpr.startPos, sourceExpr);
        return expressionSemantic(sl, scope);
    }
    on CastKind.strToRca, CastKind.staToRca, CastKind.dynaToRca do
    {
        if var e = castEmptyArrayToSlice(sourceExpr, t2) do
            sourceExpr = e;
        var rae = (new RcaAssignExpression).create(sourceExpr.startPos, sourceExpr);
        return expressionSemantic(rae, scope);
    }
    // see git 7baade0ef3fb / #570 : not supported for static @operator(true) and when using the Aggr. name
    // b/c it was more important to detect invalid uses of TypeExp than to allow that
    on CastKind.opTrue do
    {
        session.startGagging();
        var tc = toCondition(sourceExpr, scope);
        session.stopGagging();
        return tc ? impConvError();
    }
    on CastKind.toCond, CastKind.intTrunc do
    {
        var bool allow;
        if var ie = asIntegerExp(sourceExpr) do
        {
            if !t2.isSigned() do
            {
                var v = ie.value;
                const inBoolRange= v <= 1;
                const inU8Range  = v <= u8.max;
                const inU16Range = v <= u16.max;
                const inU32Range = v <= u32.max;
                           allow = (inBoolRange && t2.kind == $bool) ||
                                   (inU8Range   && t2.size >= 8)     ||
                                   (inU16Range  && t2.size >= 16)    ||
                                   (inU32Range  && t2.size >= 32)     ;
            }
            else
            {
                var s64 v = ie.value:s64;
                const inBoolRange= v <= 1;
                const inS8Range  = v <= s8.max  && v >= s8.min;
                const inS16Range = v <= s16.max && v >= s16.min;
                const inS32Range = v <= s32.max && v >= s32.min;
                           allow = (inBoolRange && t2.kind == $bool) ||
                                   (inS8Range   && t2.size >= 8)     ||
                                   (inS16Range  && t2.size >= 16)    ||
                                   (inS32Range  && t2.size >= 32)     ;
            }
        }
        return allow ? (new CastExpression).create(sourceExpr.startPos, sourceExpr, t2, CastKind.intTrunc, true) else impConvError();
    }
    on CastKind.staticCastV do
    {
        session.error(scope.filename, sourceExpr.startPos, "cast to inherited type not allowed on value instances");
        return null;
    }
    on CastKind.elemToTup do
        return castElementToTuple(sourceExpr, t2, scope);
    on CastKind.tupToStruct do
        if var e = castTupleToStruct(sourceExpr, t2, scope) do
            return e;
        else return impConvError();
    on CastKind.tupElems do
        if var e = castTupleElements(sourceExpr, t2, scope) do
            return e;
        // there is a better message output for the element that failed to convert
        else return null;
    on CastKind.lambdaPtr do
        return (new AtExpression).create(sourceExpr.startPos, sourceExpr, t2);

    else return impConvError();
    }
}

/**
 * Try to rewrite the input expression as a condition.
 *
 * Returns:
 *      null if not possible, the input expression if it
 *      was already a comparison, a `CmpExpression*` on success.
 */
function toCondition(@plain Expression* node; @plain Scope* scope): Expression*
{
    var Expression* result;
    switch node.operator do
    {
    on equal, notEqual, greater, greaterEqual, lesser, lesserEqual, andAnd, orOr do
        return node;
    on $null do
        return (new BoolExpression).create(node.startPos, null, false, TypeBool.instance());
    on ass do
    {
        if !node.parens do
        {
            session.error(scope.filename, node.startPos, "assignments cannot be used as condition");
            var be = node:BinaryExpression*;
            if be.lhs.isLvalue() do
                session.additional("- rewrite as `(%s)` if the assignment is really intended", format(node).ptr);
            node.operator = equal;
            session.additional("- rewrite as `%s` to test if both sides are equal", format(node).ptr);
            node.operator = ass;
            node.type = TypeError.instance();
            return node;
        }
        continue on else;
    }
    on amp, pipe do
    {
        if !node.parens do
        {
            session.error(scope.filename, node.startPos, "binary `%s` used as condition requires an explicit comparison",
                tokenString[node.operator].ptr);
            node.type = TypeError.instance();
            return node;
        }
        continue on else;
    }
    else
    {
        assert(node.type, "toCondition called too early, exp should have a type");
        if var e = tryOperatorOverload(BoolExpression.getFalse(), [node][]) do
            return expressionSemantic(e, scope.pushNested());
        const tk = (var t = node.type.resolved()).kind;
        if (var te = asTypeExp(node)) && tk != comma do
        {
            session.error(scope.filename, te.startPos, "expression `%s` represents a type and has no value", format(te).ptr);
            return null;
        }

        switch tk do
        {
        on $enum do
        {
            var ten = asTypeEnum(t);
            const isMember = (var s = node.symbol) && s.kind == enumMember;
            if !isMember && (!(var bt = ten.declaration.base) || !isTypeBool(bt)) do
            {
                session.error(scope.filename, node.startPos, "only enum with `bool` base type can be directly used as condition");
                session.additional("if intended then explicitly use `%s != 0`", format(node).ptr);
                session.additional("or test if it matches to a particular member", format(node).ptr);
                return null;
            }
            continue on $char;
        }
        on $bool do
            result = node;
        // integer type, returns `exp != 0`
        on $u8, $u16, $u32, $u64, $s8, $s16, $s32, $s64, $char, tkEnumSet do
            result = (new CmpExpression).create(
                node.startPos, notEqual, node, (new IntegerExpression).create(node.startPos, null, 0, t), TypeBool.instance()
            );
        // fp type, returns `exp != 0.0`
        on $f32, $f64 do
            result = (new CmpExpression).create(
                node.startPos, notEqual, node, (new FloatExpression).create(node.startPos, null, 0.0, t), TypeBool.instance()
            );
        // nullable type, returns `exp != null:exp_t`
        on mul, $null do
            result = (new CmpExpression).create(
                node.startPos, notEqual, node, (new NullExpression).create(node.startPos, t), TypeBool.instance()
            );
        // arrays, returns `exp.length != 0`
        on tkSlice, tkRca do
            result = toCondition((new LengthExpression).create(node, false, TypeUSize()), scope);
        // tuples, ditto
        on comma do
            result = (new BoolExpression).create(node.startPos, null, asTypeTuple(node.type).types.length != 0, TypeBool.instance());
        else {}
        }
    }
    }
    if !result do
    {
        session.error(scope.filename, node.startPos, "expression `%s` of type `%s` cannot be used as a condition",
            format(node).ptr, format(node.type.resolved()).ptr);
        suggestParensForCall(node, TypeBool.instance());
    }
    return result;
}

/// Cast an array literal in `exp` to another array type using the `other` element types.
function castArrayLiteralElements(@plain Expression* exp; @plain Type* other; @plain Scope* scope): Expression*
{
    // target array type and target elem type
    var tm = asTypeModified(other);
    var te = tm.modified.resolved();
    // get the ArrayExp
    var ae = asArrayExp(exp);
    var de = asDeclExp(exp);
    if !ae && de && (var da = de.expression) && (var as = asAssignExp(da)) do
        ae = asArrayExp(as.rhs);
    // cast elems
    foreach var e in ae.items do
        e = (new CastExpression).create(e.startPos, e, te).expressionSemantic(scope);
    var lvd = (var lve = ae.asLvalue) ? asVariableDeclaration(lve) else null;
    //
    switch tm.kind do
    {
        // [elem] -> [elem:Type]
        on tkSta do
        {
            ae.type = null;
            return ae.expressionSemantic(scope);
        }
        // [elem] -> [elem:Type][]
        on tkSlice, tkRca do
        {
            de?.type = lvd?.type = ae.type = (new TypeStaticArray).create(te, asTypeStaticArray(ae.type).lengthExp);
            var res = tm.kind == tkSlice ? (new SliceExpression) else (new RcaAssignExpression);
            return res.create(ae.startPos, de ? ae).expressionSemantic(scope);
        }
        else assert(0);
    }
}

/// Cast an array literal to a set of enum members
function castArrayLiteralToEnumSet(@plain ArrayExpression* al; @plain TypeEnumSet* tes): Expression*
{
    var u64 set;
    foreach var e in al.items do
    {
        if var em = symToEnumMember(e.symbol) do
            set |= 1 << getIntLiteral(em.value).value;
        else return null;
    }
    return (new IntegerExpression).create(al.startPos, null, set, tes);
}

/// Does CastKind.elemToTup
function castElementToTuple(@plain Expression* exp; @plain Type* t; Scope* sc): Expression*
{
    var pos = exp.startPos;
    var tt = asTypeTuple(t);
    var te = (new TupleExpression).create(pos, [exp]);
    while true do
    {
        tt = asTypeTuple(tt.types.ptr[0]);
        if !tt do
            return expressionSemantic(te, sc);
        te = (new TupleExpression).create(pos, [te]);
    }
}

/// Returns: If exp is an empty array literal the NullExp for an empty slice, `null` otherwise
function castEmptyArrayToSlice(@plain Expression* exp; @plain Type* tm): Expression*
{
    if exp.type:u8* != TypeEmptyArray.instance():u8* do
        return null;
    var tsl = (new TypeSlice).create(asTypeModified(tm).modified.resolved());
    return (new NullExpression).create(exp.startPos, tsl);
}

/// Does CastKind.enumToESet
function castEnumMemberToEnumSet(@plain Expression* node; @plain TypeEnumSet* tes): Expression*
{
    var pos = node.startPos;
    if var em = symToEnumMember(node.symbol) do
    {
        const val = 1:u64 << getIntLiteral(em.value).value;
        return (new IntegerExpression).create(pos, null, val, tes.declaration.type);
    }
    else
    {
        const r = (new LShiftExpression).create(pos, (new IntegerExpression).create(pos, null, 1, tes.declaration.type), node);
        r.type  = tes;
        return r;
    }
}

function castExpressionsToCommonType(var const Expression*[+] items; Scope* scope): Type*
{
    // get elem type
    var Type* et;
    var onlyTypeNull = true;
    foreach const e in items do
    {
        if isTypeNull(e.type) do
            continue;
        onlyTypeNull = false;
        et = e.type.resolved();
        break;
    }
    // NullExp must always be cast to something else
    if onlyTypeNull do
        et = typePointerOf(TypeU8.instance());

    // try the more derived common class type
    var bool wasClassPtrs;
    if (var tp = asTypePointer(et)) && asTypeClass(tp.modified) do
    {
        var ets = (new Type*[+])(items.length);
        foreach (const i; var e) in items do
            ets.ptr[i] = e.type;
        if var t = commonClassPointerType(ets) do
        {
            et = t;
            wasClassPtrs = true;
            foreach var e in items do
                e = (new CastExpression).create(e.startPos, e, t, e.operator == $null ? CastKind.nullCast else CastKind.bitCast, true);
        }
    }

    // otherwise standard impconv
    if !wasClassPtrs do foreach var e in items do
    {
        // prof. shows that the call is most of the time useless,
        // e.g: arrays made of literals almost never require conv.
        if e.operator != eopType && e.type:u8* == et:u8* do { }
        else if var c = implicitConvTo(e, et, scope) do
        {
            e  = c;
            et = c.type.resolved();
        }
        else break et = null;
    }

    return et;
}

/** Cast a static array made of enum members to a EnumSet in the form of
 * an integer expression if the elements are known and as a DeclExp, e.g
 * `[E.e1, E.e2]` ->  `var ESet eset; eset += E.e1; eset += E.e2;`, otherwise.
 */
function castStaticArrayToEnumSet(@plain Expression* staExp; @plain TypeEnumSet* tes; @plain Scope* scope): Expression*
{
    var ae = asArrayExp(staExp);
    var de = asDeclExp(staExp);
    if !ae && de do
    {
        // An ArrayExp can be hidden by a temporary created to turn it as an lvalue.
        if (var da = de.expression) && (var as = asAssignExp(da)) do
            ae = asArrayExp(as.rhs);
    }
    if ae && (var e = castArrayLiteralToEnumSet(ae, tes)) do
        return e;

    var pos     = staExp.startPos;
    var tsa     = asTypeStaticArray(staExp.type);
    var ptrBase = (new PtrExpression).create(staExp, typePointerOf(tsa.modified.resolved()), false);
    var vd      = createVariable(pos, Token.generateIdentifier(), tes, scope, true);
    var id      = varToIdentExp(vd);
    var exps    = (new Expression*[+])(tsa.length());
    foreach (const i; var e) in exps do
    {
        var index = (new IntegerExpression).create(pos, null, i, TypeS32.instance());
        var ie    = (new IndexExpression).create(pos, staExp, index, tes.modified);
        ie.expression  = ptrBase;
        // so that bounds are not checked
        ie.sourceLength= null;
        e              = (new AddAssignExpression).create(pos, id, ie, tes);
    }
    var me = (new TupleExpression).create(pos, exps):Expression*;
    me     = (new IndexExpression).create(pos, me, (new IntegerExpression).create(pos, null, exps.length - 1, TypeUSize()));
    me    .= expressionSemantic(scope);
    return (new DeclExpression).create(pos, vd, me, tes, vd.symbol);
}

/// Does CastKind.tupElems
function castTupleElements(@plain Expression* src; @plain Type* targetType; @plain Scope* sc): Expression*
{
    var te  = asTupleExp(src);
    var tt1 = asTypeTuple(src.type);
    var tt2 = asTypeTuple(targetType);
    var p   = src.startPos;
    var Expression*[+] fromValue;

    if tt2.types.length != tt1.types.length do
        return null;

    foreach (const i; var t) in tt2.types do
    {
        const same = t.resolved() == tt1.types.ptr[i].resolved();
        // src is a tuple literal, individual elems can be directly cast
        if te do
        {
            if same do continue;
            if var e = implicitConvTo(te.expressions.ptr[i], t, sc) do
                te.expressions.ptr[i] = e;
            else return null;
        }
        // src is a variable, create a new tuple literal that extracts and casts each elem
        else
        {
            var ss = (new IntegerExpression).create(p, null, i, TypeU32.instance());
            var ie = (new IndexExpression).create(p, src, ss, tt1.types.ptr[i]);
            if !same do
            {
                if var e = implicitConvTo(ie, t, sc) do
                    fromValue ~= e;
                else return null;
            }
            else fromValue ~= ie;
        }
    }
    if fromValue.length do
    {
        return (new TupleExpression).create(p, fromValue[]).expressionSemantic(sc);
    }
    else
    {
        src.type = tt2;
        return src;
    }
}

/// Does CastKind.tupToStruct
function castTupleToStruct(@plain Expression* e; @plain Type* t; @plain Scope* sc): Expression*
{
    var ts = asTypeStruct(t);
    var t1 = ts.toTypeTuple();
    if (e .= castTupleElements(t1, sc)) do
    {
        if sc.func && e.isLvalue() do
        {
            return forceCast(e, ts);
        }
        else
        {
            e.type = ts;
            return e;
        }
    }
    return null;
}

function commonClassPointerType(Type*[] types): Type*
{
    if types.length < 2 do
        return null;

    while true do
    {
    label L0;

        if types.length == 1 do
            return types.ptr[0];

        var t0 = types.ptr[0].resolved();
        var t1 = types[$-1].resolved();
        const isNull0 = isTypeNull(t0);
        const isNull1 = isTypeNull(t1);

        // [null, A*, B*, ... , A*, null]
        if isNull0 && isNull1 do
        {
            assert(types.length > 2);
            types = types[1 .. $-1];
            continue;
        }

        var tc0 = (var tp0 = asTypePointer(t0)) ? asTypeClass(tp0.modified) else null;
        var tc1 = (var tp1 = asTypePointer(t1)) ? asTypeClass(tp1.modified) else null;

        if !tc0 && !tc1 do
            return null;

        // [null, A*, B*, ... , A*]
        if !tc0 && tc1 && isTypeNull(t0) do
            continue types = types[1 .. $];
        // [A*, B*, ... , A*, null]
        if tc0 && !tc1 && isTypeNull(t1) do
            continue types = types[0 .. $-1];
        // [A*, B*, ..., A*]
        if tc0:u8* == tc1:u8* do
            continue types = types[0 .. $-1];
        if !tc0 || !tc1 do
            return null;

        var Type*[+] c0;
        var Type*[+] c1;
        getInheritanceChain(tc0, c0);
        getInheritanceChain(tc1, c1);
        c0 ~= tc0;
        c1 ~= tc1;
        foreach var ti0 in c0.reverse do
        {
            var ti0r = ti0.resolved();
            foreach var ti1 in c1.reverse do
                if ti0r == ti1.resolved() do
            {
                if tc0 != ti0r do
                    types.ptr[0] = typePointerOf(ti0r);
                continue @L0, types = types[0 .. $-1];
            }
        }
        return null;
    }
}

/** Force a cast using pointer + deref
 *
 * Params:
 *      e           = expression to cast
 *      targetType  = type of result
 *
 * Returns: `*((&e):targetType*)` */
function forceCast(@plain Expression* e; @plain Type* targetType): Expression*
{
    var pos = e.startPos;
    var a = (new AtExpression).create(pos, e, typePointerOf(e.type));
    var p = (new CastExpression).create(pos, a, typePointerOf(targetType), CastKind.bitCast, true);
    return  (new DerefExpression).create(pos, p, targetType);
}
