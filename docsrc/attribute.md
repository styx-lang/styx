---
header-includes:
title: Attribute
---

```{=html}
<code class=ebnf>
Attribute ::= [AbstractAttribute]
            | [AlignAttribute]
            | [AllocatorAttribute]
            | [AsmAttribute]
            | [CdeclAttribute]
            | [ConstructorAttribute]
            | [DeallocatorAttribute]
            | [DeprecatedAttribute]
            | [DestructorAttribute]
            | [FinalAttribute]
            | [ForeignAttribute]
            | [InlineAttribute]
            | [LLVMAttribute]
            | [NoInitAttribute]
            | [NoOptAttribute]
            | [NoReturnAttribute]
            | [OperatorAttribute]
            | [OverloadAttribute]
            | [OverrideAttribute]
            | [PlainAttribute]
            | [PostblitAttribute]
            | [ProtectionAttribute]
            | [ReturnAttribute]
            | [TLSAttribute]
            | [UnittestAttribute]
            | [VirtualAttribute]
            | [UserAttribute]
            ;
</code>
```

## Semantics

Although [Declaration]s rule for the attributes is `Attribute*`{.styx} (zero or more),
it should be a syntax error to specify more than once the same attribute.

This rule cannot be reasonably expressed formally however it is a requirement
for a styx parser to at least emit a warning in case duplicated attributes.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

# AllocatorAttribute

## Grammar

```{=html}
<code class=ebnf>
AllocatorAttribute ::= "@" "allocator" "(" [Expression] ")"
</code>
```

## Semantics

The `@allocator`{.styx} attribute defines the allocator to use in the
scope introduced by the associated declaration.

If a [NewExpression] does not specify an allocator then the attribute argument is used.

```{.styx .numberLines}
@allocator(mi_malloc) function f()
{
    new A;           // rewritten `new (A, mi_malloc)`
    new (A, malloc); // not rewritten
    new A;           // rewritten `new (A, mi_malloc)`
}
```

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

# AbstractAttribute

## Grammar

```{=html}
<code class=ebnf>
AbstractAttribute ::= "@" "abstract"
</code>
```

## Semantics

The `@abstract`{.styx} attribute just like [VirtualAttribute] marks a class member function
as virtual so that its address is written in the virtual table, however its implementation
does no exists until overridden.

It is illegal to allocate or declare an instance of a class containing abstract functions.

```{.styx .numberLines}
class A
{
    @abstract @virtual function f();
}

class B : A {}

var B b1;       // error
var b2 = new B; // error
var B* b3;      // ok, might be a downcast of a derived that overrides `f`
```

It is illegal to call an abstract function.

```{.styx .numberLines}
class A
{
    @abstract @virtual function f();
}

class B : A
{
    @override function f()
    {
        super.f() // error, call to unimplemented function ...
    }
}
```

Abstract functions can be overridden with a covariant declaration

```{.styx .numberLines}
class A
{
    @abstract @virtual function f(): A*;
}

class B : A
{
    @override @abstract function f(): B*;
}
```

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

# AlignAttribute

## Grammar

```{=html}
<code class=ebnf>
AlignAttribute ::= "@" "align" "(" [Expression] ")"
</code>
```

## Semantics

The attribute can only be attached to [FunctionDeclaration]s and local or static [VariableDeclaration]s.

The target declaration cannot specify the [ForeignAttribute].

Takes a single argument that must be evaluable to an [IntegerExpression].

The value must be a power of two lesser than `1 << 32`.

If the value is greater than the default alignment then the target data or code gets aligned to that value.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
# AsmAttribute

## Grammar

```{=html}
<code class=ebnf>
AsmAttribute ::= "@" "asm" "(" [Expression] ")"
</code>
```

## Semantics

The `@asm`{.styx} attribute indicates the asm syntax that is used in the [AsmExpression]s
located in the function it is attached to.

The expression must be an [IdentExpression] that gives either `intel`{.styx} or `att`{.styx} (AT&T).

```{.styx .numberLines}
@asm(intel) function f(): bool
{
    return asm("mov al, 1", "={al}"):bool;
}

@asm(att) function g(): s32
{
    return asm("movb $1, $0", "=r,i", 1):bool;
}
```

By default the AT&T syntax is expected.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

# CdeclAttribute

## Grammar

```{=html}
<code class=ebnf>
CdeclAttribute ::= "@" "cdecl"
</code>
```

## Semantics

Specify that a declaration will be exported with a C name.

Unlike foreign declarations, a body must be specified.

Generic declarations cannot be attributed.

Attributed declarations cannot specify the [ForeignAttribute]. This is because the latter
suggests that the implementation is external while `@cdecl` that it is implemented from the STYX side.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

# ConstructorAttribute

## Grammar

```{=html}
<code class=ebnf>
ConstructorAttribute ::= "@" "constructor"
</code>
```

## Semantics

### Member constructor

The `@constructor`{.styx} marks a member function as a constructor.

Constructor functions

- have a hidden `this`{.styx} parameter that is a pointer with for type the parent aggregate
- are not static
- may have parameters
- have no return type

```{.styx .numberLines}
struct S
{
    var u64 v;
    // rewritten @constructor function create(S* this; u64 v)
    @constructor function create(u64 v)
    {
        this.v = v;
    }
}

var S* s1 = (new S).create(123);
var S  s1 = S.create(123);
```

When the `this`{.styx} argument matches to the `this`{.styx} parameter, i.e a pointer,
the constructor is called and the compiler adjusts the expression so that
the `this`{.styx} argument appears as a return.

```{.styx .numberLines}
var auto a = (new C).create();

// is rewritten

var C* a = malloc(C.sizeof):C*;
C.create(__tmp);
```

When no `this`{.styx} argument is found, the compiler generates a
stack allocated variable, copies the default aggregate initializer
and finally calls the constructor with a pointer to this variable.

```{.styx .numberLines}
var auto a = C.create();

// is rewritten

var C __tmp;
C.create(&__tmp);
var auto a = __tmp;
```

### static constructor

Static constructors can be used to initialize static members and global variables.
They are run automatically before the program `main`{.styx} function.

static constructors

- have no parameters
- have no return type

```{.styx .numberLines}
unit u;

var s32 a;

@constructor static u_init_1()
{
    a = 8;
}

function main(): s32
{
    assert(a == 8);
    return 0;
}
```

When several static constructor are declared, the order of execution is undefined.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

# DeallocatorAttribute

## Grammar

```{=html}
<code class=ebnf>
DeallocatorAttribute ::= "@" "deallocator" "(" [Expression] ")"
</code>
```

## Semantics

The `@deallocator`{.styx} attribute defines the deallocator to use in the
scope introduced by the associated declaration.

If a [DeleteExpression] does not specify a deallocator then the attribute argument is used.

```{.styx .numberLines}
@allocator(mi_malloc)
@deallocator(mi_free)
function f()
{
    var a = new A; // rewritten `new (A, mi_malloc)`
    delete a;      // rewritten `delete (a, mi_free)`
}
```

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

# DeprecatedAttribute

## Grammar

```{=html}
<code class=ebnf>
DeprecatedAttribute ::= "@" "deprecated" ( "(" [Expression]")")?
</code>
```

## Semantics

The `@deprecated`{.styx} attribute is used to mark a declaration as obsolete.

The compiler generates warnings when an [IdentExpression] resolves to a deprecated declaration.

The optional argument must be statically evaluable to a [StringExpression].

That string is then displayed instead of the default message.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

# DestructorAttribute

## Grammar

```{=html}
<code class=ebnf>
DestructorAttribute ::= "@" "destructor"
</code>
```

## Semantics

### Member destructor

The `@destructor`{.styx} attribute marks a member function as a destructor.

Destructor functions

- have a hidden `this`{.styx} parameter that is a pointer with for type the parent aggregate.
- are not static
- dont have parameters

```{.styx .numberLines}
struct S
{
    @constructor function create() { return; }
    // rewritten @destructor function destroy(S* this)
    @destructor function destroy() { return; }
}

var S* s = (new S).create();

// same as `delete s`
s.destroy();
free(s:u8*);
```

Destructors functions dont have to be called explicitly

- destructors of heap allocated objects are implicitly called with the [DeleteExpression].
- destructors of stack allocated objects are automatically called on function exit, in reverse order of the declarations.

### Static destructor

Static destructor can be used to finalize static members and globals.

They have the same requirements as member destructors but dont have any hidden `this`{.styx} parameter.

They are run automatically after the program `main`{.styx} function.

When several static destructors are declared, the order of execution is undefined.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

# FinalAttribute

## Grammar

```{=html}
<code class=ebnf>
FinalAttribute ::= "@" "final"
</code>
```

## Semantics

### Final classes

When attached to a [ClassDeclaration] the `@final`{.styx} attribute indicates that the
class cannot be derived anymore.

```{.styx .numberLines}
unit u;

@final class C  {}
class D : C     {} // error, C is final
```

A class can also be set `@final` using an alias to a generic application

```{.styx .numberLines}
unit u;

class C[T]  {}
@final alias D = apply(C, :u8*);
class E : D {} // error, D is final
```

### Final functions

When attached to a [FunctionDeclaration] it indicates that the function cannot be overridden anymore.

```{.styx .numberLines}
unit u;

class C     {        @virtual function  f(){}   }
class D : C { @final @override function f(){}   }
class E : D {        @override function f(){}   } // error, D.f is final
```

The attribute can be used as an optimization. Under certain circumstances it allows
to perform a call without using the virtual table, saving several indirections.

The optimization is implementation defined but may at least occur if

- the [FunctionDeclaration] found for a [CallExpression] is `@final`{.styx}
- the `this`{.styx} argument passed during a [CallExpression] has for type a pointer to a class that is `@final`{.styx}

```{.styx .numberLines}
unit u;

class C
{
    @virtual function  f(){}
}
@final class D : C
{
    @constructor function create(){}
    @final @override function f(){}
}

function start()
{
    var d = (new D).create();
    test1(d);
    test2(d:C*);
}

function test1(D* d)
{
    // devirtualized:
    // the `this` arg `d`, has the type `D*` and `D` is final
    d.f();
}

function test2(C* c)
{
    // virtual:
    // 1. C.f() is not final.
    // 2. the `this` arg `c`, has the type `C*` and `C` is not final
    c.f();
}
```
### Final enums

When attached to an [EnumDeclaration] it indicates that the enum cannot be extended.

```{.styx .numberLines}
@final enum A
{
    a0, a1
}

enum AB : A // error, A is final
{
    a2
}
```

### Final overloads

See [OverloadDeclaration].

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

# ForeignAttribute

## Grammar

```{=html}
<code class=ebnf>
ForeignAttribute ::= "@" "foreign"
</code>
```

## Semantics

The `@foreign`{.styx} attribute indicates that

- the full declaration comes from a foreign language
- the symbol ABI and mangling is conform to the C language

This is notably used to bind declarations defined in C libraries

```{.styx .numberLines}
@foreign function puts(char* str): s32;

function main(): s32
{
    puts("call libc puts");
    return 0;
}
```

Only, [StructDeclaration]s, [UnionDeclaration]s, [FunctionDeclaration]s, and
[VariableDeclaration]s can be `@foreign`{.styx}.

```{.styx .numberLines}
@foreign alias int = s32; // error
```

Foreign `struct`{.styx}, `union`{.styx} or `function`{.styx} must not specify bodies and foreign `var`{.styx} cant have initializers.

```{.styx .numberLines}
@foreign function puts(char* str): s32 {return 0;} // error
```

The attribute can be applied to local declarations, that implies that the declarations is implictly `static`{.styx}.

```{.styx .numberLines}
function sayHelloTo(char[] name)
{
    @foreign function printf(char* specifier; ...): s32;
    printf("hello %s\n", (name ~ "\0").ptr);
}
```

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

# InlineAttribute

## Grammar

```{=html}
<code class=ebnf>
InlineAttribute ::= "@" "inline" ( "(" [Expression]")")?
</code>
```

## Semantics

The `@inline`{.styx} attribute takes an optional argument that must be statically evaluable as a [BoolExpression].

- `@inline`{.styx} : indicates to the backend that a call to a function can be inlined.
- `@inline(true)`{.styx} : indicates to the backend that a call to a function must be inlined.
- `@inline(false)`{.styx} : the default, indicates to the backend that a call to a function must not be inlined.

```{.styx .numberLines}
@inline(true) function f(){}

function g()
{
    f(); // will be inlined
}
```

Note that inlining only occurs if optimization are enabled (`-O1`, `-O2`, `-O3`).

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

# LLVMAttribute

## Grammar

```{=html}
<code class=ebnf>
LLVMAttribute ::= "@" "LLVM" ( "(" [Expression]  ")" )?
</code>
```

## Semantics

The `@LLVM`{.styx} attribute takes one or no argument.

It is used to mark a [FunctionDeclaration] as matching to an
[LLVM intrinsic](https://llvm.org/docs/LangRef.html#intrinsic-functions).

The optional argument must be a [StringExpression] that allows to identify qualified or overloaded intrinsic.

If not specified then the name of the function must match exactly to the targeted intrinsic name.

If specified then it must match exactly to the intrinsic name.

```{.styx .numberLines}
// https://llvm.org/docs/LangRef.html#llvm-ctlz-intrinsic
@LLVM("ctlz.i8")  function ctlz8(u8 value; bool is_zero_undef): u8;
@LLVM("ctlz.i16") function ctlz16(u16 value; bool is_zero_undef): u16;
@LLVM("ctlz.i32") function ctlz32(u32 value; bool is_zero_undef): u32;
@LLVM("ctlz.i64") function ctlz64(u64 value; bool is_zero_undef): u64;

function main(): s32
{
    { var u8  a = 1; assert(ctlz8(a, false)  ==  7); }
    { var u16 a = 2; assert(ctlz16(a, false) == 14); }
    { var u32 a = 4; assert(ctlz32(a, false) == 29); }
    { var u64 a = 8; assert(ctlz64(a, false) == 60); }
    return 0;
}
```
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

# NoInitAttribute

## Grammar

```{=html}
<code class=ebnf>
NoInitAttribute ::= "@" "noinit"
</code>
```

## Semantics

Can only be attached to local and member [VariableDeclaration]s.

Instruct the backend not to store the default initializer for the variable type,
meaning that the variable value is undefined until firstly assigned.

```{.styx .numberLines}
function f(): auto
{
    @noinit var s32 result;
    return result; // result is undefined
}
```

It is illegal to attach the attribute to a variable that has for type
a non-`@final`{.styx} class containing virtual functions.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

# NoOptAttribute

## Grammar

```{=html}
<code class=ebnf>
NoOptAttribute ::= "@" "noopt"
</code>
```

## Semantics

If attached to a [FunctionDeclaration] then the optimizer is instructed not
to optimize the body.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

# NoReturnAttribute

## Grammar

```{=html}
<code class=ebnf>
NoReturnAttribute ::= "@" "noreturn"
</code>
```

## Semantics

Must be attached to a [FunctionDeclaration].

Indicates that a call to the function does never return.

If coverage is enabled then the function body and the calls to the function are considered uncoverable.

Statements following a call to the function are considered unreachable and compilation stops with an error.

```{.styx .numberLines}
@noreturn function nr()
{
    while true do {}
}

function test(): s32
{
    nr();
    return 0; // error, statement is not reachable
}
```

It is illegal to return from a `@noreturn` function.

```{.styx .numberLines}
@noreturn function nr()
{
    return; // error, cannot return from ...
}
```

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

# OperatorAttribute

## Grammar

```{=html}
<code class=ebnf>
OperatorAttribute ::= "@" "operator" "(" [Expression] (',' [Expression] )*  ")"
</code>
```

## Semantics

### General presentation

The `@operator`{.styx} attribute takes one or more arguments.

It is used to mark [FunctionDeclaration]s and [OverloadDeclaration]s
as alternative to try when built-in operators are used on variable of custom types.

`@operator`{.styx} declarations must be member of [StructDeclaration]s or
[ClassDeclaration]s. Operator overloads are not supported at the
[UnitDeclaration] scope.

Each argument of the attribute indicates, using a dummy expression,
the operator implemented by the member. For example

- `@operator(a+b)`{.styx} indicates that the overload is for an [AddExpression]
- `@operator(a++)`{.styx} indicates that the overload is for a [PostIncrementExpression]

and so on. `@operator`{.styx} arguments are templates of the effect.
Note that they have not to be semantically correct.

Even if usually a single argument is used, two operator overloads are allowed to
share the same implementation.

```{.styx .numberLines}
struct Int32
{
    var s32 value;

    function addBuiltInt32(s32 value): s32
    {
        return this.value + value;
    }

    function addInt32(Int32 other): s32
    {
        return this.value + other.value;
    }

    // allows to add Int32 or s32 to a Int32 using "+"
    @operator(a + b) overload binaryAdd
    {
        addBuiltIn32,
        addInt32,
    }
}

function test()
{
    var Int32 a;
    auto b = a + 42; // rewritten in a first time to `auto b = a.binaryAdd(42);`
                     // then after full semantics to `auto b = a.addBuiltIn32(42);`
}
```

If a function implements several operators then the operator can be statically distinguished
using a generic parameter.

```{.styx .numberLines}
struct GenericOp
{
    @operator(a+b,a-b) function opBinary[T](s64 rhs): s64
    {
        if T == "+" do
        {
            return 1;
        }
        else do if T == "-" do
        {
            return -1;
        }
        else do assert(0);
    }
}

function test()
{
    var GenericOp go;
    assert (go + 1 == 1);
    assert (go - 1 == -1);
}
```

All the unary and binary operators are overloadable, to the exception of those used by

- the [ConditionalExpression] ( `@operator(a ? b else c)`{.styx} )

For binary expressions the LHS is the aggregate that implements
the overload whereas the RHS is the overload parameter.

For the [AddExpression] and the [MulExpression] the overload is also tried in the other way.

```{.styx .numberLines}
struct A
{
    s32 value;
    @operator (a+b) function add(s32 value): s32 { return this.value + value; }
    @operator (a/b) function div(s32 value): s32 { return this.value / value; }
}
var A a;
a + 3; // a.add(3)
3 + a; // a.add(3)
3 / a; // NG, division operands cant be exchanged
```

The DotExpression can be indirectly overloaded by overloading the [IdentExpression]:

```{.styx .numberLines}
struct JSON
{
    var JSON*[+] members;
    var char[+] name;
    var JSONValue value;
    @operator(id) function getMember(char[] identifier): auto
    {
        foreach var auto m in members[] do
            if m.name == identifier do
                return m;
        return null;
    }
}

var JSON js;
var JSON* m = js.request; // js.getMember("request");
```

The [CastExpression] can only be overloaded using a generic function.

```{.styx .numberLines}
struct Pointer
{
    var u8* value;
    @operator(a:T) function castTo[T](): T
    {
        static assert(echo(isPointer, T));
        return value:T;
    }
}

var Pointer p;
var auto ps32 = p:s32*; // p.castTo![s32*]()
```

### Primary expressions

It is generally not possible to overload [PrimaryExpression]s but there are
two exceptions. The first is the [BoolExpression] which can be used to evaluate the
aggregate as a condition:

```{.styx .numberLines}
struct R
{
    @operator(true) // `true` or `false` does not matter here
    function toCondition()
    {
        return true;
    }
}
var R r;
assert(r);
```

The second is the [DollarExpression]. Note that just like for the default expression,
`@operator($)`{.styx} is only tried when used within the square brackets of the
[IndexExpression]s and the [SliceExpression]s.

### Compound expressions

Complex sequences of expressions are not overloadable to the exception of

- `@operator(a[b]=c)`{.styx}, index assignment, which must be used with a `function (I index; V value)`{.styx} or
    `function (I index; V value): R`{.styx}.
- `@operator(a[]=b)`{.styx}, whole slice assignment, which must be used with a `function (V value)`{.styx}
- `@operator(a[a .. b]=c)`{.styx}, specified slice assignment, which must be used with a `function (L lower; U upper; V value)`{.styx}

### Static operators

`@operator`{.styx} functions may be static. For example to implement an alternative value constructor:

```{.styx .numberLines}
struct Point
{
    var s32 x;
    var s32 y;

    @constructor create(s32 x; s32 y)
    {
        this.x = x;
        this.y = y;
    }

    @operator(a(b)) static function initPoint(s32 x; s32 y): Point
    {
        return Point.create(x,y);
    }
}

function test()
{
    var Point p1 = Point.create(44,13);
    var Point p2 = Point(44,13);        // rewritten `Point.initPoint(44,13)`
    assert(p1 == p2);
}
```

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

# OverloadAttribute

## Grammar

```{=html}
<code class=ebnf>
OverloadAttribute ::= "@" "overload" "(" [Expression] ")"
</code>
```

## Semantics

The `@overload`{.styx} attribute indicates that a function is part of an [OverloadDeclaration].

The mandatory argument must be an [IdentExpression].

It is used to implicitly create a set or to retrieve an existing one.

The semantics of implicit sets dont differ from explicit [OverloadDeclaration]s.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

# OverrideAttribute

## Grammar

```{=html}
<code class=ebnf>
OverrideAttribute ::= "@" "override"
</code>
```

## Semantics

The `@override`{.styx} attribute indicates that the entry of virtual table,
initially copied from the parent class, is overwritten with the address of the override.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

# PlainAttribute

## Grammar

```{=html}
<code class=ebnf>
PlainAttribute ::= "@" "plain"
</code>
```

## Semantics

Can only be attached to local variables or function parameters that have a [TypePointer] type.

Indicates that the target is not expected to be `null`.

This is verified by generating runtime checks that are activated with the `--check=plainvars` switch.

The switch adds non-standard restrictions on how the target can be used

1. it cannot be passed by reference to parameters that are themselves not `@plain`
2. it cannot be aliased with [AtExpression]s
3. it must be initialized

If attributed to a parameter then the matching argument is checked against `null` on function entry.

If attributed to a local variable then it is checked against `null` after the mandatory initialization.

In both cases additonal checks are generated after each single reassignement.

A notable effect of the attribute is to make the checks generated with the `--check=dotvars`
and `--check=thiscalls` switches much less systematic.

```{.styx .numberLines}
// compiled with --check=plainvars --check=dotvars
struct S
{
    var s32 a;
    var s32 b;
}

function costly(S* s)
{
    s.a = s.b = 1; // two dotvars checks generated on `s`
                   // but s is never reassigned !
}

function cheaper(@plain S* s)
{
    // check s only once on entry
    s.a = s.b = 1; // no checks on `s` generated
}
```

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

# PostblitAttribute

## Grammar

```{=html}
<code class=ebnf>
PostblitAttribute ::= "@" "postblit"
</code>
```

## Semantics

Can only be attached to non-static, parameter-less, return-less, member functions.

Only one function per aggregate can be attributed.

Indicates to the compiler a function that must be called after the copy of a value-type aggregate instance.

The function is called with a `this`{.styx} argument matching to the copy.

The function is called

- on the LHS of [AssignExpression]s.
- on variables, after initialization.
- on rvalue arguments, once the target function reached.

```{.styx .numberLines}
struct S
{
    var s32 member = 8;

    @postblit function fixupCopy()
    {
        member = 0;
    }
}

var S s1;
s1.member++;
assert(s1.member == 9);

// postblit assign LHS
var S s2;
s2 = s1;
assert(s2.member == 0);

// postblit after init
var S s3 = s1;
assert(s3.member == 0);

// postblit argument
function f(S s)
{
    assert(s.member == 0);
}
f(s1);
```

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

# ProtectionAttribute

## Grammar

```{=html}
<code class=ebnf>
ProtectionAttribute ::= "@" "private"
                      | "@" "protected"
                      | "@" "public"
                      | "@" "strict"
                      ;
</code>
```

## Semantics

Overwrite the current [ProtectionDeclaration] of the attached declaration.

Past the declaration, the previous [ProtectionDeclaration] value is restored.

```{.styx .numberLines}
struct S
{
protection(strict)
    var s32 a;          // strict
    @public var s32 b;  // public
    var s32 c;          // strict
}
```

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

# ReturnAttribute

## Grammar

```{=html}
<code class=ebnf>
ReturnAttribute ::= "@" "return"
</code>
```

## Semantics

The `@return`{.styx} attribute allows to skip or handle the automatic management of
local variables that happens on function returns.

It is possible to annotate either variables or member functions.

### Variables

If a local variable is annotated `@return`{.styx}, the automatic management is simply skipped.

For [TypeRcArray] that means that the array reference count is not touched at all,
skipping the default protection system.

```{.styx .numberLines}
function lessEfficientButWorks(): char[+]
{
    var char[+] r = get();
    // 1. hidden protection: `r.incref`
    // 2. hidden auto-management: `r.decref`
    return r;
}
```

```{.styx .numberLines}
function moreEfficient(): char[+]
{
    @return var char[+] r = get();
    // no hidden refcount operations at all
    return r;
}
```

For classes, structs and unions that means that the destructor function (see [DestructorAttribute]) is not called.

```{.styx .numberLines}
unit demo_return_var;

struct S
{
    var s32 member = 8;
    @destructor function destroy()
    {
        member = 0;
    }
}

function test(): S
{
    @return var S s;
    assert(s.member == 8);
    return s;
}

function main(): s32
{
    var S s = test();
    assert(s.member == 8);
    return 0;
}
```

### Return constructor functions

If a non-static, parameter-less, member function is annotated with `@return`{.styx} then
it is called on return to adjust the aggregate instance and copy it right before the destructor call.

```{.styx .numberLines}
unit demo_return_func;

struct S
{
    var s32 member = 8;
    @destructor function destroy()
    {
        member = 0;
    }

    @return function returnCtor(): S  // copy by return
    {
        var S* s = this;
        // still on what will be destroyed, mutate, adjust refcount, etc.
        return *s;
    }
}

function test(): S
{
    var S s;
    assert(s.member == 8);
    return s;
}

function main(): s32
{
    var S s = test();
    assert(s.member == 8);
    return 0;
}
```
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

# TLSAttribute

## Grammar

```{=html}
<code class=ebnf>
TLSAttribute ::= "@" "tls"
</code>
```

## Semantics

By default a static [VariableDeclaration] is shared by all the threads.

The `@tls`{.styx} attribute indicates that the variable is made available as a distinct entity,
with a distinct storage for each thread.

When attributed to a global instance of a value-type aggregate, the attribute transitively applies to each member
that is a non-static [VariableDeclaration].

```{.styx .numberLines}
@tls var s32 a;             // no members, no problems

struct S
{
    var s32 a;              // depends on the attributes
    static var s32 b;       // always non-TLS
    @tls static var s32 c;  // always in the TLS
}

static var S s1;            // s1.a is not in the TLS
@tls static var S s2;       // s2.a is in the TLS
```

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

# UnittestAttribute

## Grammar

```{=html}
<code class=ebnf>
UnittestAttribute ::= "@" "unittest"
</code>
```

## Semantics

The `@unittest`{.styx} attribute indicates that a function is a unit test.
Annotated functions

- are `static`{.styx} because always declared in the global scope
- have no parameters
- have no return type

```{.styx .numberLines}
unit u;

@unittest function test1() {}                   // OK
@unittest function test2() : s32 {return 0;}    // NG, has a return type

struct S
{
    @unittest static function test3() {}               // NG, not in the global scope
}
```

the unit tests declared in a unit can be obtained as an array of `static function()*`{.styx} using
the `getUnittests`{.styx} command of the [EchoExpression].

`@unittest`{.styx} functions are only available if `--unittest` is a compiler argument.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

# VirtualAttribute

## Grammar

```{=html}
<code class=ebnf>
VirtualAttribute ::= "@" "virtual"
</code>
```

## Semantics

The `@virtual`{.styx} attribute marks a class member function as virtual so that its address
is written in the virtual table.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

# UserAttribute

## Grammar

```{=html}
<code class=ebnf>
UserAttribute ::= "@" "@" [Identifier] ("(" [Expression] (',' [Expression] )*  ")")?
</code>
```

## Semantics

Associates an identifier to zero or several expressions.

User attributes can be statically inspected using the [EchoExpression] and the commands
`children`, `hasAttribute`, and `getAttributeArgs`.
