---
header-includes:
title: File extensions
---

- files with the `.sx` extension are standard source containing a single unit.
- files with the `.sar` extension are styx archives.

## Byte Order Mark

files are UTF-8 encoded and optionally start with the UTF-8 BOM.

## Styx Archives

SAR (styx archives), `.sar` files, is a format that allows to aggregate several
`*.sx` files.

A SAR is made of

- an optional UTF-8 BOM
- optional shebang line
- then 1 or more times
  - mandatory line starting with `§`, continuing with a sx filename, optionally trailing ASCII whites.
  - following lines are code, until EOF or until next line starting with `§`

```{.styx .numberLines}
#!/usr/bin/styx
§main.sx
unit main;
// some code ...
§utils/adt.sx
unit utils.adt;
// some code ...
§utils/io.sx
unit utils.io;
// some code ...
```

This format is inspired by [HAR](https://github.com/marler8997/har) but adapted
to be split faster, considering the character set used to write STYX code.

Note that SAR, is not an official styx file format.

It is affected by a fundamental flaw that is that string literals and
comments in a SAR cant contain the `§` character  on the first column:

```{.styx .numberLines}
#!/usr/bin/styx
§main.sx

const auto str = "
§whatever   // here according to specification a sx start being described
";
```

Consequently it may only be used in the context of the test suite or to simplify bug reports.
