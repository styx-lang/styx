unit lvalue_transition;

struct Dot
{
    var s64 value;
    var Nested nested;
}

struct Nested
{
    var s64 value;
}

function test1()
{
    var u64[2] global_sta = [1,2];
    var u64[2] local_sta  = [1,2];

    assert(global_sta[0] == 1);
    assert(global_sta[1] == 2);
    assert(local_sta[0] == 1);
    assert(local_sta[1] == 2);

    global_sta[0] = 0;
    global_sta[1] = 1;
    local_sta[0] = 0;
    local_sta[1] = 1;

    assert(global_sta[0] == 0);
    assert(global_sta[1] == 1);
    assert(local_sta[0] == 0);
    assert(local_sta[1] == 1);

    foreach const u64 i in 0..2 do
    {
        assert(global_sta[i] == i);
        assert(local_sta[i] == i);
    }

    return;
}

function test2()
{
    foreach (const s64 i; var u32 e) in [0,1] do
    {
        assert(i == e);
    }
    return;
}

function test3()
{
    assert([0,1][0] == [1,0][1]);
    return;
}

function test4()
{
    assert(*&[0,1][0] == 0);
    assert(*&[0,8][1] == 8);
    return;
}

function test5()
{
    var Dot dot;
    dot.value = 8;
    assert(dot.value == 8);
    dot.nested.value = 16;
    assert(dot.nested.value == 16);
    return;
}

function test6()
{
    var s32 h;
    ++++h;
    assert(h == 2);
    ++h;
    assert(h == 3);
    ------h;
    assert(h == 0);
    return;
}

function test7call(){return;}

function test7()
{
    var (static function())* f = &test7call;
    f();
    return;
}

function test52()
{
    var u64[3] a;
    test52var(a);
    assert(a[1] == 1);
    assert(a[2] == 2);
    test52nonVar(a);
    assert(a[1] == 1);
    assert(a[2] == 2);
    return;
}

function test52nonVar(u64[3] p)
{
    foreach (const u64 i; var u64 v) in p do
    {
        p[i] = 0;
    }
    return;
}

function test52var(var u64[3] p)
{
    foreach (const u64 i; var u64 v) in p do
    {
        p[i] = i;
        v = i;
    }
    return;
}

function main(): s32
{
    test1();
    test2();
    test3();
    test4();
    test5();
    test6();
    test7();
    test52();
    return 0;
}
