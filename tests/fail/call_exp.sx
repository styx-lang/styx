unit call_exp;

function fun1() {}
function fun2(s8 p1) {}
function fun3(s8 p1; s8 p2) {}
function fun4(var s8 p1) {}
function fun5(var s8 p1) {}
function fun6(const var s8 p1) {}
function fun7(s16 p1) {}

function getS8(): s8 {return 0:s8;}

function test()
{
    var s8 a1;
    var s16 a2;
    var u8 a3;
    const s8 a4 = 0:s8;

    a1();

    fun1();
    fun2();
    fun2(a1);
    fun2(a3);
    fun2(a2);
    fun3(a1);
    fun3(a1, a1);
    fun3(a1, a2);
    fun2(0:s8 + 1:u8);
    fun7(0:s8);

    fun4(a1);           // OK
    fun4(getS8());      // NG, not lvalue
    fun4(a3);           // NG, formal type different
    fun4(a4);           // NG, arg is const, param is var
    fun5(a4);

    fun6(a4);           // OK
}

function badDecl1(s32 a = 0; s32 b; s32 c = 1) {}
function badDecl2(s32 a = 1; s32 b; ...) {}
function badDecl3(s32 i = 0; ...){}

function badDerefCall()
{
    var function()* f;
    *f();    // (*f)() was meant
}

function testStatic(var static s8 p)
{
    var s8 a;
    testStatic(a);
}

//

class C {}

alias F = function(): C*;

function default(): C*{ return null; }

function getFuncDeclForCallRareCase(F f = null)
{
    var C* c =  f ? f() else default();
}

//

function test389_1(): s32 {return 1;}
function test389_2(): s32 {return 1;}

function test389(): s32
{
    assert(test389_1 == test389_2);
    assert(&test389_1 == &test389_2);
    return 0;
}

//

function testCmpVoidCall()
{
    function f(){}
    if f() == f() do {}
}

//

function betterThisCallFmt()
{
    class A
    {
        function test(): f32 {return 0;}
    }

    var A* a;
    const s32 b = a.test();
    var A c;
    const s32 d = c.test();
}

function betterNumArgExpectedForThis()
{
    class A
    {
        function f(s32 a){}
    }

    var A* a;
    a.f();
}

function maybeCall()
{
    function well(): s32 {return 0;}
    well;
    const s32 a = well;
    const s8[] b = well;
}

function certainlyAbstractCall()
{
    class A
    {
        @abstract @virtual function f();
    }

    class B : A
    {
        @override function f()
        {
            super();
            super.f();
        }
    }
}

function moreMaybeCall()
{
    function fun[T](): bool
    {
        return false;
    }

    struct S
    {
        function fun[T](): bool
        {
            return false;
        }
    }

    if fun![0] do {}
    var S s;
    if s.fun![0] do {}
}

// ---

function test734(){ assert(0); }

function main734()
{
    function test734();
}
