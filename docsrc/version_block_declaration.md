---
header-includes:
title: Version block declaration
---

## Grammar

```{=html}
<code class=ebnf>
VersionBlockDeclaration ::= [VersionExpression] [SingleOrDeclarationBlock] ("else" "do"? [SingleOrDeclarationBlock])?

SingleOrDeclarationBlock ::= [Declaration]
                           | "{" [Declaration]* "}"
                           ;

VersionExpression ::= "version" [VersionOrOrExpression] "do"

VersionOrOrExpression ::= [VersionAndAndExpression] ( "||" [VersionOrOrExpression])?

VersionAndAndExpression ::= [VersionPrimaryExpression] ("&&" [VersionAndAndExpression])?

VersionPrimaryExpression ::= "!" [VersionPrimaryExpression]
                           | "(" [VersionOrOrExpression] ")"
                           | [Identifier]
                           ;
</code>
```

## Semantics

Versions are used to perform conditional compilation.
The [VersionExpression]s are evaluated before the semantic passes and depending on
their values, the nested statements and declarations are pulled or deleted.

### Expressions

The primary identifiers have the `bool`{.styx} type.
An identifier value is `true`{.styx} when it matches to either a predefined
version identifier or to a user defined identifier (passed with the `--vid=` driver option),
otherwise its value is `false`{.styx}.

The other expressions follows the regular semantic rules.

### Scope

version blocks dont introduce scope.

```{.styx .numberlines}
version a || b do
{
    function feature(){}
}
else do
{
    function feature(){ assert(0); }
}

feature(); // compile the version that crashes
           // if `--vid=a` or `--vid=b` were not passed.
```
