---
header-includes:
title: The Styx Programming Language
---

## General

- [presentation](presentation.html)
- [file extensions](file_extensions.html)
- [lexical aspects](lexical_aspects.html)

## The constructs and their semantics

### Top level construct

- [unit declaration](unit_declaration.html)

### Declarations

- [declaration](declaration.html)
- [alias declaration](alias_declaration.html)
- [attribute](attribute.html)
- [class declaration](class_declaration.html)
- [enum declaration](enum_declaration.html)
- [expression alias](expression_alias.html)
- [function declaration](function_declaration.html)
- [import declaration](import_declaration.html)
- [overload declaration](overload_declaration.html)
- [protection declaration](protection_declaration.html)
- [struct declaration](struct_declaration.html)
- [variable declaration](variable_declaration.html)
- [version block declaration](version_block_declaration.html)
- [generics](generics.html)

### Statements

- [statements](statements.html)

### Expressions

- [expressions](expressions.html)
- [unary expressions](unary_expressions.html)
- [pre expressions](pre_expressions.html)
- [post expressions](post_expressions.html)
- [primary expressions](primary_expressions.html)

## Programmer guide

- [Cross compilation](cross_compilation.html)
