/+ dub.sdl:
    name "generate"
    dependency "dmarkdown" version=">=0.3.0"
    stringImportPaths "./"
+/
module generate;

import std.algorithm.iteration;
import std.algorithm.searching;
import std.array;
import std.conv;
import std.file;
import std.path;
import std.process;
import std.string;

import dmarkdown.markdown;

void popFront(ref string str)       {  str = str[1..$]; }
immutable(char) front(string str)   {  return str[0];   }

/// Meta info about a md file
final class Item
{
    __gshared Item[string] rulesToSource;
    __gshared Item[] all;

    /// The markdown filename
    string filename;
    /// the output html filename
    string htmlFilename;
    /// The post-processed markdown filename
    string postFilename;
    /// The rules that are in the bnf code blocks
    string[] bnfRules;
    /// If a rule is a sub rule, i.e not first rule in a same block,
    /// then it likely has no valid anchors and so the link is based on the main rule
    string[string] mainRule;
    /// all the anchors
    string[] anchors;
    /// file content
    string content;
    ///
    this(string filename)
    {
        this.filename       = filename;
        this.htmlFilename   = baseName(filename).setExtension("html");
        this.postFilename   = "post/" ~ baseName(filename);
    }
    /// read the source, return this
    Item readContent()
    {
        content = filename.readText();
        return this;
    }
    /// translate "#" style headers to HTML manually
    Item processHeaders()
    {
        string levelStr;
        string patch(string line)
        {
            if (!line.startsWith('#') || line.startsWith("#!"))
                return line;
            ubyte level;
            while (line.front() == '#')
            {
                line.popFront();
                level++;
            }
            while (line.front() == ' ')
                line.popFront();
            bool isCommonParagraph = line == "Grammar" || line == "Semantics";
            if (level > 1 && !isCommonParagraph)
                levelStr = line.toLower();
            string anchor = line.toLower().replace(" ", "").replace("@", "").replace("`", "").replace("{.styx}", "");
            if (level > 1 && isCommonParagraph)
                anchor = levelStr ~ "." ~ anchor;
            anchors ~= anchor;
            return format(`<a class="anchor" href="#%s"><h%d id="%s">%s<span class="pg">§</span></h%d></a>`,
                anchor, level, anchor, line, level);
        }
        content = content.lineSplitter().map!(a => patch(a)).join("\n");
        return this;
    }
    /// extract the rules, return this
    Item extractGrammarRules()
    {
        string[] codeBlocks;
        string ps(string s) @safe nothrow
        {
            if (s.canFind("::="))
                codeBlocks ~= s;
            return s;
        }

        MarkdownSettings ms = new MarkdownSettings;
        ms.processCode      = &ps;
        ms.flags            = MarkdownFlags.backtickCodeBlocks;
        const string _      = filterMarkdown(content, ms);

        foreach (string cb; codeBlocks)
        {
            string[] blockRules = lineSplitter(cb)
                                .filter!(a => a.canFind("::="))
                                .map!(a => a.split("::=")[0].strip())
                                .array;
            blockRules.each!(a => rulesToSource[a]  = this);
            blockRules.each!(a => mainRule[a] = blockRules[0]);
            bnfRules ~= blockRules;
        }
        version(none)
        {
            import std.stdio;
            writeln(bnfRules.joiner("\n"));
        }

        return this;
    }
    /// put link to rules specified in other mardown sources, return this
    Item hyperlinkRules()
    {
        foreach (kv; rulesToSource.byKeyValue)
        {
            const string anch = kv.value.anchors.canFind(kv.key.toLower) ? kv.key : kv.value.mainRule[kv.key];
            const string link = format("<a href=\"%s#%s\">%s</a>", kv.value.htmlFilename, anch.toLower, kv.key);
            content = replace(content, "[" ~ kv.key ~ "]", link);
        }
        return this;
    }
    /// write the output HTML
    void writeHTML()
    {
        postFilename.write(content);
        ProcessPipes pp = pipeProcess(["pandoc",
            "-o../public/" ~ htmlFilename,
            "--css=style.css",
            "--standalone",
            "--highlight-style=tango",
            "--syntax-definition=styx_kde_syntax.xml",
            "-fmarkdown+raw_attribute-citations",
            "-fmarkdown+lists_without_preceding_blankline",
            "--strip-comments",
            postFilename
        ]);
        if (const int returnStatus = pp.pid().wait())
        {
            import std.stdio;
            writefln("pandoc failed to convert `%s` to `../public/%s` and returned %d",
                postFilename, htmlFilename, returnStatus);
            pp.stdout.byLine().each!(a => stdout.writeln(a));
            pp.stderr.byLine().each!(a => stderr.writeln(a));
        }
    }
}

void cleanOutputDirs()
{
    static const entries = ["post", "../public"];
    foreach (const string e; entries)
    {
        if (e.exists)
            rmdirRecurse(e);
        mkdir(e);
    }
}

void extractStyleAndScripts()
{
    enum css = import("style.css");
    "../public/style.css".write(css);
}

void main(string[] args)
{
    cleanOutputDirs();
    dirEntries("", "*.md", SpanMode.depth).each!(a => Item.all ~= new Item(a));
    // firstly, generate info
    Item.all.each!(a => a
        .readContent()
        .processHeaders()
        .extractGrammarRules()
    );
    // secondly, cross-link the whole
    Item.all.each!(a => a
        .hyperlinkRules()
        .writeHTML()
    );
    extractStyleAndScripts();
}

