---
header-includes:
title: Cross compilation
---

Cross compilation is at this date only possible from Linux X86 and targeting another X86 system.

## Linux to Windows

### Basic example

1. create a source named xcomp.sx

```{.styx .numberLines}
unit xcomp;

function main(): s32
{
    return 42;
}
```

2. produce the MSCOFF64 object

```{.bash}
$ styx xcomp.sx --mvendor=microsoft --mos=windows --be=obj
```

3. link the PE

```{.bash}
$ lld-link /NOLOGO /OPT:REF /OPT:ICF /SUBSYSTEM:WINDOWS /ENTRY:main\
 /OUT:a.exe .styx/xcomp.o
```

4. test in wine

```{.bash}
$ wine a.exe
$ echo $?
```

should give 42

### Integration of the C library

Styx runtime and backend automatically use a few functions of the standard C library (stdc).

Stdc requires initialization and finalization. The user-defined entry point, `main`, must also be
called between the two routines.

While this task is not automatically handled by styx for linux,
it is possible using a special version of [MinGW](https://github.com/ldc-developers/mingw-w64-libs).

1. acquire the libraries

- download [a release](https://github.com/ldc-developers/mingw-w64-libs/releases)
- unpack the content whereever you want
- set the path to the newly extracted `\lib64` sub-folder as environmental variable `MINGW`

2. create a source named xcomp.sx

```{.styx .numberLines}
unit xcomp;

@foreign function printf(char* fmt; ...): s32;

@constructor function initializeUnit()
{
    printf("%s\n", echo(func).stringof);
}

@destructor function finalizeUnit()
{
    printf("%s\n", echo(func).stringof);
}

function main(): s32
{
    printf("%s\n", echo(func).stringof);
    return 0;
}
```

3. produce the MSCOFF64 object

```{.bash}
$ styx xcomp.sx --mvendor=microsoft --mos=windows --be=obj
```

4. link the PE

```{.bash}
$ lld-link /NOLOGO /OPT:REF /OPT:ICF /SUBSYSTEM:WINDOWS /ENTRY:mainCRTStartup\
 /LIBPATH:$MINGW /OUT:a.exe\
 vcruntime140.lib legacy_stdio_definitions.lib\
 /OUT:a.exe .styx/xcomp.o
```

5. test in wine

```{.bash}
$ wine a.exe
```

should output

> initializeUnit
>
> main
>
> finalizeUnit
