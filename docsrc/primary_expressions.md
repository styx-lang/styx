---
header-includes:
title: Primary Expressions
---

## Grammar

```{=html}
<code class=ebnf>
PrimaryExpression ::= [ArrayExpression]
                    | [ApplyExpression]
                    | [AsmExpression]
                    | [BoolExpression]
                    | [CharExpression]
                    | [DeleteExpression]
                    | [DollarExpression]
                    | [EchoExpression]
                    | [FloatExpression]
                    | [FromExpression]
                    | [IdentExpression]
                    | [IntegerExpression]
                    | [VarDeclExpression]
                    | [LambdaExpression]
                    | [NewExpression]
                    | [NullExpression]
                    | [ParenExpression]
                    | [StringExpression]
                    | [SuperExpression]
                    | [SwitchExpression]
                    | [ThisExpression]
                    | [TypeExpression]
                    | [TupleExpression]
                    ;
</code>
```

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## ArrayExpression

### Grammar

```{=html}
<code class=ebnf>
ArrayExpression ::= "[" [Expression] ("," [Expression])* "]"
</code>
```
### Semantics

Array expression type is a [TypeStaticArray] type determined from the common elements type.

The first element that is not a [NullExpression] gives the type.

The other elements must be implictly convertible to this type.

```{.styx .numberLines}
// first elem gives the type
const a = [0:s64, 0:s8];
assert(echo(is, a, s64[2]));

// second elem gives the type
var s64[]* b =  &a;
const c = [null, b, null];
assert(echo(is, c, s64[2]*[3]));
```

The whole array converts implicitly to another [TypeStaticArray] if each individual
element converts implicitly to the type of the other [TypeStaticArray] elements.

```{.styx .numberLines}
// two cases of `u64[2]` to `s8[2]`
// 1. accepted per IntegerExpression conversion rules
const s8[2] a = [0, 1];
// 2. rejected as 2nd element does not convert to `s8`
const s8[2] b = [0, 256];
```

An array expression implictly converts to a value of type [TypeEnumSet] if
all its elements are values of the same [EnumDeclaration].

An array expression made of a single element that is a [RangeExpression] defined
using two values of the same [EnumDeclaration] becomes a set made of all the members
covered by the range.

```{.styx .numberLines}
enum E {e0, e1, e2, e3}
var bool[E] set1 = [e0, e1, e2];
var bool[E] set2 = [e0 .. e2];
assert(set1 == set2);
```

Otherwise element that are [RangeExpression]s defined with integer literals are expanded to
the sequence of values they represent.

```{.styx .numberLines}
var a1 = [0 .. 5, 10 .. 15]; //
assert(a1 == [0,1,2,3,4,5,10,11,12,13,14,15]);
```

Array expressions are rvalues however when an array expression is required as an lvalue,
the compiler inserts a temporary.

```{.styx .numberLines}
const r = [0,2][a];
// rewritten
const __temp = [0,2];
const r      = __temp[a];
```

temporaries are never inserted for nested array expressions.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## AsmExpression

### Grammar

```{=html}
<code class=ebnf>
AsmExpression ::= "asm" "(" [StringLiteral] ("," [StringLiteral] ("," [Expression])* )? ")" (":" [Type])?
</code>
```
### Semantics

The asm expression is a minimal interface to [LLVM inline assembler](https://llvm.org/docs/LangRef.html#inline-assembler-expressions).
It allows to inject target specific assembler code.

The styx interface does not add any specific arguments to the LLVM instruction:

- the first string represents the code
- the second string represents the [operand constraints](https://llvm.org/docs/LangRef.html#inline-asm-constraint-string)
- the [Expression]s represent the input and output variables, e.g the arguments of the "pseudo function" called
- the trailing [Type] represents the asm expression type, e.g the return type of the "pseudo function" called

Note that lvalue arguments are always passed as-is, e.g as if passed to a `var`{.styx} parameter
so the expression sees a pointer.

Without [AsmAttribute] the expected syntax flavor is AT&T.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## BoolExpression

### Grammar

```{=html}
<code class=ebnf>
BoolExpression ::= "false"
                 | "true"
                 ;
</code>
```
### Semantics

Bool expressions are rvalues that have the `bool`{.styx} [Type].

They implictly convert to all other integer type.

When used with arithmetic operators they are promoted to the `u8`{.styx} [Type].

```{.styx .numberLines}
var s32 c = true + true;
assert(c == 2); // promotion, so not 0
```

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## CharExpression

### Grammar

```{=html}
<code class=ebnf>
CharExpression ::= [CharLiteral]
</code>
```

### Semantics

Char expressions have the `char`{.styx} [Type].

```{.styx .numberLines}
var char[+] a;
a ~= '\x09';
a ~= '-';
```

They implictly convert to all other integer type.

```{.styx .numberLines}
var s32 a = 'a';
```

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## DeleteExpression

### Grammar

```{=html}
<code class=ebnf>
DeleteExpression ::= "delete" "(" [Expression] ("," [Expression])? ")"
                   | "delete" [Expression]
                   ;
</code>
```
### Semantics

The `delete`{.styx} expression allows to delete an object previously obtained
with a [NewExpression]. It has the type void.

The second optional expression gives a deallocator. When not specified
it is `malloc`{.styx} from the standard C library. When specified it has to be
a `u8*`{.styx} function that takes a rvalue integer argument with a bitwidth of at least 32.

The first expression gives the object to destroy.

If that object implements a destructor then this destructor is called.

Then the memory is released using the deallocator.

Finally if the object to delete is a lvalue then it is set to null.

```{.styx .numberLines}
@foreign function free(u8* p);

class C { }

var C* c1 = new C;
delete (c1, free);
var C* c2 = new C;
delete c2;
```

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## DollarExpression

### Grammar

```{=html}
<code class=ebnf>
DollarExpression ::= "$"
</code>
```
### Semantics

The expression has three distinct semantics, depending on the use site.

1. Within the square brackets of the [IndexExpression], the [SliceExpression],
or the [TypeElements], the dollar expression expands to the array `.length` property.

```{.styx .numberLines}
var s32 e = $;  // error
var s32[2] a;
a[$-1] = 1;     // a[a.length - 1] = 1;
```

2. Within an [ApplyExpression], or in its postfix variant, the expression is
a wildcard used to keep a generic parameter unapplied.

3 Within a [SwitchExpression] or a [SwitchStatement] the expression represents
the `.max` [DotExpression] property of the type of the switched expression.

```{.styx .numberLines}
var u8 a;
switch a do
{
    on 1,2    do {}
    on 3 .. $ do {} // 3 .. 255
}
```

In all the other contexts, the expression is rejected.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## EchoExpression

### Grammar

```{=html}
<code class=ebnf>
EchoExpression ::= "echo" "(" ([Identifier] | Keyword) ("," [Expression])* ")"
</code>
```

### Semantics

The echo expression is a parametric intrinsic that allows to perform static reflection.

The mandatory first argument is the command, the count of trailing arguments required depends on the command.

During compilation an echo is simplified to a another primary expression.

At run-time, only this new expression is known.

The semantics of the arguments are verified but errors are not reported.
This is because only the new expression is supposed to introduce significant errors.

#### The `argv` command

This commands takes no argument.

The command can only be used in a non-variadic function body.

The command yields, as a TupleExpression, the arguments passed to the current function,
for example the result is suitable as argument for a recursive call, thanks to automatic expansion.

#### The `callerInfo` command

This commands takes no argument.

The command can only be used in a function body.

The command can only be used as argument of a [CallExpression].

The command yields a [TupleExpression] representing the source filename, the current function, and line of the call.

The type of the tuple is defined as `CallerInfo` in the runtime library but can be infered with an `auto` parameter.

If the command is used as default argument then the information are evaluated for each new call.

```{.styx .numberLines}
unit test;

function a(auto info = echo(callerInfo))
{
    // `a` called in `test.sx` by `main` at line `12`
    printf("`%s` called in `%s` by `%s` at line `%d`\n",
        echo(func).stringof, info.expand);
}

function main(): s32
{
    a();
    return 0;
}
```

#### The `children` command

This command takes one argument.

If the argument gives a [Declaration] then the command yields a tuple containing all the direct children
visible from the scope of the echo, and otherwise an empty tuple.

```{.styx .numberLines}
struct S
{
    static var s32 a;
    function f(){}
}
static assert(echo(children, S).length == 2);
```

The idiomatic usage of the command is tuple-unrolling.

#### The `convert` command

This command yields a [BoolExpression] that indicates if the expression given by the first argument
implicitly converts to the [Type] specified by the second argument.

```{.styx .numberLines}
var s32 a;
assert( echo(convert, a, s64));
assert(!echo(convert, a, s16));
```

#### The `filename` command

This commands yields a [StringExpression] that represents the current unit file name.

#### The `func` command

This commands yields an [IdentExpression] that resolves to the current function name.

```{.styx .numberLines}
var s32 a;
function f()
{
    while a++ != 32 do
        echo(func)();
}
```

#### The `func_t` command

This commands yields a [TypeExpression] that represents the current function type.

```{.styx .numberLines}
@foreign function puts(char* str): s32;
// combined with .stringof to pretty print
function f()
{
    puts(echo(func_t).stringof); // outputs "function f()"
}
```

#### The `getAttributeArgs` command

This command takes two arguments.

The first argument must give a [Declaration].

The second argument must be a [StringExpression].

If the declaration has the [UserAttribute] identified by the string then the command yields a [TupleExpression]
containing the attribute arguments, and `false` otherwise.

```{.styx .numberLines}
@@field(0) var s32 a;
static assert(echo(getAttributeArgs, a, "field")[0] == 0);
```

#### The `getLastTemporary` command

This command takes no argument.

This commands yields an [IdentExpression] that matches to the last auto-generated variable known at the `echo`{.styx} position.

It is an error not to use the command in a function body.

It is an error to use the command if the body does not own auto-generated temporaries.

This command exposes compiler internal details and is only designed to styx development.

#### The `getUnittests` command

This command yields a static array of `static function()*`{.styx} that contains the `@unittest`{.styx} functions
declared in the unit passed as argument.

```{.styx .numberLines}
unit u;
@unittest function test1(){}

function main(): s32
{
    const tests = echo(getUnittests, u);
    assert(tests.length == 1);
    foreach const auto test in tests do
        test();
    return 0;
}
```

#### The `hasAttribute` command

This command takes two arguments.

The first argument must give a [Declaration].

The second argument must be a [StringExpression].

The command yields `true` if the declaration has the [UserAttribute] identified by the string and `false` otherwise.

```{.styx .numberLines}
@@field var s32 a;
static assert(echo(hasAttribute, a, "field"));
```

#### The `isArray` command

This command takes one argument that must be an expression.

This command yields a [BoolExpression] that indicates if the argument type is
a [TypeSlice], a [TypeStaticArray], or a [TypeRcArray].

```{.styx .numberLines}
function onlyForArray[T](T t)
{
    static assert(echo(isArray, t), "generic function can only be applied is the argument is an array");
}
```

#### Numeric type checks

The `isFloating` command yields a [BoolExpression] that indicates if the supplied argument is of a floating point type.

The `isIntegral` command yields a [BoolExpression] that indicates if the supplied argument is of an integer type.

The `isSigned` command yields a [BoolExpression] that indicates if the supplied argument is of a signed numeric type.

#### First class type checks

The `isClass`, `isFunction`, `isStruct`, `isUnion`, `isPointer`, `isTuple` and the `isUnit` commands
yields a [BoolExpression] that indicates if the symbol given by the argument is a `class`{.styx},
a `function`{.styx}, a `struct`{.styx}, a `union`{.styx}, a pointer, a tuple, or a `unit`{.styx}.

```{.styx .numberLines}
struct S{}
var S s;
static assert(echo(isStruct, S));
static assert(echo(isStruct, s));
```

#### Storage class checks

The `isConst`, `isStatic`, and the `isVar` commands yield a [BoolExpression] that indicates
if the symbol given by the argument has the `const`{.styx}, `static`{.styx} or `var`{.styx} storage class.

```{.styx .numberLines}
static var s32 a;
static assert(echo(isVar, a));
static assert(echo(isStatic, a));
```

This also works on function parameters

```{.styx .numberLines}
function f(const s32 p)
{
    assert(!echo(isVar, p));
    assert( echo(isConst, p));
}
```

And on the function return type

```{.styx .numberLines}
function f(): var s32
{
    assert(0);
}
static assert(echo(isVar, f));
```


#### The `is` command

The `is` command yields a [BoolExpression] that indicates if the two symbols or the two types given by
the two arguments are the same;

```{.styx .numberLines}
var s32 a;
alias aa = a;
alias at = s32;
assert(echo(is, a, aa));
assert(echo(is, at, s32));
```

When one of the parameter gives a symbol and the other one a type, the type of the symbol is automatically extracted

```{.styx .numberLines}
var s32 a;
alias at = s32;
assert(echo(is, at, a));
```

#### The `line` command

```{.styx .numberLines}
const u32 line = echo(line);
```

This command yields an [IntegerExpression] that gives the line where the echo is located.

It takes no argument.

#### The `return` command

If the command is used with one argument and that this argument represents a function then yields a [TypeExpression]
that wraps the return type of that function.

```{.styx .numberLines}
function f[Fun]()
{
    alias FunReturnType = echo(return, Fun);
}
```

If the command is used with no argument and is located within a function body then yields a [TypeExpression] that wraps
the return type of the current function.

```{.styx .numberLines}
function a(): s32
{
    static function b(): echo(return)
    {
        return 0;
    }
    return b();
}
```

Otherwise an error is issued.

#### The `type` command

This command yields a [TypeExpression] that wraps the type of the expression passed as argument.

```{.styx .numberLines}
function f[Fun]()
{
    alias FunReturnType = echo(type, Fun(0));
}
```

#### The `version` command

```{.styx .numberLines}
const v = echo(version);
```

This command yields a [StringExpression] that represents the compiler version,
formatted following the [semantic versioning](https://semver.org/spec/v1.0.0.html).

It takes no argument.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## FloatExpression

### Grammar

```{=html}
<code class=ebnf>
FloatExpression ::= [FloatLiteral]
</code>
```

### Semantics

Float expressions are rvalues that have the `f64`{.styx} [Type].

They dont implicitly convert to `f32`{.styx}.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## FromExpression

### Grammar

```{=html}
<code class=ebnf>
FromExpression ::= "from" "(" [Identifier] ("." [Identifier])*  ")"
</code>
```

### Semantics

Introduce a scope matching to the [UnitDeclaration] represented by the identifier chain.

```{.styx .numberLines}
import a.b;
const c = a.b.call();

// can be rewritten
const c = from(a.b).call();
```

The expression has an internal [TypeDeclared](type.html#typedeclared) wrapping the matching [UnitDeclaration].

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## IdentExpression

### Grammar

```{=html}
<code class=ebnf>
IdentExpression ::= [Identifier]
</code>
```

### Semantics

IdentExpressions are resolved to either to a declarations or [Type]s.

Single identifiers or first identifiers of a [DotExpression] chain are solved using
a "wild search" that starts from the most nested scope and continue in the parent scopes.
Other identifiers are solved using a "strict search" (only children are considered)
in the scope found by a previous "wild search".

IdentExpressions take the type and the lvalueness of their source symbol.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## VarDeclExpression

### Grammar

```{=html}
<code class=ebnf>
VarDeclExpression ::= [Attribute]* StorageClass+ [Type]? [Identifier] ("=" [Expression])?
</code>
```

### Semantics

Introduces a [VariableDeclaration] in the current scope.

The expression takes the type of the variable and is always a lvalue.

```{.styx .numberLines}
// a Lvalue TupleExpression made of two VarDeclExpressions
(var u32 a1, var u 32 b1) = (1,2);

// an `if` condition that appends two new variables in the scope of the `then` block
if (const a2 = getA()) && (const b2 = getB()) do {}
a2 = 0; // error, unrecognized identifier
```

Default initialization is guaranteed even if the declaration is never evaluated

```{.styx .numberLines}
var u8 e;
var u8* b = &e;
if (var u8* a = b) || (var u8* t = u) do
{
    // even if `u` is not evaluated, `t` is well defined, it is `null`
}
```

To prevent pitfalls, the AST of the initializer is limited in a few ways:

1. if used as operand of a relational expression then the expression must be enclosed between parens.

```{.styx .numberLines}
// without parens, a2 is initialized with `getA() && const b2 = getB()`
if const a2 = getA() && const b2 = getB() do {}
```

2. if used as operand of the shorthand version of the [ConditionalExpression] then the other operand
must also be a [VarDeclExpression].

```{.styx .numberLines}
if (var a = b) ? c    do {} // error
if ((var a = b) ? c)  do {} // still an error
if ((var a = (b ? c)) do {} // ok, that is more likely the intent
if (var a = b) ? (var c = d) do {}  // that is fine too
```

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## IntegerExpression

### Grammar

```{=html}
<code class=ebnf>
IntegerExpression ::= [HexLiteral]
                    | [BinLiteral]
                    | [IntegerLiteral]
                    ;
</code>
```

### Semantics

Integer expressions are rvalues that have the `u32`{.styx} [Type] if their value
is less or equal to `u32.max` and `u64` otherwise.

During implicit conversions the value range is analyzed so that explicit post-[CastExpression] are rarily needed.

```{.styx .numberLines}
var u8 v;
switch v do
{
    on 0    do {} // no need to cast to u8
    on 255  do {} // no need to cast to u8
    else    do {}
}
```
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## LambdaExpression

### Grammar

```{=html}
<code class=ebnf>
LambdaExpression ::= [Attribute]* "function" [LambdaParameters] (":" [TypeReturn])? "=>" [LambdaBody]

LambdaBody ::= [BlockStatement]
             | [Expression]
             ;

LambdaParameters ::= [ShortenedLambdaParameters] / [RegularLambdaParameters]

RegularLambdaParameters ::= [GenericParameters]? "(" [FunctionParameters]? ")"

ShortenedLambdaParameters ::= "(" [ShortenedLambdaParameter] (";" [ShortenedLambdaParameter])* ")"

ShortenedLambdaParameter ::= [Attribute]* [StorageClass]* [Identifier]
</code>
```

### Semantics

Yields a function as an expression.

When required the expression implicitly converts to a function pointer.

```{.styx .numberLines}
var function()* f1 = &function => {}             ; // explicit conv
var function()* f2 =  function => {}:function()* ; // explicit conv, verbose
var function()* f3 =  function => {}             ; // implicit conv
var             f4 =  function => {}             ; // error, variable `f4` cannot be of type ...
```

Just like nested functions, lambda can read allocas defined in the parent frame.

If [ShortenedLambdaParameters] are used then for each shortened paramater an implicit generic parameter
is created and then used to define the type of the parameter

```{.styx .numberLines}
alias L1 = function(t; u; v) => (t + u) / v;
// is equivalent to
alias L1 = function[T,U,V](T t; U u; V v) => (t + u) / v;
```

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## NewExpression

### Grammar

```{=html}
<code class=ebnf>
NewExpression ::= "new" "(" [Expression] ("," [Expression])? ")"
                | "new" [Expression]
                ;
</code>
```
### Semantics

The `new`{.styx} expression allows the allocation of an object, typically on the heap.

The second optional expression gives an allocator.
When not specified it is `malloc` from the standard C library.
When specified it has to be a a `function(const usize sz) u8*`{.styx}.

Given that a type size is limited to `u32.max / 8`{.styx} the parameter can be of type `u32`{.styx} or `u64`{.styx}.

The type of the expression is a [TypePointer] modifying the type given by the first sub-expression.

After the call to the allocator, the memory is initialized with
the type-specific initializer.

```{.styx .numberLines}
var s32* a = new u32;
assert(*a == 0);
*a = 42;
```

`new`{.styx} is rather dedicated to allocate user types, typically classes

```{.styx .numberLines}
@foreign function malloc(usize p): u8*
class C {}
var a = new(C, malloc);
```
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## NullExpression

### Grammar

```{=html}
<code class=ebnf>
NullExpression ::= "null"
</code>
```

### Semantics

The `null`{.styx} expression is a rvalue that implicitly converts to all pointer and reference [Type]s.
It is also their initial value.

```{.styx .numberLines}
var s32* a;
var s8*  b;
var s64  c;
assert(a == null);
assert(b == null);
assert(&c != null);
```

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## ParenExpression

### Grammar

```{=html}
<code class=ebnf>
ParenExpression ::= "(" [Expression] ")"
</code>
```
### Semantics

The paren expression wraps another expression.

It has the type of its sub-expression, its lvalueness and complies to its conversion rules.

It's usually used to overcome precedence rules or more simply for readability of combined [AndAndExpression]s and [OrOrExpression]s.

```{.styx .numberLines}
const a = (b + c) * 8;
```

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## StringExpression

### Grammar

```{=html}
<code class=ebnf>
StringExpression ::= [StringLiteral]
                   | [RawStringLiteral]
                   ;
</code>
```

### Semantics

String expressions have `char*`{.styx} for [Type] and are always null terminated.

Working with strings literal however does not require `strlen`{.styx}.

_Note: The following conversion rules refer to `length` as the count of UTF-8 code units in the literal_

String expressions implictly convert to

- `char`{.styx} [TypeStaticArray] of same length
- `char`{.styx} [TypeSlice] and [TypeRcArray]

```{.styx .numberLines}
const char[5] season = "été";   // OK
const char[2] hi     = "hello"; // NG, length mismatches

const char[] slice   = "slice"; // create a slice of a `.length` of 5 and a `.ptr` equals to the default `char*` value
const char[+] rca    = "rca";   // allocate a refcounted array, using `fromSlice` from the runtime library
```
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## SuperExpression

### Grammar

```{=html}
<code class=ebnf>
SuperExpression ::= "super"
</code>
```

### Semantics

The SuperExpression is only allowed in non-static member functions of derived classes.

It allows the access to the inherited members.

#### Variables

If `super`{.styx} is used to access a parent variable then the semantics are equivalent to
a [DotExpression] that contains a [Type], i.e a downcast is performed on `this`{.styx}.

```{.styx .numberLines}
class C1
{
    var s32 a;
}
class C2 : C1
{
    var s32 a;
    function f()
    {
        super.a = 0; // equivalent to `(this:C1*).a = 0`
                     // or w/ the DotExp flavor,  `this.C1.a = 0`
    }
}
```

#### Functions

If `super`{.styx} is used to access a parent function then the parent virtual table
of the class given by the this parameter is used to perform a devirtualized call.

```{.styx .numberLines}
class C1
{
    @virtual function t(): auto {return true;}
}
class C2 : C1
{
    @override function t(): auto {return false;}

    function test()
    {
        assert(super.t()); // `this` is C1*, use C2 vtable entry for t().
        assert(!this.t());
    }
}
```

#### Shorthand call syntax

If `super`{.styx} is used as sub-expression of the [CallExpression]
then the call is performed on the function that the current function overrides.

```{.styx .numberLines}
@override function test()
{
    super(); // shorthand for `super.test()`
}
```
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## SwitchExpression

### Grammar

```{=html}
<code class=ebnf>
SwitchExpression ::= "switch" [Expression] "do" "{" [MatchExpression] ("," [MatchExpression])* ","? [MatchElseExpression]? "}"

MatchExpression ::= [Expression] ("," [Expression])* "=>" [Expression]

MatchElseExpression ::= "else" "=>" [Expression]
</code>
```

### Semantics

Evaluates the first expression as value `v`.

The type of `v` must be either integral, `char[]`, or `char[+]`.

If `v` type is integral then the [MatchExpression]s LHS must be statically simplifiable to [IntegerExpression]s.

If `v` type is `char[]` then the [MatchExpression]s LHS must be statically simplifiable to [StringExpression]s.

If one of the [MatchExpression] LHS is equal to `v` then yields its RHS.

If none of the [MatchExpression] LHS is equal to `v` then, if specified, yields the [MatchElseExpression] and
otherwise generates a runtime error, which does not depend on any `--check`.

All the [MatchExpression]s LHS must be implicitly convertible to `v`.

The expression takes for type the common type of the [MatchExpression]s RHS.

Example:

```{.styx .numberLines}
// SwitchExpression used to save repeated returns;
function f(const s32 v): f64
{
    return switch v do { 0 => 0.1, 1 => 0.2, else => 0.3 };
}
```

If all the [MatchExpression]s are lvalues then the expression is also a lvalue, otherwise it is a rvalue.
Note that since statements take prcedence over expressions, the switch expression must be
surrounded by parens in order to be used a LHS in an [ExpressionStatement] made of an [AssignExpression]:

```{.styx .numberLines}
// SwitchExpression used to select an assign LHS
var f64 a;
var f64 b;
var f64 c;
var s32 v;
(switch v do { 0 => a, 1 => b, else => c }) += 0.4;
```

To transform the `else`{.styx} match in a custom runtime error it is possible to use a [LambdaExpression] that
returns the right type but never actually returns, using `assert(0)`{.styx}:

```{.styx .numberLines}
var s32 b;
const a = switch b do
{
    0,1  => 2,
    2,3  => 4,
    else => function(): u32 => { assert(0, "unexpected value"); }()
};
```

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## ThisExpression

### Grammar

```{=html}
<code class=ebnf>
ThisExpression ::= "this"
</code>
```
### Semantics

#### Non-static Member function

In a member function that is not `static`{.styx}, `this`{.styx} represents the parent aggregate instance
as it matches to an hidden parameter, that is identified as `this`{.styx}.

```{.styx .numberLines}
struct S
{
    var s32 a;
    function setA(s32 a)
    {
        // resolves the LHS "a" using `this`,
        // resolves the RHS "a" using the parameter, before trying implicitly on `this`.
        this.a = a;
    }
}
```

It is illegal to reassign `this`{.styx}.

#### Static Member function

In a member function that is `static`{.styx}, `this`{.styx} resolves to the parent declaration, allowing to
access its static members with a "strict search", for example when disambiguation is required.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## TypeExpression

### Grammar

```{=html}
<code class=ebnf>
TypeExpression ::= [Type]
</code>
```

### Semantics

Type expressions wraps a [Type].

After semantics the expression has the type of the contained type.

If a [Type] has the same syntax as an [Expression] then the disambiguation
happens during semantics.

```{.styx .numberLines}
alias S8 = s8;
// DotExp LHS is an IndexExp, transformed to a TypeExp
// wrapping a `s8[2]` during expression semantics
const a = S8[2].sizeof;
```

A TypeExpression is rejected unless used in one of the following context:

#### Type properties

Type expressions are allowed as [DotExpression] LHS when the goal is to get a property.

```{.styx .numberLines}
const a = s32.sizeof; // contribute to create an IntegerExp
```

#### Generics

Type expressions are allowed as argument during generic application.

```{.styx .numberLines}
struct S[T]
{
    static var T t;
}

function f()
{
    S![s32].t = 1; // contribute to create a StructDeclaration
}
```

#### Echoes

Type expressions are allowed as [EchoExpression] argument

```{.styx .numberLines}
const bool a = echo(is, s32 , s64); // contribute to create a BoolExpression
```

#### `new`

Type expressions are allowed as first sub-expression of the [NewExpression]

```{.styx .numberLines}
var a = new s32; // indicates the type of a pointer
```

### Tuple element

Type expressions are allowed as element of the [TupleExpression] making the tuple itself
subject to one the previously specified context.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## TupleExpression

### Grammar

```{=html}
<code class=ebnf>
TupleExpression ::= "(" [Expression] ("," [Expression])+ ")"
</code>
```

### Semantics

The expressions form the equivalent of a structure instance.

The expression type is a [TypeTuple] made using each expression type.

```{.styx .numberLines}
var (u64, f64) a = (0, 0.0);
```

The [ParenExpression] takes precedence over tuples but single values are
implicitly convertible to a tuple consisting of single elements of the same type.

Similarly tuples consisting of single elements are convertible to the
value type.

```{.styx .numberLines}
alias T0 = (u64, u64);   // (u64) is a TypeUnambiguous
alias T1 = T0[0];

var T1 a = (0); // paren expression but is converted to a `(u64)` tuple

var u64 b = a;
```

Additionally a [ParenExpression] can use the [DotExpression] property `tupleof`
to build a single value tuple.

```{.styx .numberLines}
struct S { u64 member; }
var S s = (1).tupleof;
```

A tuple can be used as [AssignExpression] left hand side if all its elements are lvalues

```{.styx .numberLines}
var u64 a;
var u64 b;
(a,b) = (1,2);
assert(a==1 && b==2);
(b,0) = (0,1); // error
```

A tuple implictly converts to a `struct`{.styx} if its members implicitly
convert to the struct non-static members.

```{.styx .numberLines}
struct Point[T] {var T x; var T y;}
var Point![u64] p = (1,2);
assert(p.x == 1);
assert(p.y == 2);
```

If a tuple contains [TypeExpression]s then it cannot be used as value.

Such tuples are essentially used to pass variadic arguments to a generic

```{.styx .numberLines}
function f[T](T t)
{
    static assert(echo(isTuple, T), "generic argument must be a tuple");
}

f((12,0.5));  // f![(u32, f64)](12,0.5)
```
