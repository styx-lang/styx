---
header-includes:
title: Enum declaration
---

## Grammar

```{=html}
<code class=ebnf>
EnumDeclaration ::= [Attribute]* "enum" [Identifier] ( ":" [Type])? "{" (EnumMember ",")* EnumMember ","? "}"

EnumMember ::= [Attribute]* [Identifier] ( "=" [Expression] )?
</code>
```

## Semantics

An EnumDeclaration allows to declare constant expressions in a named group.

If the [Type] is not specified then each member is of type `u32`{.styx}.
If specified then it must be one of the integer type, `bool`{.styx}, [TypeAuto], or another `enum`{.styx}.

When the Expression used to define the value of a member is not specified then
the member value is set to its rank, starting from the last member that was initialized.

```{.styx .numberLines}
enum EnumA: u8
{
    a0,     // 0
    a1,     // 1
    a2 = 8, // 8
    a3      // 9
}
```

When the Type is another `enum`{.styx} it extends it.

```{.styx .numberLines}
enum Foo
{
     a0 = 1,
     a1
}

enum Bar: Foo
{
    a2 // 3
}

function extendedEnum()
{
    Bar bar;
    assert(bar.a0 == 1);
    assert(bar.a1 == 2);
    assert(bar.a2 == 3);
}
```

When the Type is `auto`{.styx} the smaller unsigned integer type allowing to contain
the member with the highest value is selected.

```{.styx .numberLines}
enum Bar: auto
{
    member1 = (1 << 16) - 1;
}

function autoEnum()
{
    Bar b;
    assert(echo(is, b, u16));
}
```

Members implicitly convert to the enumeration type.

```{.styx .numberLines}
const u8 a = EnumA.a0;
```

Members implictly convert to integer types of same or greater size.

Members cannot specify a particular protection attribute.

Variables typed as enum can only be used as condition if the base type is `bool`.

Variables typed as enum take the enum `.min` property as initializer.

```{.styx .numberLines}
enum E { e = 1 }
var E e;
assert(e == 1);
```

Enumerations are not generated as a whole by the backend.
Their members, however, when used in other expressions,
are replaced by the rvalue they represent.

In several contexts, the members can be used without using the declaration
name. Supported contexts are

- [VariableDeclaration]s initializer and if the type is not `auto`{.styx}
- the RHS of all assign, binary, and binary assign expressions
- the argument of non-UFC, non-overloaded, non-member [CallExpression]s
- the expressions contained in [OnMatchStatement]s
- the expressions giving the iterable in [ForeachStatement]s
- the subscripts of an [IndexExpression]
- the expression of a [ReturnStatement] and if the the function return is not `auto`{.styx}
- the expression passed to apply a generic parameter that expects an enum value

```{.styx .numberLines}
unit enum_name_inference;

enum E { e1, e2 }

alias ESet = bool[E];

function testParam(E p){}

function main(): s32
{
    // decl type -> init
    var ESet es = [e1];
    var E e = e1;
    // set type -> subscript
    assert(es[e1]);

    // binary left -> binary right
    const auto n = E.e1 + e2;
    e   = e2;
    e   &= e1;
    es  += e1;

    // straight call, fd is available early -> passed argument
    testParam(e2);

    // switch condition -> on match expression
    switch e do
    {
        on e1 do {}
        on e2 do {}
    }

    // foreach var type -> foreach aggregate exp
    foreach (const E el) in e1 .. e2 do {}

    // generic parameter -> generic argument
    struct TestGenericArg[E e] { }
    var TestGenericArg![e1] tgae;

    return 0;
}
```

Normal [IdentExpression] resolution rules are not affected.
The parent enum is only tried as last resort.
