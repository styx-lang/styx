---
header-includes:
title: Presentation
---

## Hello world

```{.styx .numberLines}
@foreign function printf(char* specifier; ...): s32;

function main(s32 argc; char** args): s32
{
    printf("hello world\n");
    return 0;
}
```

## General presentation

Styx is a system-level programming language.
It has a simple [LL](https://en.wikipedia.org/wiki/LL_grammar)(1) grammar.
Its type system is strong, static, optionally inferred, and named.
Its programming paradigms are imperative, structured, and object oriented.

## Self-hosted

The first motivation behind this project was to write a self-hosted language.
It was made possible using a small subset of the _"egg"_ language, D, consequently, the feature set is quite similar to this subset.

The goal was reached _remotely, on a VM_ [on January the 24 of 2022](https://gitlab.com/styx-lang/styx/-/merge_requests/70)
using gitlab features that allow to publish binaries when a git tag is pushed.

Since, the project uses a script that downloads the latest published binaries based on this system,
in order to test new features on commit pushes, and to build the latest tag.

## Influences

Styx is mostly influenced by D and to a lesser extent by ObjectFPC.

### SwitchStatement

The [SwitchStatement], despite of of its syntax is influenced by the Pascal
`case ... of` construct, especially the implicit _break_.

Additional control flow statement have been added to overcome the
limitation of the model, see [BreakOnStatement] and [ContinueOnStatement].

### The `var`{.styx} storage class

In comparison with D, where most variable declarations have no storage classes,
Styx variables always have one, e.g the `var`{.styx}, borrowed from ObjectFPC.
`var`{.styx} is convenient to be re-used as parameter storage classes, e.g to denote "passed by reference".

### Pascal `set of`

[TypeEnumSet] is a assumably inspired by the Pascal [set of](https://wiki.freepascal.org/Set) construct.
As styx type system is more advanced, no special keyword is necessary.

Set types can be declared inline or using an [AliasDeclaration].

### D version enhanced

`version`{.styx}, borrowed from D, and which is used to for conditional compilation,
allows logical expression, e.g

```{.styx .numberLines}
version linux && x86_64 do {} else do {}
```

### More conditional and optional expressions

In addition to the conditional expression, styx has

- shortened conditional expression : `a ? a else b`{.styx} can be rewritten `a ? b`{.styx}, (with single evaluation of a)

And

- optional assign : `a ?= value`{.styx} , (with single evaluation of a)
- optional member access :  `a?.b`{.styx}, yielding the default value for `b` type if `a` is false.
- optional member call : `a?.b()`{.styx}, yielding the default value for the return type of `b` if `a` is false.

Even if [existing since 2005](https://en.wikipedia.org/wiki/Safe_navigation_operator#cite_note-6.1._Safe_navigation_operator-1)
they never found their way in D, although discussed each semester (nb: this is an estimation) on the news group.

Optional expressions make the source code more readable but if used naively, if used systematically,
then they lead to slower execution time and bigger binaries.
However they still have their place in short programs, scripts, or in cold spots.

They should simply be considered as additional tools in the expression toolbox.

### `@` attributes

Unlike D that mixes the `@` style and the keyword style for its attributes,
all the Styx attributes begin with `@`.

This decision is motivated by this extreme example in D:

```d
struct S
{
    const const(S) fun(const int p) const;
}
```

### D member function pointer type-correctness

D member function pointers [are not correct](https://issues.dlang.org/show_bug.cgi?id=2672).

This is fixed in Styx, as the implicit `this`{.styx} parameter can be explictly declared,
it is then possible to call a member function pointer, optionally with UFC, on any valid
object instance, without patching a fat pointer.

See the [function declaration](function_declaration.html#thethisparameter) specifications.

### Fixed the restriction on the count of declarations that can be used as condition

Instead of an expression, D can use a variable declaration as condition, but only as-if,
i.e as a special case allowed in the statement

```d
if (auto a = getA())
    if (auto b = getFrom(a))
{
    // use a, use b
}
```

styx introduces _in-situ_ variable declarations, a new type of primary expressions (the [VarDeclExpression]),
so that

```d
if (var auto a = getA()) && (var auto b = getFrom(a)) do
{
    // use a, use b
}
```

can be written instead of the D version.

### Linting

While styx agrees with D policy about compiler warnings, which can be described as "it is an error or it is okay",
styx integrates an optional linter.

That linter does not warn about potentially harmfull code but suggests writing better code.

### Other

There are other details adopted by Styx that are not really influences but rather match to current tendencies,
such as

- support for [uniform function calls](https://en.wikipedia.org/wiki/Uniform_Function_Call_Syntax) (UFC)
- less parenthesis.
- dropped the `for` statement.
- dynamic arrays with automatic reference counting.

Killing parentheses, to keep the grammar simple, implied using the `do`{.styx} keyword.
See [ForeachStatement], [IfElseStatement] for example.

## Styx specific features

### Explicit overloads

In styx, all declarations located in a given scope must have an unique identifier, including functions.

This does not prevent to have function overloads.

They are build as a group, itself uniquely identified. See [OverloadDeclaration].

### Optional break/continue expression.

No need to open a block to execute an expression and then break a loop.
The [BreakStatement] and the [ContinueStatement] syntax allows to write the block in one line.

### UFC on `var`{.styx} parameter

Void calls can be chained if the first parameter is `var`{.styx}.
