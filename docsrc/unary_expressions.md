---
header-includes:
title: Unary Expressions
---

## Grammar

```{=html}
<code class=ebnf>
UnaryExpression ::= [PreExpression]
</code>
```
