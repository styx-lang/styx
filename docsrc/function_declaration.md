---
header-includes:
title: Function declaration
---

## Grammar

```{=html}
<code class=ebnf>
FunctionDeclaration ::= [Attribute]* [StorageClass]* "function" [Identifier]? [GenericParameters]? "(" [FunctionParameters]? ")" (":" [TypeReturn])? [FunctionBody]

TypeReturn ::= "var"? [Type]

FunctionParameters ::= [FunctionVariadicParameter]
                     | [FirstFunctionParameter] (";" [FunctionParameter])* ((";" [FunctionVariadicParameter]) | ";" )?
                     ;

FirstFunctionParameter ::= [ThisParameter]
                         | [FunctionParameter]
                         ;

FunctionParameter ::= [Attribute]* StorageClass* [Type]? [ParameterName] ("=" [Expression])?

ThisDotParameterName ::= "this" "." [Identifier]

ParameterName ::= [Identifier]
                | [ThisDotParameterName]
                ;

ThisParameter ::= [StorageClass]+ [Type] "this"

FunctionVariadicParameter ::= "..."

FunctionBody ::= ";"
               | [BlockStatement]
               | [AsmBody]
               ;

AsmBody ::= "=" "asm" [Expression] ";"

</code>
```

## Semantics

### Attributes

See [Attribute] for a list of the attributes allowed on function declarations.

### Storage classes

Functions only support the `static`{.styx} storage class.

`static`{.styx} does not participate to the function type, it only indicates semantic restrictions.

In aggregates, `static`{.styx} can be used to indicate that the function does not require a `this`{.styx} argument.

For a nested function, `static`{.styx} indicates that the body cannnot access to local variables declared in a parent frame.

### Function name

Just like other declarations the [Identifier] must be unique in the scope.
This aspect does not prevent functions overloads, which in styx, are explicitly
declared using an [OverloadDeclaration].

If a function declaration is used to declare a [TypeFunction] then the name is optional.

### The `this`{.styx} parameter

The `this`{.styx} parameter is automatically created for the member functions that are not `static`{.styx}
however it can be explicitly declared when required, e.g for an [AliasDeclaration].

```{.styx .numberLines}
struct S { }
// Func is the type of parameter-less S member function returning an s8
alias Func = function (S* this): s8;
```

The `this`{.styx} parameter must always have for type a pointer to the parent aggregate and no storage class.

### Regular parameters

Parameters are declared just like variables to the difference that the storage classes are optional.

#### No storage class

If no storage class is specified then the parameter is passed by value.
[IdentExpression]s that resolve to those parameters are then rvalues.

Certain expressions such as the [AssignExpression], the [AtExpression], the [IndexExpression]
but also the `.length` property, the `.ptr` property, etc. have for effect to turn the parameter to an lvalue,
however this is made by local copy and has no impact on the variables passed as arguments, e.g

```{.styx .numberLines}
function f(ssize v): ssize
{
    v++;    // v is assigned but has no address to create the store
    v*=8;
    return v %= 100;
}
```

is rewritten

```{.styx .numberLines}
function f(ssize v): ssize
{
    var ssize cp = v;
    cp++;
    cp*=8;
    return cp %= 100;
}
```

#### `var`{.styx}

If a parameter is `var`{.styx} then its matching argument is passed by reference.
[IdentExpression]s that resolve to those parameters are then lvalues.

#### `static`{.styx}

If a parameter is `static`{.styx} then its matching argument must also be `static`{.styx}.
This is only verifiable if the argument is a `static`{.styx} [VariableDeclaration].

### C-style variadic parameter

A single [FunctionVariadicParameter] is allowed.
It indicates that 0 or more additional arguments are allowed, following the last regular parameter.

```{.styx .numberLines}
@foreign function printf(char* specifier; ...): s32;
```

### `this` dot parameter name

If a parameter name is prefixed with `this.` then the compiler is instructed to insert
an [AssignExpression] with for RHS the parameter name and for LHS the matching `this` member.

```{.styx .numberLines}
struct S
{
    var u32 a;
    var u32 b;
    @constructor function create(u32 this.a; u32 this.b){}
}
```

is equivalent to

```{.styx .numberLines}
struct S
{
    var u32 a;
    var u32 b;
    @constructor function create(u32 a; u32 b)
    {
        this.a = a;
        this.b = b;
    }
}
```

It is an error to use this naming if the function

- is not an aggregate member
- is used to define a type
- has no body

### Parameter default value

If a parameter specifies an initializer [Expression] then it is used as argument
and only when a [CallExpression] does not contains enough arguments.

All parameters that are declared after an initialized parameter must also be initialized.

The expression must have valid semantics in the declaration scope.

If the declaration scope is not a function then the expression is not allowed to create hidden temporaries
that require management.

If the parameter is for a member function then the expression cannot use member variables of the same aggregate.

```{.styx .numberLines}
struct S
{
    var s32 member;
    function f(s32 a = member) { } // error, non static member `member` cannot be used as parameter default value
}
```

### Parameter type

[TypeAuto] is only allowed if a default argument is specified.

### Return type

#### Unspecified

If the return type is not specified then it is set to an internal `void`{.styx} type.

#### `var`{.styx}

If the return type is specified and prefixed with the `var`{.styx} storage class then
the return is a lvalue. For example struct members such as static arrays
can be returned by reference, which avoids the creation of temporaries.

#### `auto`{.styx}

If the return type is `auto`{.styx} then the actual return type is infered from the [ReturnStatement]s declared in the body.

[ReturnStatment] that depend on a condition that is statically verified to be false are not considered.

If the first `return`{.styx} does not have an expression or if it returns a void call
then the internal `void`{.styx} type if infered and the other [ReturnStatement]s must either return void calls or nothing.

```{.styx .numberLines}
function f(): auto
{
    if condition do
        return;     // internal void is inferred
    return 0;       // error as per implicit conversion rules
}
```

```{.styx .numberLines}
function v();
function f(): auto
{
    if condition do
        return v(); // internal void is inferred
    return;         // OK
}
```

Otherwise the type given by the expression is used to implictly convert the expression of the other `return`{.styx}s

`auto`{.styx} returns are never implictly `var auto`{.styx}.

```{.styx .numberLines}
function f(): auto
{
    if condition do
        return 0;   // u64
    return v();     // OK, rewritten "return v():u64;"
}
function v(): s32;
```

#### Missing and implicit returns

If the body of a function that returns void contains no final [ReturnStatement] then it is automatically inserted.

```{.styx .numberLines}
function f()
{
    if condition do
        return;
    // return; inserted implictly
}
```

If the body of a function that does not return void contains no final [ReturnStatement] then an error is issued.

```{.styx .numberLines}
function f(): s32
{
    if condition do
        return 0;
    // dont know what to return so...
    // error, missing return
}
```
A missing final return statement is allowed if the last statement is an [AssertStatement] with `0`{.styx} or `false`{.styx}
as condition.

```{.styx .numberLines}
function f(): s32
{
    assert(0); // no error
}
```

### Function body

#### Unspecifed body

If the function body consists of a semicolon then
the function must either exist in another unit or in a foreign object file.
If this is not the case then linking fails.

#### Regular body

A body contains statements and declarations.
Out of order declaration are not allowed in function bodies.

#### Assembler body

The assembler body allows to specify the body in pure assembler language.

The expression that gives the body must be statically simplifiable to a [StringExpression].

The `@foreign`{.styx} attribute is allowed. If used, the function will be exported
with its unqualifed name.

The syntax default to AT&T. Use the [AsmAttribute] to switch to the Intel syntax.
