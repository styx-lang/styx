set -e
../../../bin/matchex -fmain.sx -l8 -c6 > results.txt
echo 'success: will list occurences of `struct Byte`
main.sx:3:8
main.sx:8:5
main.sx:9:5
main.sx:39:32
main.sx:39:41
main.sx:41:9' > expected.txt
DIFF=$(diff "expected.txt" "results.txt")
if [ ! -z "$DIFF" ]; then
    echo $DIFF
    exit 1
fi
