---
header-includes:
title: Protection declaration
---

## Grammar

```{=html}
<code class=ebnf>
ProtectionDeclaration ::= "protection" "(" [Identifier] ")"
</code>
```

## Semantics

A ProtectionDeclaration restricts the visibility of [Declaration]s based on the lexical scope where declared.

ProtectionDeclarations have only an effect in the lexical scopes created by

- [ClassDeclaration]s
- [StructDeclaration]s
- [UnionDeclaration]s
- [UnitDeclaration]s

A ProtectionDeclaration is defined until overwritten by another ProtectionDeclaration.

The visitility of a particular [Declaration] can be temporarily changed using the `@public`{.styx}, `@private`{.styx}, `@protected`{.styx}, or `@strict`{.styx} [Attribute]s.

A ProtectionDeclaration does not introduce a scope.

```{.styx .numberLines}
unit u;

protection(public)

var s32 a; // public
var s32 b; // public

protection(private)

var s32 c; // private
var s32 d; // private
```

There are four levels of protection.

### public

`public` [Declaration]s are always visible.

This the default value.

```{.styx .numberLines}
unit u;
protection(private)

static var s32 a;

protection(public)

static var s32 a; // visible in u and to in all units that import u
```

### private

`private` [Declaration]s are only visible in the [UnitDeclaration] where they are declared.

```{.styx .numberLines}
unit u;

struct S
{
protection(private)
    static var s32 a;
}

struct T
{
    function testPrivate()
    {
        S.a = 8; // ok, same unit
    }
}
```

in another unit

```{.styx .numberLines}
unit other;

import u;

function testPrivate()
{
    S.a = 8; // error, not visible from here
}
```

### protected

In aggregates that support inheritence `protected` [Declaration]s are visible in all the derived aggregates.

Those derived aggregates can be located in [UnitDeclaration]s other than the base aggregate unit.

`protected` is based on the lexical scope, not the `this`{.styx} instance.

When used out of an aggregate, `protected` [Declaration]s are like `private` ones.

```{.styx .numberLines}
unit u;

class Base
{
protection(protected)

    function f() { }
}

function test()
{
    var Base* v = new Base;
    b.f();  // ok, same unit
}
```

and in another unit

```{.styx .numberLines}
unit other;

import u;

class Derived : Base
{
    static function test1()
    {
        var Base* b = new Base;
        b.f();    // ok, we are in `Derived`, which derives from `Base`;
        delete b;
    }

    function test2()
    {
        this.f(); // ok, `this` derives from `Base`;
    }
}

function test()
{
    var Base* b = new Base;
    b.f();  // error, not visible from here
}
```

### strict

`strict` [Declaration]s are only visible in the current aggregate.

At the unit scope, `strict` is equivalent to `private`.

```{.styx .numberLines}
unit u;

struct S
{
protection(strict)
    static var s32 a;
}

struct T
{
    function testStrict()
    {
        S.a = 8; // error, not visible from here
    }
}

function testStrict()
{
    S.a = 8;    // error, not visible from here
}
```

This is the strongest protection possible.
