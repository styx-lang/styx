# Styx

Styx programming language and its compiler.

Styx has rather a C syntax, an efficient PEG grammar, is statically typed and compiles to native code.

Styx is a hobby project that had initially no other goal than to experiment
self-hosting of a compiler, however since that milestone reached,
the project revealed itself very maintainable and able to evolve fast as for example,
and without mentioning the dozen of smaller improvments, first class tuples,
builtin coverage and even generics were added during the first six months that
followed the self-hosting.

Learn more in the [documentation](https://styx-lang.gitlab.io/styx/).

## Build

### Common requirements

Only available and only targets linux x86-64

### Compiler

requires

- being self-hosted, the content of the [latest](https://gitlab.com/styx-lang/styx/-/releases) styx release in `previous/`, that is automated in _get-previous.sh_.
- llvm 15 (shared library)
- the standard C library (must provide _crt1.o_ and related C runtime init/fina objects)

then

    $ make compiler-previous

then optionally, to bootstrap

    $ make compiler

### Documentation

requires

- a [D compiler](https://dlang.org/download.html) to generate the documentation generator
- [Pandoc](https://pandoc.org/) as a markdown to html and static syntax highlighter tool

then

    $ cd ./docsrc && bash generate.sh

### Test

requires

- a [D compiler](https://dlang.org/download.html) to build the test script.
- styx compiler
- valgrind

then

    $ make test

## Tooling

- builtin instrumentation for coverage by compiling with the CLI arg `--cov`, compatible with codecov (gcov textual format)
- builtin linter
- a standalone [C header translator](https://gitlab.com/styx-lang/xstep).
- DWARF informations (`-g`) allow
  - debuging with the GNU or the LLVM debugger
  - to find illegal heap accesses or to track leaks with valgrind
  - profiling with callgrind
  - fancy local coverage reports using for example [kcov](https://github.com/SimonKagstrom/kcov)
- syntax highlighting is supported by
  - [dexed](https://gitlab.com/basile.b/dexed)
  - Most of the editors based on the KDE framework (kwrite, kate, kdevelop, etc.) by installing [this xml](docsrc/styx_kde_syntax.xml)
