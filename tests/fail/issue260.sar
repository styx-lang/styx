§ast/visitor.sx
unit ast.visitor;

@private import ast.base;

class AstVisitor
{
    @virtual function visitNode(AstNode* node)                  { node.accept(this); }
    @virtual function visitDeclaration(DeclarationNode* node)   { node.accept(this); }
    @virtual function visitDeclarationOrStatement(DeclarationOrStatementNode* node) { node.accept(this); }
    @virtual function visitDeclarationsOrStatements(DeclarationsOrStatementsNode* node) { node.accept(this); }
    @virtual function visitStatement(StatementNode* node)       { node.accept(this); }

    overload visit
    {
        visitDeclarationOrStatement,
        visitDeclarationsOrStatements,

        visitDeclaration,
        visitStatement,
    }
}

§ast/base.sx
unit ast.base;

@private import ast.visitor;

class AstNode
{
    @virtual function accept(AstVisitor* visitor){}
}

class DeclarationNode : AstNode { }

@final class DeclarationOrStatementNode : AstNode
{
    var DeclarationNode* declaration;
    var StatementNode* statement;
    @override function accept(AstVisitor* visitor)
    {
        declaration ? visitor.visit(declaration) else visitor.visit(statement);
    }
}

@final class DeclarationsOrStatementsNode : AstNode
{
    var DeclarationOrStatementNode*[] items;
    @constructor function construct(){}
    @override function accept(AstVisitor* visitor)
    {
        foreach var DeclarationOrStatementNode dos in items do
           visitor.visit(dos);
    }
}

class StatementNode : AstNode { }


