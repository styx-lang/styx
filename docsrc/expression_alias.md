---
header-includes:
title: Expression alias
---

## Grammar

```{=html}
<code class=ebnf>
ExpressionAlias ::= [Attribute]* "const"? "alias" [Identifier] "=>" [Expression] ";"
</code>
```

## Semantics

Make an expression evaluable using an identifier.

The semantics of the original expression are never verified.

Instead, when the identifier is used, it is copied then contextualized in the current scope.

```{.styx .numberLines}
struct S
{
    struct T
    {
        var s32 m;
    }
    var T t;
    alias m => t.m;
}

function test()
{
    var S s;
    s.m = 42;
    assert(s.t.m == 42);
}
```

While the typical usage is to pull nested members there is no restricton on the expression kind

```{.styx .numberLines}
@foreign function puts(char* str): s32;

struct S
{
    struct T
    {
        function hello()
        {
            puts("hello");
        }
    }
    var T t;
    alias hello => t.hello();
}

function test()
{
    var S s;
    s.hello; // this is call
}
```

As the expression is contextualized at the use site, local identifiers can be unexpectedly captured

```{.styx .numberLines}
struct S
{
    var s32[2] array;
    var s32 index = 1;

    alias secondElement => array[index];
}

function test()
{
    var S s;
    var s32 index = 2;   // will be captured
    s.secondElement = 1; // bound violation
}
```

An expression alias can be defined locally, allowing an usage similar to
a nested function but without the penality caused by the indirect access
to escapes

```{.styx .numberLines}
function work(var s32);
function call();

function test(): s32
{
    var s32 result;

    // use a tuple to alias several expressions
    alias exitUsingAlias => (call(), result)[1];

    // semantically equivalent to exitUsingAlias
    function exitUsingNestedFunc(): auto
    {
        call();
        return result;
    }

    while someCondition do
    {
        work(result);
        if otherCondition do
            return exitUsingAlias;
        if otherCondition do
            break;
    }
    return exitUsingAlias;
}
```

Expression aliases can use other expression aliases

 ```{.styx .numberLines}
struct S
{
    struct T
    {
        struct U
        {
            var s32 m;
        }
        var U u;
        alias m => u.m;
    }
    var T t;
    alias m => t.m;
}

function test()
{
    var S s;
    s.m = 42;
    assert(s.t.u.m == 42);
}
```

If an expression alias is `const` then the expression must be statically evaluable.

`const` expression aliases are not contextualized at the use site.

`const` expression aliases are used to defined named constants that are not to be emitted in the target binary.
