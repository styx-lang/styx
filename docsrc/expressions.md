---
header-includes:
title: Expressions
---

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## Expression

### Grammar

```{=html}
<code class=ebnf>
Expression ::= [ConditionalExpression]
             | [BinaryAssignExpression]
             | [DotAssignExpression]
             | [OptAssignExpression]
             | [RangeExpression]
             | [AssignExpression]
             ;
</code>
```

### Semantics

In the language an expression is the generic term to describe binaries, unaries and primaries.
All expression are characterized by

1. a type

   Styx being statically typed, all expressions have a [Type].
   An initial type is determined by [PrimaryExpression]s.
   That type can evolve depending on the conversions and the effects that happen when used in the other expressions.

2. the Lvalue-ness

   All the expressions can yield a rvalue but only a few can be assigned to.
   In the compiler an assignable expression is said to be a Lvalue.
   In a program an assignable expression corresponds to something that has an address.

Other characteristics of expressions are

1. promotion

   Styx generally does not promote arithmetic expressions operands that can overflow.
   Numerical extension is however possible using the [CastExpression].

   Exceptions are

   - unsigned and signed comparison of numerical operands of same size
   - use of variable of [Type] `bool`{.styx} in arithmetic expressions

2. precedence

   The precedence, affecting expressions with an arity over one, is documented by the EBNF grammar blocks included
   with the documentation of each construct.

3. Order of evaluation

  To the exception of the [AssignExpression]s, expressions with an arity over one are evaluated from the left to the right.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## ConditionalExpression

### Grammar

```{=html}
<code class=ebnf>
ConditionalExpression ::= [OrOrExpression] "?" [Expression] [ConditionalElsePart]?
ConditionalElsePart ::= "else" [Expression]
</code>
```

### Semantics

#### General

The expression located before the question mark, the condition, must be or convertible to a [CmpExpression].
If the condition is verified then the expression gives, as a rvalue,
the `else`{.styx} LHS (the _thenExpression_) and otherwise its RHS (the _elseExpression_).

The type is determined as following:

- it is the type of the _elseExpression_ if the _thenExpression_ implictly converts to.
- it is the type of the _thenExpression_ if the _elseExpression_ implictly converts to.
- if the type of the _elseExpression_ and the type of the _thenExpression_ are both [TypePointer] to classes then it is the less derived base type they have in common.

The expression is a lvalue if the _elseExpression_ and the _thenExpression_ are both lvalues.

#### Void special case

If one of the expression has for type void then the conditional expression also
takes the void type and it does not yield any value.

```{.styx .numberLines}
function f(){}
var s32 a = 0;
a ? f() else a = 1;
```

Such conditional expressions can only be used as the expression of a [ExpressionStatement]
or as the expression of a [ReturnStatement] if located in a function without return.

#### Shorthand syntax

When the optional [ConditionalElsePart] is not specified then the compiler rewrites the expression.
The _thenExpression_ becomes the _elseExpression_ and the comparison LHS becomes the _thenExpression_.

```{.styx .numberLines}
const s32 a = 1 ? 2;
// is lowered to const s32 a = 1 != 0 ? 1 else 2;
assert(a == 1);
```
The _thenExpression_, contrary to what would happen with the standard syntax, is evaluated only once.

```{.styx .numberLines}
static function get(): s32
{
    static var s32 result;
    return result += 1;
}
// standard
var s32 a = get() ? get() else 3;
assert(a == 2);
// shorthand form
var s32 a = get() ? 3;
assert(a == 1);
```

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## AssignExpression

### Grammar

```{=html}
<code class=ebnf>
AssignExpression ::= [OrOrExpression] ("=" [AssignExpression])?
</code>
```

### Semantics

The LHS must be an assignable Lvalue.
The RHS must be implictly convertible to the LHS type.
The RHS is stored in the LHS.
The expression returns the LHS after the side effect and as a Lvalue.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## BinaryAssignExpression

### Grammar

```{=html}
<code class=ebnf>
BinaryAssignOperator ::= "/=" | "*=" | "%=" | "-=" | "+=" | "&=" | "^=" | "|=" | "<<=" | ">>=" | "~=";
BinaryAssignExpression ::= [OrOrExpression] [BinaryAssignOperator] [Expression]
</code>
```

### Semantics

The semantics is similar to `LHS = LHS op RHS`{.styx}, excepted that the LHS is only evaluated once.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## DotAssignExpression

### Grammar

```{=html}
<code class=ebnf>
DotAssignExpression ::= [OrOrExpression] ".=" [Expression]
</code>
```

### Semantics

The RHS must be either an [IdentExpression] or a [CallExpression].

If the RHS is an [IdentExpression] then the semantics are similar to `LHS = LHS.RHS`{.styx},
excepted that the LHS is only evaluated once.

Otherwise, if the RHS is a [CallExpression] then the semantics are similar to `LHS = (LHS.RHS)()`{.styx},
excepted that the LHS is only evaluated once.

Runtime checks that would normally be generated are also performed only once.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## OptAssignExpression

### Grammar

```{=html}
<code class=ebnf>
OptAssignExpression ::= [OrOrExpression] "?=" [Expression]
</code>
```

### Semantics

The LHS must be an assignable Lvalue and also evaluable as a condition, as described for each [Type].
The LHS remains untouched if the condition evaluates to `true`{.styx} and it is set to the RHS otherwise.
The LHS is only evaluated once.
The RHS must be implictly convertible to the LHS type.

```{.styx .numberLines}
// the following OptAssigns are semantically
// equivalent to `if a == 0 do a = 10;`

var s32 a = 8;
a ?= 10;
assert(a == 8);

a = 0;
a ?= 10;
assert(a == 10);
```

The expression returns the LHS after the optional side effect and as a Lvalue.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## RangeExpression

### Grammar

```{=html}
<code class=ebnf>
RangeExpression ::= [OrOrExpression]  ".." [Expression]
</code>
```

### Semantics

The expression takes a dedicated and unconvertible TypeRange type so that it can
only be used as [ForeachStatement] iterable and as expression in the [OnMatchStatement].

The left hand side must be implicitly convertible to the right hand side type,
or the opposite.

Nested ranges are always rejected.

Other semantics are detailed in the [ForeachStatement], the [SwitchStatement], and the [ArrayExpression] specifications.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## InExpression

### Grammar

```{=html}
<code class=ebnf>
InExpression ::= [OrExpression]  ("!"? "in" InExpression)?
</code>
```

### Semantics

The operator is uniquely available for operator overloading using `@operator(a in b)`{.styx}
and for the inclusion test of [TypeEnumSet].

If the RHS is a TypeStaticArray with a enum as element type then
the RHS is automatically converted to an equivalent set.

In the context of inclusion test it is illegal to use a [NotExpression] as RHS.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## OrOrExpression

### Grammar

```{=html}
<code class=ebnf>
OrOrExpression ::= [AndAndExpression] ("||" OrOrExpression)?
</code>
```

### Semantics

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## AndAndExpression

### Grammar

```{=html}
<code class=ebnf>
AndAndExpression ::= [InExpression] ("&&" AndAndExpression)?
</code>
```

### Semantics

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## OrExpression

### Grammar

```{=html}
<code class=ebnf>
OrExpression ::= [XorExpression] ("|" OrExpression)?
</code>
```

### Semantics

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## XorExpression

### Grammar

```{=html}
<code class=ebnf>
XorExpression ::= [AndExpression] ("^" XorExpression)?
</code>
```

### Semantics

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## AndExpression

### Grammar

```{=html}
<code class=ebnf>
AndExpression ::= [CmpExpression] ("&" AndExpression)?
</code>
```

### Semantics

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## CmpExpression

### Grammar

```{=html}
<code class=ebnf>
CmpOperator   ::= "==" | ">" | ">=" | "<" | "<=" ;
CmpExpression ::= [RShiftExpression] (CmpOperator [RShiftExpression])?
</code>
```

### Semantics

Compare the two operands and returns a `bool`{.styx} rvalue.
The operands must be both of a numeric type or both of a pointer type.

If two integer values of same size are compared, if their value is not statically known
and if only one is signed, then the signed value is promoted.

Comparisons of `union`{.styx} types are only supported using operator overloads.

Comparisons of `struct`{.styx}, `class`{.styx}, dynamic array and static array types are only
supported for `==`{.styx} and `!=`{.styx} on lvalues. The comparison consists of a simple
`memcmp`{.styx} so the operator overloads defined by the aggregate elements are ignored.

The other comparison operators are supported by `class`{.styx} and `struct`{.styx} when overloaded.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## RShiftExpression

### Grammar

```{=html}
<code class=ebnf>
RShiftExpression ::= [LShiftExpression] (">>" RShiftExpression)?
</code>
```

### Semantics

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## LShiftExpression

### Grammar

```{=html}
<code class=ebnf>
LShiftExpression ::= [ConcatExpression] ("<<" LShiftExpression)?
</code>
```

### Semantics

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## ConcatExpression

### Grammar

```{=html}
<code class=ebnf>
ConcatExpression ::= [SubExpression] ("~" ConcatExpression)?
</code>
```

### Semantics

The Concat concatenates the two sides in a new slice that's automatically managed.
The type of the slice is determined this way:

- if one of the side is an array then the element type is selected as a first reference.
- if one of the side is a value then its type is taken as a second reference.
- the two types must be equal.

this allows to concatenate two arrays

```{.styx .numberLines}
var u64[2] a = [1,2];
var u64[2] b = [3,4];
var u64[]  c   =  a ~ b;
```

but also to create arrays from values

```{.styx .numberLines}
var u64[] a  =  1 ~ 2 ~ 3 ~ 4;
```

and obviously the two ways can be mixed

```{.styx .numberLines}
var u64[] b = get();
var auto a  =  [1, 2] ~ 3 ~ 4 ~ get();
```

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## SubExpression

### Grammar

```{=html}
<code class=ebnf>
SubExpression ::= [AddExpression] ("-" SubExpression)?
</code>
```

### Semantics

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## AddExpression

### Grammar

```{=html}
<code class=ebnf>
AddExpression ::= [ModExpression] ("+" AddExpression)?
</code>
```

### Semantics

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## ModExpression

### Grammar

```{=html}
<code class=ebnf>
ModExpression ::= [DivExpression] ("%" ModExpression)?
</code>
```

### Semantics

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## DivExpression

### Grammar

```{=html}
<code class=ebnf>
DivExpression ::= [MulExpression] ("/" DivExpression)?
</code>
```

### Semantics

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## MulExpression

### Grammar

```{=html}
<code class=ebnf>
MulExpression ::= ([UnaryExpression] ("*" MulExpression)?)
                | ([UnaryExpression]"*")
                ;
</code>
```

### Semantics

The first form yields the product of the two sides.

If the RHS is not specified but `*` is and if the LHS gives a type then
the expression is lowered to a [TypeExpression] wrapping a [TypePointer].

Note that second form is parsed when a [PostExpression] is.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
