---
header-includes:
title: Statements
---

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## Grammar

```{=html}
<code class=ebnf>
Statement ::= [DeclarationStatement]
            | [AssertStatement]
            | [BlockStatement]
            | [BreakOnStatement]
            | [BreakStatement]
            | [ContinueOnStatement]
            | [ContinueStatement]
            | [DeferStatement]
            | [ExpressionStatement]
            | [ForeachStatement]
            | [GotoStatement]
            | [IfElseStatement]
            | [ReturnStatement]
            | [StaticAssertDeclaration]
            | [SwitchStatement]
            | [VersionBlockStatement]
            | [WhileStatement]
            ;
</code>
```
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## AssertStatement

### Grammar

```{=html}
<code class=ebnf>
StaticAssertDeclaration ::= "static" AssertStatement
AssertStatement ::= "assert" "(" [Expression] ( "," [Expression])? ")" ";"
</code>
```

### Semantics

The first expression must be evaluable as a condition.
The second expression, when specified, must give a null terminated UTF8 string.
When not specified it is automatically set to a [StringExpression] that
represents the source code of the first expression.

When the condition is not verified a message is displayed then either
the compilations stops if the assertion is statically evaluated and
otherwise the program crashes.

```{.styx .numberLines}
assert(echo(is, s8, s32)); // error: assert failure ...
```

Assertions are enabled with the compile switch `--check=asserts`

#### `static`{.styx} limitation

static evaluation of `assert`{.styx} is limited to

- equality of string literals
- arithmetic, bitwise, relational expressions for all integer and floating point type
- logical expressions for `bool`{.styx}
- the second expression must be statically evaluable to a [StringExpression]

#### Unreachable

`assert(0)`{.styx} and `assert(false)`{.styx} are used to mark the current block as unreachable.

Unless specified, assertions that denote unreachability dont have for effect to output a message.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## AtLabel

### Grammar

```{=html}
<code class=ebnf>
AtLabel ::= "@" Identifier
</code>
```

### Semantics

Specify the [GotoStatement] target or the optional [BreakStatement] and [ContinueStatement] target scope.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## BlockStatement

### Grammar

```{=html}
<code class=ebnf>
BlockStatement ::= [CurlyBlockStatement]
                 | [SingleStatement]
                 ;
CurlyBlockStatement ::= "{" [Statement]* "}"
SingleStatement     ::= [Statement]
</code>
```

### Semantics

When not used for a VersionBlockStatement, a block introduces a scope.

```{.styx .numberLines}
var u64 a;
{
    var u32 a;
    a = 42;     // affects the u32 one
}
assert(a == 0); // tests the u64 one
```

Forward uses of declarations is not supported.

On exit the managed locals declared in the scope are freed.

Passed the scope it is UB to use a reference.

```{.styx .numberLines}
var char* text;
{
    var char[+] temp = get();
    text = temp.ptr;
}
// it is UB to use `text` because `temp` is out of scope.
```

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## BreakOnStatement

### Grammar

```{=html}
<code class=ebnf>
BreakOnStatement ::= "break" "on" ";"
</code>
```

### Semantics

Explicitly exit the current [SwitchStatement].

Managed locals declared from the parent [SwitchStatement] to the `break on`{.styx} are freed.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## BreakStatement

### Grammar

```{=html}
<code class=ebnf>
BreakStatement ::= "break" (([AtLabel] ("," [Expression])?) | [Expression])? ";"
</code>
```

### Semantics

If specified, evaluate the expression.

If the label is not specified then break the current loop.

Otherwise break the loop in which the label is declared.

```{.styx .numberLines}
struct F
{
    var s32 m;
}

var F*[][] items;

foreach var F[] row in items do
{
    label A;
    foreach var F f in row do
    {
        if f.m == 8 do
            break;      // continue to next row
        if f.m == 12 do
            break @A;   // break the foreach that introduces row
    }
}
```

Managed locals declared from the parent [ForeachStatement] or [WhileStatement] to the semicolon following the `break`{.styx} are freed.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## ContinueOnStatement

### Grammar

```{=html}
<code class=ebnf>
ContinueOnStatement ::= "continue" "on" ([Expression] | "else)"? ";"
</code>
```

### Semantics

The ContinueOnStatement must be located in the body of a [OnMatchStatement].

When neither the optional expression nor `else`{.styx} is specified then the statement
has for effect to jump in the body of the next [OnMatchStatement].

```{.styx .numberLines}
var s32 a;
switch a do
{
    on 0 do { a+=32; continue on;   } // set a and go to next match
    on 1 do {/*a is either 1 or 32*/}
    else do {                       }
}
```

If the expression is not specified then it is an error to use the statement in the last match body.

```{.styx .numberLines}
var s32 a;
switch a do
{
    on 0 do { a+=32; continue on;   } // set a and go to next match
    on 1 do { continue on;          } // error, cannot find target...
    else do {                       }
}
```

It is an error not to use the statement at the end of the body

```{.styx .numberLines}
var s32 a;
switch a do
{
    on 0 do { continue on; a+=32;   } // error, stmt not reachable...
    on 1 do {                       }
    else do {                       }
}
```

If the expression is specified then it must be equal to an existing match
and the statement has for effect to jump in the body of this match.

```{.styx .numberLines}
var s32 a;
switch a do
{
    on 0 do { a+=32; continue on 1;    } // set a and go to match for 1
    on 1 do { /*a is either 1 or 32 */ }
    else do {                          }
}
```

If the expression is not specified but `else`{.styx} is present then the switch must
contain the explicit `else`{.styx} block which then becomes the `continue on`{.styx} target.

```{.styx .numberLines}
var s32 a;
var s32 b;
switch a do
{
    on 0 do { b=1; continue on else; }
    on 1 do { b=2; continue on else; }
    else do {
                // else reached from case 0 and 1
                if b == 1 ||b == 2 do assert(a == 0 || a == 1);
                // else block reached because no match
                else do assert(a != 0 && a != 1);
            }
}
```

Managed locals declared from the parent [SwitchStatement] to the `continue on`{.styx} are freed.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## ContinueStatement

### Grammar

```{=html}
<code class=ebnf>
ContinueStatement ::= "continue" (([AtLabel] ("," [Expression])?) | [Expression])? ";"
</code>
```

### Semantics

If specified, evaluate the expression.

If no label is specified then continue the current loop.

If the label is specified then continue the loop in which the label is declared.

```{.styx .numberLines}
struct F
{
    var s32 m;
}

function log(char* text);

var F*[][] items;

foreach (var F[] row) in items do
{
    label A;
    foreach (var F* f) in row do
    {
        if f.m == 12 do
            continue @A, log("encountered 12..."); // continue to next row
    }
}
```

Managed locals declared from the parent [ForeachStatement] or [WhileStatement] to the semicolon following the `continue`{.styx} are freed.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## DeclarationStatement

### Grammar

```{=html}
<code class=ebnf>
DeclarationStatement ::= [Declaration]
</code>
```

### Semantics

A simple shell allowing to declare in function bodies.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## DeferStatement

### Grammar

```{=html}
<code class=ebnf>
DeferStatement ::= "defer" [BlockStatement]
</code>
```

### Semantics

Defers the execution of a [BlockStatement] to the end of the current scope.

The statement cannot be nested.

The statement is not allowed in the main body of a function attributed `@noreturn`.

[GotoStatement] are not allowed if `defer`{.styx}s exist in the scope.

[ReturnStatement] are not allowed inside the statement.

If the scope ends with a non-void [ReturnStatement] then the `defer`{.styx}s are evaluated after the returned expression.

```{.styx .numberLines}
function main(): s32
{
    var s32 a = 0;
    defer
    {
        a = 0;
    }
    a = 1;
    return a;  // returns 1
}
```

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## ExpressionStatement

### Grammar

```{=html}
<code class=ebnf>
ExpressionStatement ::= [Expression] ";"
</code>
```

### Semantics

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## ForeachStatement

### Grammar

```{=html}
<code class=ebnf>
ForeachStatement ::= "foreach" [ForEachVariables] "in" [Expression] "do" [BlockStatement]

ForEachVariables ::= [ForeachVariableList]
                   | "(" [ForeachVariableList] ")"
                   ;

ForeachVariableList ::= [Attribute]* [VariableDeclaration]  ( ";" [Attribute]* [VariableDeclaration] )*
</code>
```

### Semantics

The statement does not support modification of aggregate being iterated.

#### `foreach`{.styx} over numeric ranges

If the expression is a [RangeExpression] then the two expressions that give the bounds must have an integer [Type].
Their value gives the count of iteration, starting from the left (inclusive) to the right (exclusive).

A single variable is allowed and it must be implictly convertible to the type of the integer range.

```{.styx .numberLines}
foreach const s8 i in 0 .. 8 do
    printf("%d ", i);               // 0 1 2 3 4 5 6 7
```

#### `foreach`{.styx} over enums and enumsets

In both case a single variable is allowed.

If the expression gives a [TypeEnum] then the variable consecutively represents each member value.

```{.styx .numberLines}
enum E {e0, e1, e2}
foreach const E e in E do
    printf("%d ", i);   // 0 1 2
```

If the expression gives a [TypeEnumSet] value then the variable consecutively represents each member being included in the set.

```{.styx .numberLines}
enum E {e0, e1, e2, e3}
var bool[E] es = [e1,e3];
foreach const E e in es do
    printf("%d ", i);   // 1 3
```

#### `foreach`{.styx} over arrays

If the expression gives a [TypeStaticArray], [TypeSlice], or a [TypeRcArray] then two variables are allowed.

The first optional variable, the "counter", represents the current iteration number.

The counter type must be implicitly convertible to the type of the array length.

For [TypeSlice] and [TypeRcArray] this is always `usize`{.styx} or `ssize`{.styx}.

For [TypeStaticArray]s this depends on its `.length` property.

The last variable represents an array element.

The variable type must exactly match to the array element type.

```{.styx .numberLines}
// over an array literal
foreach const char* s in ["0", "1", "2" ] do
    printf("%s ", s);    // 0 1 2

// also count
foreach (const s8 counter; const char* s) in ["0", "1", "2" ] do
    printf("%d-%s ", counter + 10, s);  // 10-0 11-1 12-2
```

#### `foreach`{.styx} over aggregates (random access style)

A custom type can be directly used in `foreach`{.styx} if it implements

- a `length()`{.styx} function that returns an integer value.
- an `@operator(a[b])`{.styx} function that takes an integer index and returns a value with a
  type that is compatible with the last `foreach`{.styx} variable.

```{.styx .numberLines}
struct Array
{
    var s32[+] array;
    function length(): auto
    {
        return array.length;
    }
    @operator(a[b]) function getElement(usize index): auto
    {
        return array[index];
    }
}
```

If the `foreach`{.styx} expression gives an aggregate or an aggregate pointer then

- the expression side-effect is extracted
- the `.length`{.styx} [DotExpression] property is added to the new expression
- the `foreach`{.styx} body first statement becomes a [VariableDeclaration] with for initializer
  an [IndexExpression] on the new expression

so that

```{.styx .numberLines}
var Array array;
var u64 sum;
foreach var e in array do
{
    sum += e;
}
```

becomes

```{.styx .numberLines}
var Array array;
foreach const __counter in 0 .. array.length() do
{
    var e = array.getElement(__counter);
    sum  += e;
}
```

Two variables are supported. If the `foreach`{.styx} counter is explicitly declared then
it is reused in the [VariableDeclaration] inserted in the loop body, so that

```{.styx .numberLines}
var Array array;
var u64 sum;
foreach (const usize c; var e) in array do
{
    sum += e;
}
```

becomes

```{.styx .numberLines}
var Array array;
foreach const usize c in 0 .. array.length() do
{
    var e = array.getElement(c);
    sum  += e;
}
```

#### `foreach`{.styx} over tuples

If the expression gives a tuple then the loop body is considered as a generic.

The body is applied for each element.

This is called tuple unrolling.

If two variables are specified then the first variable is transformed in an [ExpressionAlias]
that gives the element index as an [IntegerExpression].

That variable has to be `const` and can be either of type [TypeAuto] or of an integral type with
a bitwidth equal or higher than 32.

The second or the single variable is transformed as an alias that reads
the matching element.

That variable has to be `const` and must be of type [TypeAuto].

The generated alias can be either

- an [ExpressionAlias], if the element is an expression.
- an [AliasDeclaration], if the element is a [Type] or a symbol.

```{.styx .numberLines}
var t = (0,0.1);
foreach (const i; const v) in t do
{
}
```

becomes

```{.styx .numberLines}
{
    {
        alias i => 0;
        alias v => t[0];
    }
    {
        alias i => 1;
        alias v => t[1];
    }
}
```

As the blocks of an [IfElseStatement] that are statically determined not to be reachable are allowed to contain
semantically incorrect code, it is possible to process each element individually.

Such code, which at first glance looks semantically incorrect, works because

```{.styx .numberLines}
var (u32, f64) t = (0,0.1);
foreach (const i; const v) in t do
{
    if i == 0 do v = 1;
    if i == 1 do v = 0.5; // what when v is u32 ?
}
```

becomes in a first time

```{.styx .numberLines}
{
    {
        alias i => 0;
        alias v => t[0];
        if i == 0 do v = 1;
        if i == 1 do v = 0.5;
    }
    {
        alias i => 1;
        alias v => t[1];
        if i == 0 do v = 1;
        if i == 1 do v = 0.5;
    }
}
```

then finally

```{.styx .numberLines}
{
    {
        alias i => 0;
        alias v => t[0];
        v = 1;
    }
    {
        alias i => 1;
        alias v => t[1];
        v = 0.5;
    }
}
```

If unrolling does not occur inside a regular loop then [BreakStatement]s and the [ContinueStatement]s are transformed
in [GotoStatement].

A [BreakStatement] has for effect to jump passed the unrolling.

A [ContinueStatement] has for effect to jump before the next iteration.

#### Reversed `foreach`{.styx}

The [DotExpression] property `.reverse` has for effect to iterate from the last
element to the first.

```{.styx .numberLines}
@foreign function printf(char* specifier;... ): s32;

foreach const s8* s in (0 .. 3).reverse do
    printf("%d", s);                            // 2 1 0

foreach (const s8 counter; const char* s) in ["00", "11", "22" ].reverse do
    printf("%d-%s ", counter, s);               // 2-22 1-11 0-00
```

Tuple unrolling does not support this property.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## GotoStatement

### Grammar

```{=html}
<code class=ebnf>
GotoStatement ::= "goto" [AtLabel] ( "," [Expression])? ";"
</code>
```

### Semantics

If specified, evaluate the expression then jump to the position following the
[LabelDeclaration] given by the [AtLabel] identifier.

```{.styx .numberLines}
function func(s32 max)
{
    var s32 i;
    label nxt;
    i += 1;
    if i < max do
        goto @nxt;
}
```

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## IfElseStatement

### Grammar

```{=html}
<code class=ebnf>
IfElseStatement ::= "if" [IfCondition] "do" [BlockStatement] ("else" "do"? [BlockStatement])?

IfCondition     ::= [Expression]
</code>
```

### Semantics

The condition type must be convertible to [TypeBool].

The first block, the "thenBlock" is executed if the condition is verified,
otherwise, if the second block, the "elseBlock", is specified then it is executed.

The statement introduces a scope that is only valid in the "thenBlock".

The condition is tried at compile time. If the evaluation is possible and if it evaluates to false
then the "thenBlock" semantics are not checked. If it evaluates to true then the
"elseBlock" semantics are not checked.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## ReturnStatement

### Grammar

```{=html}
<code class=ebnf>
ReturnStatement ::= "return" [Expression]? ";"
</code>
```

### Semantics

Leave the current function.

The optional expression gives the return value.

It must be implictly convertible to the function return type.

If the current function has no return type then `return`{.styx} can be omitted, in which
case it is implicitly added as last statement of the body.

Managed locals declared in the BlockStatement that constitutes the function body are freed.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## SwitchStatement

### Grammar

```{=html}
<code class=ebnf>
SwitchStatement ::= "switch" [Expression] "do" "{" [OnMatchStatement]+ ("else" "do" [BlockStatement])? "}"

OnMatchStatement ::= "on" [OnMatchExpressions] "do" [BlockStatement]

OnMatchExpressions ::= [Expression] ("," [Expression])*
</code>
```

### Semantics

The switch statement is comparable to a suite of tests on a constant condition but with
restrictions on what can be tested so that the compiler can always generate fast jump tables.

The restriction are the following:

- the condition must test an expression that gives an integer, a bool, an enum set, or a string that has either the `char[]`{.styx} or the `char[+]` [Type]
- the matches must be expressions that are simplifiable to literals and that are convertible to the condition type
- the matches must be unique
- the match for a switch over a string cannot be empty
- the maximum number of cases can be limited and that limit is implemntation defined

There are two ways to express a match to the condition.
They can be mixed but duplicate cases are not allowed.

#### Individual matches

When a list of individual values is specified, the matching block is executed if the
condition matches exactly to one value of the list.

```{.styx .numberLines}
var s32 a;
switch a do
{
    on 0,1,2 do {}
    on 3,4,5 do {}
}
```

#### Matches over ranges

When a list of range is specified by a [RangeExpression], the matching block is executed if the
condition value is within (right value is inclusive) one of the range of the list.

```{.styx .numberLines}
var s32 a;
switch a do
{
    on 0 .. 2 do {}
    on 3 .. 5 do {}
}
```

Switch over strings cannot match ranges:

```{.styx .numberLines}
var char[] text;
switch a do
{
    on "aaa" .. "zzz" do {} // error
}
```

#### Switch over enum sets

Individual matches or values covered by a range dont represent the elements of a set.

Set literals are represented using [ArrayExpression]s

```{.styx .numberLines}
enum E {e0, e1, e2, e3}
var bool[E] es;

switch es do
{
    on e0, e1       do {} // same as `on [e0], [e1]`
    on [e0, e1]     do {} // to match a set made of both
    on e0 .. e2     do {} // same as `on [e0], [e1], [e2]`, and error, duplicated matches
    on [e0 .. e2]   do {} // to match a set made of the three members
}
```

#### Implicit break

After a match, and if the block does not close with an explcit terminator then
the program execution continues passed the switch.

```{.styx .numberLines}
var s32 a = 0;
var s32 b;
switch a do
{
    on 0 .. 2 do b = 0; // implicitly go to L0
    on 3 .. 5 do b = 3; // implicitly go to L0
    else      do b = 6; // implicitly go to L0
}
label L0;
assert(b == 0);
```

#### Explicit break

Although breaks are usually implicit, early breaks can be made using the [BreakOnStatement].

#### Fallthrough

Fallthroughs are explicitly performed using [ContinueOnStatement]s.

#### Else block

If specified, the `else`{.styx} block is exectued when none of the [OnMatchStatement]s matches.

#### Missing and checked else block

When the `else`{.styx} block is not specified and if an uncovered match is reached
and if the compiler was passed the `--check=switches` argument then the program
displays a diagnostic before crashing.

For example

```{.styx .numberLines}
var s32 a;
switch a do
{
    on 0 .. 2 do {}
    on 3 .. 5 do {}
}
```

Is turned, under X86, into

```{.styx .numberLines}
var s32 a;
switch a do
{
    on 0 .. 2 do {}
    on 3 .. 5 do {}
    else      do
    {
        printf("<source loc info>: error, switch match `%llu` is not covered", a);
        fflush(0);
        asm("ud2");
    }
}
```

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## VersionBlockStatement

### Grammar

```{=html}
<code class=ebnf>
VersionBlockStatement ::= [VersionExpression] [BlockStatement] ("else" "do"? [BlockStatement])?
</code>
```

### Semantics

The statement does not introduce scopes.

Same as [VersionBlockDeclaration] excepted that statements are allowed.

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## WhileStatement

### Grammar

```{=html}
<code class=ebnf>
WhileStatement ::= "while" [Expression] "do" [BlockStatement]
</code>
```

### Semantics

The statement introduces a scope.

Evaluate the declarations and statements while the condition given by the
expression is true.
