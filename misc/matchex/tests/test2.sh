set -e
../../../bin/matchex -fmain.sx -l28 -c10 > results.txt
echo 'success: will list occurences of `struct Aa`
main.sx:21:8
main.sx:28:9' > expected.txt
DIFF=$(diff "expected.txt" "results.txt")
if [ ! -z "$DIFF" ]; then
    echo $DIFF
    exit 1
fi
