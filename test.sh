set -e

if [ -z ${DC} ]; then
    DC=dmd
fi
# D dependencies of the tester are prefetched if running in a CI
DUB_REGISTRY_ARG=
if [ -n ${CI} ]; then
  DUB_REGISTRY_ARG=--skip-registry=all
fi

find . -name *.gcov -delete

# run @unittest functions and test mem managment
make compiler-front extra="--unittest --cov --of=bin/styx-front-unittest --vid=LEAK_CHECK"
valgrind --leak-check=full --show-leak-kinds=all -s --error-exitcode=1 bin/styx-front-unittest

if [ -d temp ]; then
    rm -rf temp
fi

# front-end tests
make compiler-front extra="--of=bin/styx-front --cov"
cd tests
dub build "test.d" --single $DUB_REGISTRY_ARG
./test "frontend.yml" -c../bin/styx-front
cd ..

make compiler-cov

# tools
cd misc/matchex
make
cd tests
sh test1.sh
sh test2.sh
sh test3.sh
cd ../../..

## build MUSL
#cd misc/musl
#sh ./configure ARCH=X86_64
#make --silent -j5
#cd ../..

# back-end tests
cd tests
./test "backend.yml" -c../bin/styx
cd ..
