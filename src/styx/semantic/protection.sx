unit styx.semantic.$protection;

@private import
    styx.ast.base,
    styx.ast.declarations,
    styx.token,
    styx.position,
    styx.scope,
    ;

/// Apply the `ProtectionDeclaration` to each `Declaration` of the provided unit
function setProtections(UnitDeclaration* node)
{
    static var NodeProtectionVisitor npv;
    npv.scope = node.scope;
    npv.visitUnitDeclaration(node);
}

protection(private)

class NodeProtectionVisitor : from(styx.ast.visitor).AstVisitor
{
    var bool inDerivable;
    var Protection currProt;
    var Scope* scope;
    var AstNode* illegalScope;

    function identifierToProt(Token* tok; var Position p)
    {
        if illegalScope do
        {
            var t = tok.text();
            error(scope.filename, p, "`protection(%s)` or `@%s` is only allowed in `class`, `struct`, `union`, and `unit` scopes", t.ptr, t.ptr);
        }
        const i = tok.iptr;
        if i == publicToken().iptr do
        {
            overwriteProtection(Protection.public);
        }
        else if i == protectedToken().iptr do
        {
            overwriteProtection(Protection.protected);
            if !inDerivable do
                warn(scope.filename, p, "in non-derivable aggregates `protected` is equivalent to `private`");
        }
        else if i == privateToken().iptr do
        {
            overwriteProtection(Protection.private);
        }
        else if i == strictToken().iptr do
        {
            overwriteProtection(Protection.strict);
        }
        else error(scope.filename, p,
            "`%s` is not a valid protection, expected `public`, `protected`, `private` or `strict`",
            tok.text().ptr);
    }

    function pushProtection(const Protection value): Protection
    {
        var result = currProt;
        currProt = value;
        return result;
    }

    function overwriteProtection(const Protection value)
    {
        currProt = value;
    }

    function popProtection(const Protection previous)
    {
        currProt = previous;
    }

    function setField(Declaration* node)
    {
        node.$protection = currProt;
    }

    @override function visitAliasDeclaration(AliasDeclaration* node)
    {
        setField(node);
    }

    @override function visitClassDeclaration(ClassDeclaration* node)
    {
        const prevInDerivable = inDerivable;
        const prevIllegalScope = illegalScope;
        illegalScope = null;
        inDerivable = true;
        setField(node);
        const p = pushProtection(Protection.public);
        node.accept(this);
        popProtection(p);
        inDerivable = prevInDerivable;
        illegalScope = prevIllegalScope;
    }

    @override function visitDeclaration(Declaration* node)
    {
        const old = currProt;
        var p = node.getAtProtection();
        if p do identifierToProt(p.identifier, p.startPos);
        super(node);
        if p do currProt = old;
    }

    @override function visitEnumDeclaration(EnumDeclaration* node)
    {
        setField(node);
    }

    @override function visitExpression(Expression* node)
    {
    }

    @override function visitFunctionDeclaration(FunctionDeclaration* node)
    {
        setField(node);
        const p = pushProtection(Protection.public);
        if node.body do
            visitConcreteStatement(node.body);
        popProtection(p);
    }

    @override function visitImportDeclaration(ImportDeclaration* node)
    {
        setField(node);
        node.accept(this);
    }

    @override function visitOverloadDeclaration(OverloadDeclaration* node)
    {
        setField(node);
    }

    @override function visitProtectionDeclaration(ProtectionDeclaration* node)
    {
        identifierToProt(node.$protection, node.startPos);
    }

    @override function visitStatement(Statement* node)
    {
        const prevIllegalScope = illegalScope;
        illegalScope = node;
        if node.kind == leftCurly || node.kind == eopDecl do
            visitConcreteStatement(node);
        illegalScope = prevIllegalScope;
    }

    @override function visitStructDeclaration(StructDeclaration* node)
    {
        const prevInDerivable = inDerivable;
        const prevIllegalScope = illegalScope;
        illegalScope = null;
        inDerivable = false;
        setField(node);
        const p = pushProtection(Protection.public);
        node.accept(this);
        popProtection(p);
        inDerivable = prevInDerivable;
        illegalScope = prevIllegalScope;
    }

    @override function visitType(Type* node)
    {
        node.accept(this);
    }

    @override function visitUnionDeclaration(UnionDeclaration* node)
    {
        const prevInDerivable = inDerivable;
        const prevIllegalScope = illegalScope;
        illegalScope = null;
        inDerivable = false;
        setField(node);
        const p = pushProtection(Protection.public);
        node.accept(this);
        popProtection(p);
        inDerivable = prevInDerivable;
        illegalScope = prevIllegalScope;
    }

    @override function visitUnitDeclaration(UnitDeclaration* node)
    {
        currProt = public;
        node.accept(this);
    }

    @override function visitVariableDeclaration(VariableDeclaration* node)
    {
        setField(node);
    }
}
