set -e
STYX="../../../bin/styx"
${STYX} imported.sx -v --be="obj"
${STYX} imported.sx -v --be="ar" --of="imported.a"
${STYX} app.sx .styx/imported.o -i"../link_obj_import"
./a.out
${STYX} app.sx .styx/imported.o -i"imported.sx"
./a.out
${STYX} app.sx imported.a -i"../link_obj_import"
./a.out
