set -e
../../../bin/matchex -fmain.sx -l35 -c24 > results.txt
echo 'success: will list occurences of `b1 = 1`
main.sx:32:12
main.sx:35:23' > expected.txt
DIFF=$(diff "expected.txt" "results.txt")
if [ ! -z "$DIFF" ]; then
    echo $DIFF
    exit 1
fi
