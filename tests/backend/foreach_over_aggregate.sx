unit foreach_over_aggregate;

@foreign function printf(s8* spec; ...): s32;

struct S
{
    var s32 a = 50;
    function length(): u64
    {
        return 10;
    }
    @operator(a[b]) function getElement(const usize i): u64
    {
        a+=1;
        return a;
    }
}

struct M
{
    var s32 a = 50;
    var s32 b = 70;
    function length(): u64
    {
        return 10;
    }
    @operator(a[b]) function getElement(const usize i): auto
    {
        return (a++,b++);
    }
}

function main(): s32
{
    // using ptr to value type + .reverse
    var S* s1 = new S;
    foreach (const usize counter; var auto v) in s1.reverse do
    {
        assert(v == 50 + 10 - counter);
    }
    delete s1;

    // using value type
    var S s2;
    foreach (const usize counter; var u64 v) in s2 do
    {
        assert(v == 50 + counter + 1);
    }

    // using the implicitly created counter var
    var S s3;
    var s32 is3;
    foreach (var u64 v) in s3 do
    {
        assert(v == 50 + ++is3);
    }

    // counter, infer more complex type
    var M m1;
    foreach (const usize counter; var auto v) in m1 do
    {
        assert(v[0] == 50 + counter);
        assert(v[1] == 70 + counter);
    }

    // extract side-effect
    var s32 sideFxCount;
    var M* m2;
    function get(): auto
    {
        sideFxCount++;
        return m2 = new M;
    }
    foreach (const usize counter; var auto v) in get() do
    {
        assert(v[0] == 50 + counter);
        assert(v[1] == 70 + counter);
    }
    delete m2;
    assert(sideFxCount == 1);

    return 0;
}
