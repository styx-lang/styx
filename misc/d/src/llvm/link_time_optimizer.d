//===-- llvm/LinkTimeOptimizer.h - LTO Public C Interface -------*- C++ -*-===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// This header provides a C API to use the LLVM link time optimization
// library. This is intended to be used by linkers which are C-only in
// their implementation for performing LTO.
//
//===----------------------------------------------------------------------===//

module llvm.link_time_optimizer;

import llvm.types;

extern (C):

/**
 * @defgroup LLVMCLinkTimeOptimizer Link Time Optimization
 * @ingroup LLVMC
 *
 * @{
 */

/// This provides a dummy type for pointers to the LTO object.
alias llvm_lto_t = void*;

/// This provides a C-visible enumerator to manage status codes.
/// This should map exactly onto the C++ enumerator LTOStatus.
enum llvm_lto_status
{
    LLVM_LTO_UNKNOWN = 0,
    LLVM_LTO_OPT_SUCCESS = 1,
    LLVM_LTO_READ_SUCCESS = 2,
    LLVM_LTO_READ_FAILURE = 3,
    LLVM_LTO_WRITE_FAILURE = 4,
    LLVM_LTO_NO_TARGET = 5,
    LLVM_LTO_NO_WORK = 6,
    LLVM_LTO_MODULE_MERGE_FAILURE = 7,
    LLVM_LTO_ASM_FAILURE = 8,

    //  Added C-specific error codes
    LLVM_LTO_NULL_OBJECT = 9
}

alias llvm_lto_status_t = llvm_lto_status;

/// This provides C interface to initialize link time optimizer. This allows
/// linker to use dlopen() interface to dynamically load LinkTimeOptimizer.
/// extern "C" helps, because dlopen() interface uses name to find the symbol.
llvm_lto_t llvm_create_optimizer ();
void llvm_destroy_optimizer (llvm_lto_t lto);

llvm_lto_status_t llvm_read_object_file (
    llvm_lto_t lto,
    const(char)* input_filename);
llvm_lto_status_t llvm_optimize_modules (
    llvm_lto_t lto,
    const(char)* output_filename);

/**
 * @}
 */

