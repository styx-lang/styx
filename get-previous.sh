rm -rf previous
mkdir previous
URL="https://gitlab.com/styx-lang/styx/-/releases/v0.24.1/downloads/binaries/styx-0.24.1-0-linux-x86_64.tar"
curl -J -L -f -ostyx.tar $URL

cd previous
tar -xf ../styx.tar
cd ..
