---
header-includes:
title: Class declaration
---

## Grammar

```{=html}
<code class=ebnf>
ClassDeclaration ::= [Attribute]* [StorageClass]* "class" [Identifier] [GenericParameters]? [BaseType]? [ClassBody]

BaseType ::= ":" [Type]

ClassBody ::= ";"
            | "{" [Declaration]* "}"
            ;
</code>
```

## Semantics

Classes are the constructs that allow to program using the Object Oriented paradigm.

### Attributes

See [Attribute] for a list of the attributes allowed on class declarations.

### BaseType

Classes can only inherits of a single [Type] that must represent a [ClassDeclaration].

### ABI

The layout of a class consists of

- a pointer to the instance virtual table
- the inherited fields
- the instance fields

The type of a virtual table is a pointer to a static array of pointer.

The first pointer is a pointer to the super virtual table (or null).
It allows to perform dynamic casts, i.e from less specialized to more specialized
but also to call the previous implementation of a virtual method,
as required by the [SuperExpression].

The following pointers are the addresses of the most overridden
virtual functions known by the instance.

### Allocation

Because they have a virtual table, pointer to classes may not be manually allocated.
Instead the [NewExpression] must be used with a custom allocator, which
also permit to copy the virtual table.

```{.styx .numberLines}
class C
{
    @constructor function create() {}
    @destructor function destroy() {}
}

function test()
{
    var C* c_on_heap = (new C).create(); // `new C` propers init the vtable
    var C c_on_stack1 = C.create();      // vtable init correctly because of the constructor called as value constructor
    var C c_on_stack2;                   // vtable init correctly as per deafault init rules

    // at end of scope
    // - c_on_stack1.destroy() called automatically
    // - c_on_stack2.destroy() called automatically
    // - c_on_heap continues to live
}
```
