/*===- llvm-c/ExternC.h - Wrapper for 'extern "C"' ----------------*- C -*-===*\
|*                                                                            *|
|* Part of the LLVM Project, under the Apache License v2.0 with LLVM          *|
|* Exceptions.                                                                *|
|* See https://llvm.org/LICENSE.txt for license information.                  *|
|* SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception                    *|
|*                                                                            *|
|*===----------------------------------------------------------------------===*|
|*                                                                            *|
|* This file defines an 'extern "C"' wrapper                                  *|
|*                                                                            *|
\*===----------------------------------------------------------------------===*/

module llvm.extern_c;

import llvm.types;

extern (C):

//enum LLVM_C_STRICT_PROTOTYPES_END = _Pragma("clang diagnostic pop");

//enum LLVM_C_EXTERN_C_BEGIN = LLVM_C_STRICT_PROTOTYPES_BEGIN;
//enum LLVM_C_EXTERN_C_END = LLVM_C_STRICT_PROTOTYPES_END;

