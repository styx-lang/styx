set -e
../../../bin/matchex -fmain.sx -l47 -c17 > results.txt
echo 'success: will list occurences of `class NoDupInClassMemberFunc`
main.sx:47:9
main.sx:58:9
main.sx:51:7
' > expected.txt
DIFF=$(diff "expected.txt" "results.txt")
if [ ! -z "$DIFF" ]; then
    echo $DIFF
    exit 1
fi
