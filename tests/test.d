/+ dub.sdl:
   name "test"
   dependency "dyaml" version="~>0.8.3"
+/
module test;

import
    std.algorithm,
    std.array,
    std.getopt,
    std.stdio,
    std.string,
    std.path,
    std.process;

import
    dyaml;

static immutable helpText =
`STYX test runner
================

Syntax:
   test TestDescriptor Options?

TestDescriptor:
    a YAML file that describes the tests to run

Options:
    -a<arg>     : add a common styx argument, e.g -a"--be=ir"
    -t<test>    : add a specific test to run. e.g -t"backend/hello_world.sx"
`;

struct options
{
__gshared:

    string[] commonArgs;
    string[] specificTests;
    string compilerPath;

    static void addCommonArg(string, string arg)
    {
        if (arg.length)
            commonArgs ~= arg;
    }

    static void addSpecificTest(string, string arg)
    {
        specificTests ~= arg;
    }

    static string styx_path()
    {
        if (!compilerPath.length)
            compilerPath = buildPath([__FILE_FULL_PATH__.dirName.dirName, "bin", "styx"]);
        return compilerPath;
    }
}

bool filterTests(Node n)
{
    if (options.specificTests.length)
    {
        if (n.containsKey("file"))
            return options.specificTests.canFind(n["file"].get!string());
        else if (n.containsKey("alias"))
            return options.specificTests.canFind(n["alias"].get!string());
    }
    return true;
}

extern(C) int isatty(int fd);

void setupColors()
{
    version(Windows)
        return;
    if (isatty(0))
    {
        Red   = "\033[31m";
        Green = "\033[32m";
        Yellow= "\033[33m";
        Normal= "\033[0m" ;
    }
}

int main(string[] args)
{
    const r = getopt(args,
        std.getopt.config.caseSensitive,
        "a", &options.addCommonArg,
        "t", &options.addSpecificTest,
        "c", &options.compilerPath,
    );

    setupColors();

    if (r.helpWanted || args.length == 1)
    {
        writeln(helpText);
        return 0;
    }

    Loader loader   = Loader.fromFile(args[1]);
    Node root       = loader.load();
    int passCount;

    root.sequence().filter!(filterTests).each!(a => passCount += a.execute_test());

    writefln("\n%d/%d tests passed", passCount, options.specificTests.length ?
        options.specificTests.length : root.sequence().length);
    return root.sequence().length != passCount;
}

string base_path()
{
    shared string _base_path;
    if (!_base_path.length)
        _base_path = __FILE_FULL_PATH__.dirName;
    return _base_path;
}

static string Red   ;
static string Green ;
static string Yellow;
static string Normal;

bool execute_test(Node n)
{
    const string alias_ = n.containsKey("alias") ? n["alias"].get!string() : [];
    const string fname  = n.containsKey("file") ? n["file"].get!string() : [];
    const string[] args = n.containsKey("args")  ? n["args"].get!string().splitter.array : [];
    const string[] eout = n.containsKey("output") ? n["output"].get!string().lineSplitter().array() : [];
    const int erc       = n.containsKey("return_code") ? n["return_code"].get!int() : 0;
    const string postcmd= n.containsKey("post_cmd") ? n["post_cmd"].get!string() : [];

    ProcessPipes pp;
    if (fname.length)
        pp  = pipeProcess([options.styx_path(), fname] ~ options.commonArgs ~ args, Redirect.all, null, Config.none, base_path());
    else
        pp  = pipeProcess(options.styx_path() ~ options.commonArgs ~ args, Redirect.all, null, Config.none, base_path());

    const string ppin  = n.containsKey("stdin") ? n["stdin"].get!string() : [];
    if (ppin.length)
    {
        pp.stdin.writeln(ppin);
        pp.stdin.close();
    }

    const int arc       = pp.pid.wait();
    string[] aout       = pp.stdout.byLineCopy().array();
    const bool rc_ok    = arc == erc;
    bool pst_ok         = true;

    if (postcmd.length)
    {
        // this splitter will likely not be enough at some point
        auto r = execute(postcmd.splitter(' ').array);
        pst_ok = r.status == 0;
        aout ~= r.output;
    }

    const bool out_ok   = eout.length ? eout == aout : true;
    const bool pass     = rc_ok && out_ok && pst_ok;

    writeln("TESTING ", alias_ ? alias_ : fname, ": ",  pass ? Green : Red, pass ? "OK" : "FAILED", Normal);

    if (!pass)
    {
        if (!rc_ok)
        {
            writefln("expected return code `%d` but got `%s`", erc, arc);
        }
        else
        {
            writeln("return code is correct");
        }
        if (!out_ok)
        {
            writeln("diff output:\n----------------");

            size_t ie, ia;
            while (true)
            {
                if (ie < eout.length && ia < aout.length)
                {
                    if (eout[ie] != aout[ia])
                    {
                        if (ie + 1 < eout.length && eout[ie + 1] == aout[ia])
                        {
                            writeln(Red, "- | ", eout[ie], Normal);
                            ie++;
                        }
                        else if (ia + 1 < aout.length && aout[ia + 1] == eout[ie])
                        {
                            writeln(Green, "+ | ", aout[ia], Normal);
                            ia++;
                        }
                        else
                        {
                            writeln(Green, "+ | ", aout[ia], Normal);
                            writeln(Red, "- | ", eout[ia], Normal);
                            ie++;
                            ia++;
                        }
                    }
                    else
                    {
                        writeln("= | ", aout[ia]);
                        ie++;
                        ia++;
                    }
                }
                else if (ie < eout.length)
                {
                    writeln(Red, "- | ", eout[ie], Normal);
                    ie++;
                }
                else if (ia < aout.length)
                {
                    writeln(Green, "+ | ", aout[ia], Normal);
                    ia++;
                }
                else break;
            }
        }
        else
        {
            writeln("\nactual output:\n--------------");
            aout.length ? aout.each!(a => writeln(a)) : writeln("(no actual output)");
        }
    }

    return pass;
}
