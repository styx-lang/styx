---
header-includes:
title: Declaration
---

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## Grammar

```{=html}
<code class=ebnf>
Declaration ::= [AliasDeclaration]
              | [ClassDeclaration]
              | [EnumDeclaration]
              | [ExpressionAlias]
              | [FunctionDeclaration]
              | [ImportDeclaration]
              | [LabelDeclaration]
              | [OverloadDeclaration]
              | [ProtectionDeclaration]
              | [StaticAssertDeclaration]
              | [StructDeclaration]
              | [TemplateDeclaration]
              | [UnionDeclaration]
              | [VariableDeclaration] ';'
              | [VersionBlockDeclaration]
              ;
</code>
```
