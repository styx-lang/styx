---
header-includes:
title: Variable declaration
---

## Grammar

```{=html}
<code class=ebnf>
VariableDeclaration ::= [Attribute]* [StorageClass]+ [Type]? [Identifier] ("=" [Expression])?
<!-- keep at the bottom, https://gitlab.com/styx-lang/styx/-/issues/759 -->
StorageClass ::= "const" | "static" | "var"
</code>
```

## Semantics

A variable declaration begins with optional list of [Attribute] followed by a set of storage class.

### Storage class

#### `const`{.styx}

Indicates that the variable cannot be modified.

If the variable is not `@foreign` then it must be initialized with an [Expression].

```{.styx .numberLines}
// out of a function the initializer must be evaluable by the compiler
const auto a1 = 8:u8;
// otherwise it can be any expression
function foo()
{
    const auto a2 = call();
    a2 = otherCall(); // error
}
```

If the variable is `@foreign` then it must not be initialized.

If the variable is a non-static aggregate member then it can be optionally modified
in one of the aggregate constructor.

```{.styx .numberLines}
struct S
{
    const u8* a = null; // static init is still required

    @constructor function make(u8* other)
    {
        // optional mutation
        if other do
            a = other + 1;
    }

    function f()
    {
        a++; // error, now it is really const
    }
}
```

`const`{.styx} can only be combined with `static`{.styx}.

#### `static`{.styx}

Indicates that the variable is a global.

At the global scope, the `static`{.styx} storage is implicit so it doesn't have to be included in the set.

```{.styx .numberLines}
unit u;
static var s32 a; // static is not required at the global scope
```

In a function body this has for effect to hide the declaration.

```{.styx .numberLines}
function foo(): s32
{
    static var s32 v;
    v += 1;
    return v;
}

function main()
{
    assert (foo() == 1);
    assert (foo() == 2);
    // only foo() can see and mutate the global `v`
    foo.v = 3; // error, unrecognized identifier `v`
}
```

When static is neither combined to `var` nor to `const` it is supposed to be the same as `static var`

```{.styx .numberLines}
unit u;
static u32 a = 0; // static var u32 a = 0
static b = 1;     // static auto b = 1
```

#### `var`{.styx}

Indicates that the variable is a mutable lvalue.

`var`{.styx} can only be combined with `static`{.styx}.

### Type

If the type is not specified then it is implicitly set to [TypeAuto], which
implies that the initializer must be present.

### Default initialization

Unless attached to the [NoInitAttribute], a variable that's not explicitly initialized is prefilled with zeroes.

```{.styx .numberLines}
function foo()
{
    var ssize* p1; // prefilled with zeroes.
    assert(p1 == null);
    var ssize* p2; // not prefilled because of the following assignation.
    p2 = call();
}
```

When the [Type] is `auto`{.styx}, default initialization is an error.

```{.styx .numberLines}
function foo()
{
    var auto a1;            // error, `a1` type cannot be resolved without initializer
    var auto a2 = 0:s16;    // ok, 16 bits signed integer can be infered.
}
```

### Initializer

An intializer is an [Expression] that has the same type or that can be implictly converted to the type of the variable.
The expression is also used to infer the variable type when its type is `auto`{.styx}.
The kinds of expression allowed as intializer depend on the declaration scope.

#### Local non-static scope

The initializer of a local non-static variable is a syntax sugar for a declaration followed by an [AssignExpression]
therefore in this context there's no restriction on complexity of the initializer.

```{.styx .numberLines}
function foo()
{
    var s32 local = call1() + call2();
}
```

is like

```{.styx .numberLines}
function foo()
{
    var s32 local;
    local = call1() + call2();
}
```

#### Global, local static and aggregate scope

Otherwise the initializer must be an expression that gives a compile-constant, i.e a simple value.

```{.styx .numberLines}
unit u;

const s32 global0 = 1:s32;  // OK
const s32 global1 = call(); // NG: not compile-constant;

struct Foo
{
    const s32 member0 = 1;      // OK
    const s32 member1 = call(); // NG: not compile-constant;
}

function foo()
{
    static const s32 localStatic1 = call(); // NG: not compile-constant;
}
```

The palette of the expressions considered compile-constant may grow but never shrink.
